//
//  SlideMenuViewController.h
//  letSub
//
//  Created by Vishnu Varthan .P on 07/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlideMenuViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
