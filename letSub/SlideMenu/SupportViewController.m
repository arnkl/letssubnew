//
//  SupportViewController.m
//  letSub
//
//  Created by Vishnu Varthan .P on 16/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "SupportViewController.h"
#import "FeedBackViewController.h"

@interface SupportViewController ()

@end

@implementation SupportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Support";
    // Do any additional setup after loading the view.
}

#pragma mark -

- (IBAction)goHome:(id)sender {
    
     [[AppDelegate instance]openHomeView];
}


-(IBAction)inviteFriends:(id)sender
{

    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"LetsSub Invite";
    alertViewController.message = @"Invite friends using";
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleSlideFromBottom;
    alertViewController.buttonColor = [UIColor colorWithRed:86.0f/255.0f green:1.0f/255.0f blue:71.0f/255.0f alpha:0.90f];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Facebook"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                              [self facebookInvite];
                                                              
                                                          }]];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Email"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                              [self eMailInvite];
                                                                                                                             }]];
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Text Message"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                              [self smsInvite];
                                                            }]];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Cancel"
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                          }]];
    
    
    [self presentViewController:alertViewController animated:YES completion:nil];


}


-(void)facebookInvite
{
    FBSDKAppInviteContent *content =[[FBSDKAppInviteContent alloc] init];
    content.appLinkURL = [NSURL URLWithString:@"https://fb.me/508511066018718"];
    //optionally set previewImageURL
    content.appInvitePreviewImageURL = [NSURL URLWithString:@"http://letssub.com/images/fb_share_v1.png"];
    
    // Present the dialog. Assumes self is a view controller
    // which implements the protocol `FBSDKAppInviteDialogDelegate`.
    [FBSDKAppInviteDialog showFromViewController:self
                                     withContent:content
                                        delegate:nil];

    
}

-(void)eMailInvite
{
    
    if(![MFMailComposeViewController canSendMail]) {
        return;
    }

    
        NSArray *toRecipents = [NSArray arrayWithObject:@""];
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [[controller navigationBar] setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName]];
        [controller setSubject:@"Lets-Sub Invite !!"];
        [controller setMessageBody:@"Hey, Have you heard about Lets-Sub, the easiest way to find sublets with a swipe. </br> </br> Click to download LETS-Sub from <a href=\"https://appsto.re/in/ellncb.i\">AppStore - LetsSub</a> " isHTML:YES];
    
        [controller setToRecipients:toRecipents];
    
        if(controller)
            [self presentViewController:controller animated:YES completion:NULL];
}

-(void)smsInvite
{
    if(![MFMessageComposeViewController canSendText]) {
        return;
    }
    
    NSArray *recipents = @[];
    NSString *message = [NSString stringWithFormat:@"Hey, Have you heard about Lets-Sub, the easiest way to find sublets with a swipe. Click to download LETS-Sub from https://appsto.re/in/ellncb.i"];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [[messageController navigationBar] setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName]];
    [messageController setRecipients:recipents];
    [messageController setBody:message];
    
    // Present message view controller on screen
    if(messageController)
    [self presentViewController:messageController animated:YES completion:nil];
}

-(IBAction)contactUS:(id)sender
{
    FeedBackViewController *postController = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedBackViewController"];
    postController.isPostingFeeds = NO;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:postController];
    [self presentViewController:navigationController animated:YES completion:nil];

}
- (IBAction)redirectToFAQ:(id)sender {
    
    NSURL *url = [NSURL URLWithString:@"http://letssub.com/faq.php"];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

#pragma mark -
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            //NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            //NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            //NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            //NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
            break;
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
