//
//  AddBubbleTagViewController.h
//  letSub
//
//  Created by Vishnu Varthan on 16/06/16.
//  Copyright © 2016 com.vishnuvarthan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JCTagListView.h"

@protocol AddTagDelegate <NSObject>

-(void)selectedTagsList:(NSArray*)arrayList;

@end

@interface AddBubbleTagViewController : UIViewController
@property (weak, nonatomic) IBOutlet JCTagListView *jcTagList;

@property (strong, nonatomic) NSMutableArray *selectedTagList;
@property(nonatomic)id<AddTagDelegate>delegate;


@end
