//
//  AddBubbleTagViewController.m
//  letSub
//
//  Created by Vishnu Varthan on 16/06/16.
//  Copyright © 2016 com.vishnuvarthan. All rights reserved.
//

#import "AddBubbleTagViewController.h"

@interface AddBubbleTagViewController ()

@end

@implementation AddBubbleTagViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureTags];
    
    UIBarButtonItem *tmpButtonItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(donePicking)];
    UIBarButtonItem *tmpButtonItem2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewCustomTag)];
    
    self.navigationItem.rightBarButtonItems = @[tmpButtonItem1,tmpButtonItem2];
    // Do any additional setup after loading the view.
}

-(void)configureTags
{
    self.jcTagList.canSelectTags = YES;
    self.jcTagList.tagStrokeColor = [UIColor colorWithRed:66.0f/255.0f green:28.0f/255.0f blue:82.0f/255.0f alpha:1.0f];
    //    self.tagView.tagBackgroundColor = [UIColor orangeColor];
    self.jcTagList.tagTextColor = [UIColor colorWithRed:66.0f/255.0f green:28.0f/255.0f blue:82.0f/255.0f alpha:1.0f];
    self.jcTagList.tagSelectedBackgroundColor = [UIColor colorWithRed:66.0f/255.0f green:28.0f/255.0f blue:82.0f/255.0f alpha:1.0f];
    self.jcTagList.tagSelectedTextColor = [UIColor whiteColor];
    self.jcTagList.tagCornerRadius = 5.0f;
    [self.jcTagList.tags addObjectsFromArray:@[@"Clean",@"Student",@"Professional",@"Smoker",@"Non-smoker",@"Sharing Rocks",@"Early bird",@"Partier",@"Drinker",@"Non-Drinker",@"Foodie",@"Loves Cooking",@"Book Lover",@"Healthy",@"Artist",@"Loud",@"Quiet",@"Animal lover",@"Dancer",@"Musician",@"Gamer",@"Sports fan",@"Techie",@"Nerdy",@"Fitness buff",@"Movie fan",@"Couch potato",@"Political",@"Yoga lover"]];
    
      // get custom tags
    for (NSString* str in self.selectedTagList)
    {
        if(![self.jcTagList.tags containsObject:str])
        [self.jcTagList.tags addObject:str];
    }
    
    [self.jcTagList.selectedTags addObjectsFromArray:self.selectedTagList];
    [self.jcTagList.collectionView reloadData];
//    [self.jcTagList setCompletionBlockWithSelected:^(NSInteger index) {
//        NSLog(@"______%ld______", (long)index);
//    }];
    
}


-(void)donePicking
{
    if([self.delegate respondsToSelector:@selector(selectedTagsList:)])
    {
        [self.delegate selectedTagsList:self.jcTagList.selectedTags];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addNewCustomTag
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Add custom tag" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"enter tag";
    }];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             
                            // NSLog(@"entered ..%@", alertController.textFields[0].text);
                             [self.jcTagList.tags addObject:alertController.textFields[0].text];
                             [self.jcTagList.selectedTags addObject:alertController.textFields[0].text];
                             [alertController dismissViewControllerAnimated:YES completion:nil];
                             [self.jcTagList.collectionView reloadData];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alertController dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    [alertController addAction:ok];
    [alertController addAction:cancel];
    [self presentViewController:alertController animated:YES completion:nil];
    
    
    
}


@end
