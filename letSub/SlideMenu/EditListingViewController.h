//
//  EditListingViewController.h
//  letSub
//
//  Created by Vishnu Varthan .P on 09/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import "ELCImagePickerHeader.h"
#import "MWPhotoBrowser.h"
#import "AmenitiesViewController.h"
#import "AddBubbleTagViewController.h"
#import "JCTagListView.h"

@protocol AddListingDelegate <NSObject>

-(void)addNewListingToListWithData:(NSDictionary *)data;

@end

@interface EditListingViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,ELCImagePickerControllerDelegate,MWPhotoBrowserDelegate,UITextViewDelegate,UITextFieldDelegate,AddTagDelegate>

@property (weak) id <AddListingDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *errView;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSString *listingID,*listingName;
@property (nonatomic) BOOL isAddingNewListing;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet JCTagListView *tagView;

- (IBAction)reloadData:(id)sender;
- (IBAction)addNewImages:(id)sender;

@end
