//
//  OthersViewController.h
//  letSub
//
//  Created by Vishnu Varthan .P on 28/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OthersViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *contentView;
@property (strong,nonatomic)NSString * pageDetail;
@end
