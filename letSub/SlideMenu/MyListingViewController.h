//
//  MyListingViewController.h
//  letSub
//
//  Created by Vishnu Varthan .P on 09/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "EditListingViewController.h"

@interface MyListingViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,AddListingDelegate>

@property (weak, nonatomic) IBOutlet UIView *errView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *noDataView;
@property (strong,nonatomic)NSString *listerId;
@property (nonatomic) BOOL othersListing;

- (IBAction)reloadContent:(id)sender;

@end
