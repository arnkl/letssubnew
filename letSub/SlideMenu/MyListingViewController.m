//
//  MyListingViewController.m
//  letSub
//
//  Created by Vishnu Varthan .P on 09/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "MyListingViewController.h"
#import "SubletDetailViewController.h"
#import "CustomFavoritiesButton.h"


@interface MyListingViewController ()
{
        NSMutableArray *idArray,*imageArray,*locationArray,*stateArray,*nameArray,*areaArray,*typeArray,*favouriteArray;
}
@end

@implementation MyListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    idArray= [[NSMutableArray alloc]init];
    imageArray = [[NSMutableArray alloc]init];
    locationArray = [[NSMutableArray alloc]init];
    stateArray =[[NSMutableArray alloc]init];
    nameArray = [[NSMutableArray alloc]init];
    areaArray =[[NSMutableArray alloc]init];
    typeArray =[[NSMutableArray alloc]init];
    favouriteArray = [[NSMutableArray alloc]init];
    
    [self performSelector:@selector(loadUserListings) withObject:nil afterDelay:01];
    _tableView.alwaysBounceVertical = NO;
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    if(!self.othersListing)
    {
         self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"homeSelected"]style:UIBarButtonItemStylePlain target:self action:@selector(closeMyListing:)];
        self.title = @"My Listings";
    }
    else{
        self.title = @"User Listings";
        self.navigationItem.rightBarButtonItem = nil;
    }
    
    [super viewWillAppear:YES];
}

#pragma mark -

-(void)loadUserListings{
    
        [AppDelegate showLoadingView:self];
    
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }

    
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:self.othersListing?self.listerId:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"userid",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email", nil];
        NSString *urlAddress = [NSString stringWithFormat:@"sublets/getUserSublets.json"];
        LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
        [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    
            [AppDelegate hideLoadingIndicator];
            //NSLog(@"JSON: %@", responseObject);
            
            NSDictionary *response = [responseObject objectForKey:@"response"];
            NSString *status = [response objectForKey:@"status"];
            
            if([status boolValue] == 1)
            {
                
                NSArray *array = [response objectForKey:@"list"];
                NSMutableDictionary *data1 =[[NSMutableDictionary alloc]init];
                
                for (int i=0; i<[array count]; i++) {
                    
                    data1 = [array objectAtIndex:i];
                    
                    [idArray addObject:[data1 valueForKey:@"id"]];
                    [imageArray addObject:[data1 valueForKey:@"image"]];
                    [locationArray addObject:[data1 valueForKey:@"city"]];
                    [favouriteArray addObject:[data1 valueForKey:@"isFavourite"]];
                    [stateArray addObject:[data1 valueForKey:@"state"]];
                    [nameArray addObject:[data1 valueForKey:@"name"]];
                    [areaArray addObject:[data1 valueForKey:@"size"]];
                    [typeArray addObject:[data1 valueForKey:@"type"]];
                    
                }
                
                [_tableView reloadData];
                _tableView.hidden = NO;
                _errView.hidden = _noDataView.hidden = YES;
                
            }
            else
            {
                _tableView.hidden = YES;
                _errView.hidden = YES;
                _noDataView.hidden = NO;
            }

    
    
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            //NSLog(@"Error: %@", error);
            [AppDelegate hideLoadingIndicator];
            _tableView.hidden = YES;
            _errView.hidden = NO;
            _noDataView.hidden = YES;

    
        }];
    
}


-(void)addNewListingToListWithData:(NSDictionary *)data
{
    _tableView.hidden = NO;
    _errView.hidden = _noDataView.hidden = YES;
    int index = (int)[idArray indexOfObject:[data valueForKey:@"id"]];
    
    if([idArray containsObject:[data valueForKey:@"id"]])
    {
        
        [idArray replaceObjectAtIndex:index withObject:[data valueForKey:@"id"]];
        [imageArray replaceObjectAtIndex:index withObject:[data valueForKey:@"image"]];
        [locationArray replaceObjectAtIndex:index withObject:[data valueForKey:@"city"]];
        [favouriteArray replaceObjectAtIndex:index withObject:[data valueForKey:@"isFavourite"]];
        [stateArray replaceObjectAtIndex:index withObject:[data valueForKey:@"state"]];
        [nameArray replaceObjectAtIndex:index withObject:[data valueForKey:@"name"]];
        [areaArray replaceObjectAtIndex:index withObject:[data valueForKey:@"size"]];
        [typeArray replaceObjectAtIndex:index withObject:[data valueForKey:@"room"]];
        
        
    }
    else{
        
        [idArray insertObject:[data valueForKey:@"id"] atIndex:0 ];
        [imageArray insertObject:[data valueForKey:@"image"] atIndex:0];
        [locationArray insertObject:[data valueForKey:@"city"] atIndex:0 ];
        [favouriteArray insertObject:[data valueForKey:@"isFavourite"] atIndex:0 ];
        [stateArray insertObject:[data valueForKey:@"state"] atIndex:0 ];
        [nameArray insertObject:[data valueForKey:@"name"] atIndex:0 ];
        [areaArray insertObject:[data valueForKey:@"size"] atIndex:0 ];
        [typeArray insertObject:[data valueForKey:@"room"] atIndex:0 ];
    }
    
    [_tableView reloadData];
    
}

- (void)addListingToFavourities:(id)sender {
    
    
    CustomFavoritiesButton *btn = (CustomFavoritiesButton *)sender;
    [btn setBackgroundImage:[UIImage imageNamed:@"favouriteSelected" ]forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(removeFavourities:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"userID",idArray[btn.index],@"listingID",@"1",@"isFavourite",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email", nil];
    
    NSString *urlAddress = [NSString stringWithFormat:@"sublets/toggleFavouriteSublet.json"];
    
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //NSLog(@"JSON: %@", responseObject);
        [KSToastView ks_showToast:@"Added to Favorites !!" duration:2.0f];
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        //NSLog(@"Error: %@", error);
        [btn setBackgroundImage:[UIImage imageNamed:@"favouriteUnSelected" ]forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(addListingToFavourities:) forControlEvents:UIControlEventTouchUpInside];
        
    }];
    
}


-(void)removeFavourities :(id)sender
{
    
    CustomFavoritiesButton *btn = (CustomFavoritiesButton *)sender;
    [btn setBackgroundImage:[UIImage imageNamed:@"favouriteUnSelected" ]forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(addListingToFavourities:) forControlEvents:UIControlEventTouchUpInside];
    
    
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }

    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"userID",idArray[btn.index],@"listingID",@"0",@"isFavourite",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email", nil];
    NSString *urlAddress = [NSString stringWithFormat:@"sublets/toggleFavouriteSublet.json"];
    
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

    } failure:^(NSURLSessionTask *operation, NSError *error) {
        //NSLog(@"Error: %@", error);
      
        [btn setBackgroundImage:[UIImage imageNamed:@"favouriteSelected" ]forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(removeFavourities:) forControlEvents:UIControlEventTouchUpInside];
        
    }];
    
}



-(void)removeDataAtIndex: (int)index
{
    
    [AppDelegate showLoadingView:self];
    
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }

    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"userid",idArray[index],@"listingID",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email", nil];
    NSString *urlAddress = [NSString stringWithFormat:@"sublets/removeSublet.json"];
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [AppDelegate hideLoadingIndicator];
        
        NSDictionary *response = [responseObject objectForKey:@"response"];
        NSString *status = [response objectForKey:@"status"];
        
        if([status boolValue] == 1)
        {
            
            [idArray removeObjectAtIndex:index];
            [imageArray removeObjectAtIndex:index];
            [favouriteArray removeObjectAtIndex:index];
            [locationArray removeObjectAtIndex:index];
            [stateArray removeObjectAtIndex:index];
            [nameArray removeObjectAtIndex:index];
            [areaArray removeObjectAtIndex:index];
            [typeArray removeObjectAtIndex:index];
            [_tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
            [_tableView reloadData];
            
            if([idArray count]==0)
            {
                _tableView.hidden = YES;
                _errView.hidden = YES;
                _noDataView.hidden = NO;
            }
            
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        //NSLog(@"Error: %@", error);
    }];
    
}

#pragma mark - Target Actions

- (IBAction)createNewListing:(id)sender {

    EditListingViewController *editVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EditListingViewController"];
    editVC.listingID = @"";
    editVC.delegate = self;
    editVC.isAddingNewListing = YES;
    [self.navigationController pushViewController:editVC animated:YES];

}



- (IBAction)closeMyListing:(id)sender {
    
    [[AppDelegate instance]openHomeView];
    
}

- (IBAction)reloadContent:(id)sender {
    
    [self loadUserListings];
}


#pragma mark - TableView Delegate & Data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return [idArray count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"myListCell" forIndexPath:indexPath];
    UIImageView *img = (UIImageView *)[cell viewWithTag:550];
    [img setImageWithURL:imageArray[indexPath.row] placeholderImage:[UIImage imageNamed:@"defaultImage"]];
    UILabel *heading = (UILabel *)[cell viewWithTag:551];
    heading.text = nameArray[indexPath.row];
    UILabel *location = (UILabel *)[cell viewWithTag:552];
    location.text =  [NSString stringWithFormat:@"%@, %@",locationArray[indexPath.row],stateArray[indexPath.row]];
    UILabel *detail = (UILabel *)[cell viewWithTag:553];
    detail.text =[NSString stringWithFormat:@"%@ / %@",typeArray[indexPath.row],areaArray[indexPath.row]];

    CustomFavoritiesButton *btn = (CustomFavoritiesButton *)[cell viewWithTag:554];
    btn.index = (int)indexPath.row;
    
    if ([favouriteArray[indexPath.row] boolValue] == 1) {
        
        [btn setBackgroundImage:[UIImage imageNamed:@"favouriteSelected" ]forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(removeFavourities:) forControlEvents:UIControlEventTouchUpInside];

    }
    else{
        
        [btn setBackgroundImage:[UIImage imageNamed:@"favouriteUnSelected" ]forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(addListingToFavourities:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    btn.hidden= !self.othersListing;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(!self.othersListing)
    {
        EditListingViewController *editVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EditListingViewController"];
        editVC.listingID = idArray [indexPath.row];
        editVC.listingName = nameArray [indexPath.row];
        editVC.isAddingNewListing = NO;
        editVC.delegate = self;
        [self.navigationController pushViewController:editVC animated:YES];
    }
    else{
        
        SubletDetailViewController*  subLetDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SubletDetailViewController"];
        subLetDetailVC.listingID = idArray [indexPath.row];
        subLetDetailVC.listingName = nameArray [indexPath.row];
        subLetDetailVC.hideListerInfo = YES;
        [self.navigationController pushViewController:subLetDetailVC animated:YES];

    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return !self.othersListing;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
       
        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
        alertViewController.title = @"Warning !";
        alertViewController.message = @"Are you sure you want to delete?";
        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
        alertViewController.buttonColor = [UIColor colorWithRed:86.0f/255.0f green:1.0f/255.0f blue:71.0f/255.0f alpha:0.90f];
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"Delete"
                                                                style:UIAlertActionStyleDestructive
                                                              handler:^(NYAlertAction *action) {
                                                                  [self dismissViewControllerAnimated:YES completion:nil];
                                                                  [self removeDataAtIndex:(int)indexPath.row];
                                                                  
                                                                  
                                                              }]];
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"Cancel"
                                                                style:UIAlertActionStyleCancel
                                                              handler:^(NYAlertAction *action) {
                                                                  [self dismissViewControllerAnimated:YES completion:nil];
                                                                }]];
        
       
        [self presentViewController:alertViewController animated:YES completion:nil];
        
        //add code here for when you hit delete
    }
}




@end
