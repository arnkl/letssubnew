//
//  AboutViewController.h
//  letSub
//
//  Created by Vishnu Varthan .P on 16/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController

- (IBAction)back:(id)sender;
- (IBAction)openTermsConditions:(id)sender;
- (IBAction)openPrivacyPolicy:(id)sender;

@property (weak, nonatomic) IBOutlet UITextView *aboutContentView;

@end
