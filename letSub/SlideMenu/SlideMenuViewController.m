//
//  SlideMenuViewController.m
//  letSub
//
//  Created by Vishnu Varthan .P on 07/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "SlideMenuViewController.h"
#import "AppDelegate.h"

@interface SlideMenuViewController ()
{
    NSArray *titles,*images;
}
@end

@implementation SlideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    titles = @[@"LET'S SUB HOME",@"My FEEDS",@"MY FAVORITES",@"MY LISTINGS",@"CHATTING",@"SETTINGS"];
    images = @[@"purple_house icon",@"feeds_Selected",@"favorities_Selected",@"my_listings",@"chat_Selected",@"settings"];

    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self.tableView reloadData];
}

#pragma mark - TableView Delegate & Data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return titles.count+1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == 0)
    return 194;
    else
    return 66;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell* cell ;
    if (indexPath.row == 0) {
        cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"profileCell" forIndexPath:indexPath];
        UIImageView* dpImageView = (UIImageView*)[cell viewWithTag:12];
        NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserPicture"];
        dpImageView.image = [UIImage imageWithData:imageData];
        [[dpImageView layer] setCornerRadius:50.0];
        [[dpImageView layer] setBorderWidth:1.0];
        [[dpImageView layer] setBorderColor:[[UIColor colorWithRed:0.384 green:0.000 blue:0.686 alpha:1.000] CGColor]];
        
        UILabel *lbl = (UILabel *)[cell viewWithTag:13];
        lbl.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserName"];
    }
    
    else
    {
        cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"listCell" forIndexPath:indexPath];
        UILabel *lbl = (UILabel *)[cell viewWithTag:11];
        lbl.text = titles[indexPath.row -1];
        UIImageView *img = (UIImageView *)[cell viewWithTag:10];
        img.image =[UIImage imageNamed:images[indexPath.row -1]];

    }
    
    cell.textLabel.textColor = [UIColor blackColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSLog(@"index %ld",(long)indexPath.row);
    
    switch (indexPath.row) {
        case 1:
            [[AppDelegate instance] openHomeView];
            break;
        case 2:
            [[AppDelegate instance] openFeedView];
            break;
        case 3:
            [[AppDelegate instance] openFavoriteView];
            break;
        case 4:
            [[AppDelegate instance] openMyListing];
            break;
        case 5:
            [[AppDelegate instance] openChatView];
            break;
            
        case 6:
            [[AppDelegate instance] openSupport];
            break;
            
        default:
            break;
    }
    
}
@end
