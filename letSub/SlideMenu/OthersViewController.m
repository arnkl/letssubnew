//
//  OthersViewController.m
//  letSub
//
//  Created by Vishnu Varthan .P on 28/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "OthersViewController.h"

@interface OthersViewController ()

@end

@implementation OthersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    self.title = self.pageDetail;

    if([self.pageDetail isEqualToString:@"Terms&Conditions"])
    {
        NSAttributedString *attributedStringWithRtf = [[NSAttributedString alloc]initWithFileURL:[NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"terms&conditions" ofType:@"rtf"] ] options:@{NSDocumentTypeDocumentAttribute:NSRTFTextDocumentType} documentAttributes:nil error:nil];
        self.contentView.attributedText=attributedStringWithRtf;
    }
    else if([self.pageDetail isEqualToString:@"PrivacyPolicy"])
    {
        NSAttributedString *attributedStringWithRtf = [[NSAttributedString alloc]initWithFileURL:[NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"PrivacyPolicy" ofType:@"rtf"] ] options:@{NSDocumentTypeDocumentAttribute:NSRTFTextDocumentType} documentAttributes:nil error:nil];
        self.contentView.attributedText=attributedStringWithRtf;
    }
    else if([self.pageDetail isEqualToString:@"Equal Housing"])
    {
        NSAttributedString *attributedStringWithRtf = [[NSAttributedString alloc]initWithFileURL:[NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"Equal_Housing" ofType:@"rtf"] ] options:@{NSDocumentTypeDocumentAttribute:NSRTFTextDocumentType} documentAttributes:nil error:nil];
        self.contentView.attributedText=attributedStringWithRtf;
        
    }

    
}


@end
