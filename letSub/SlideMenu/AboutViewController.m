//
//  AboutViewController.m
//  letSub
//
//  Created by Vishnu Varthan .P on 16/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "AboutViewController.h"
#import "OthersViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"About";
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    NSAttributedString *attributedStringWithRtf = [[NSAttributedString alloc]initWithFileURL:[NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"about" ofType:@"rtf"] ] options:@{NSDocumentTypeDocumentAttribute:NSRTFTextDocumentType} documentAttributes:nil error:nil];
    self.aboutContentView.attributedText=attributedStringWithRtf;
}

#pragma mark - Target Actions

- (IBAction)back:(id)sender {
    
    [[AppDelegate instance]openHomeView];
}

- (IBAction)openTermsConditions:(id)sender {
    
    OthersViewController *othersVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OthersViewController"];
    othersVC.pageDetail = @"Terms&Conditions";
    [self.navigationController pushViewController:othersVC animated:YES];
    
}

-(IBAction)openEqualHousing:(id)sender
{
    OthersViewController *othersVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OthersViewController"];
    othersVC.pageDetail = @"Equal Housing";
    [self.navigationController pushViewController:othersVC animated:YES];

}

- (IBAction)openPrivacyPolicy:(id)sender {
    
    OthersViewController *othersVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OthersViewController"];
    othersVC.pageDetail = @"PrivacyPolicy";
    [self.navigationController pushViewController:othersVC animated:YES];
}

@end
