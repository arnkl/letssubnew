//
//  EditListingViewController.m
//  letSub
//
//  Created by Vishnu Varthan .P on 09/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "EditListingViewController.h"

@interface EditListingViewController ()
{
    NSString *areaString,*bathroomString,*bedRoomString,*closestTransportString,*transportDistanceString,*descriptionString,*nameString,*priceString,*roommatesString,*locationString,*stateString,*zipString,*moveInString,*moveOutString,*depositString,*minStayString,*bedTypeString,*propertyTypeString,*furnishStr,*expiryString;
    
    NSMutableArray *amenityList,*restrictionList,*imagesListArray,*imagesArray,*lookingForArray;
    NSMutableDictionary *listerInfoDict;
    
    UITextField *nameTxt,*addressZipTxt,*addCityTxt,*addStateTxt,*sizeTxt,*priceTxt,*nearStationTxt,*nearMilesTxt,*bathroomTxt,*bedRoomTx,*roommateTxt,*minStayTxt,*deposteTxt;
    UITextView *descTxtView;
    UIButton *addImgButton,*moveInButton,*moveOutButton;
    UIDatePicker *datePicker,*outDatePicker;
    BOOL canEdit,showHiddenCell,showMoveInPicker;
    int defaultImageSlots;
    
    MWPhotoBrowser *browser;
    AmenitiesViewController *amenitiesVC;
}

@end

@implementation EditListingViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    UIBarButtonItem *tmpButtonItem1 = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(startEditing:)];
    UIBarButtonItem *tmpButtonItem2 = [[UIBarButtonItem alloc] initWithTitle:@"Create" style:UIBarButtonItemStylePlain target:self action:@selector(createListing:)];
    UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigationBackButton"] style:UIBarButtonItemStylePlain target:self action:@selector(leavePage)];
    
    self.navigationItem.leftBarButtonItem = leftBarButton;
    self.navigationItem.rightBarButtonItem = self.isAddingNewListing?tmpButtonItem2:tmpButtonItem1;
    self.navigationController.navigationBar.translucent =NO;
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    // Do any additional setup after loading the view.
    
    imagesArray = [[NSMutableArray alloc]init];
    
    bedTypeString = @"0";
    propertyTypeString = @"1";
    
    [[NSNotificationCenter defaultCenter]removeObserver:@"selectedAmenities"];
    [[NSNotificationCenter defaultCenter]removeObserver:@"selectedRestrictions"];
    
    if(!self.isAddingNewListing)
    {
        [self performSelector:@selector(loadSubletDetails) withObject:nil afterDelay:01];
        self.title = self.listingName;
    }
    else
    {
        self.title = @"New Listing";
        _tableView.hidden = NO;
        _errView.hidden = YES;
        expiryString = [self getExpiryDate];
    }
    
    listerInfoDict = [[NSMutableDictionary alloc]init];
    imagesListArray = [[NSMutableArray alloc]init];
    amenityList = [[NSMutableArray alloc]init];
    restrictionList = [[NSMutableArray alloc]init];
    lookingForArray = [[NSMutableArray alloc]init];
    
}


#pragma mark - Network Operations

-(void)loadSubletDetails{
    
    [AppDelegate showLoadingView:self];
    
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }

    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:self.listingID,@"listingID",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email", nil];
    NSString *urlAddress = [NSString stringWithFormat:@"sublets/getSubletDetails.json"];
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
      //  //NSLog(@"JSON: %@", responseObject);
        
        NSDictionary *listing = [[responseObject objectForKey:@"response"] objectForKey:@"detail_listing"];
        areaString = [listing objectForKey:@"size"];
        bathroomString = [listing objectForKey:@"bathrooms"];
        bedRoomString = [listing objectForKey:@"bedrooms"];
        roommatesString = [listing objectForKey:@"roommates"];
        closestTransportString = [listing objectForKey:@"closestTransport"];
        transportDistanceString = [listing objectForKey:@"closestTransportDistance"];
        descriptionString = [listing objectForKey:@"description"];
        furnishStr = [listing objectForKey:@"furnishDetails"];
        locationString = [listing objectForKey:@"city"];
        stateString= [listing objectForKey:@"state"];
        zipString= [listing objectForKey:@"zipCode"];
        nameString = [listing objectForKey:@"name"];
        priceString = [listing objectForKey:@"price"];
        bedTypeString =  [listing objectForKey:@"room"];
         propertyTypeString =  [listing objectForKey:@"type"];
        lookingForArray = [[listing objectForKey:@"lookingFor"]mutableCopy];
        expiryString = [listing objectForKey:@"expiryDate"];
        minStayString =[[listing objectForKey:@"minStay"]isEqualToString:@"(empty)"]?@"":[listing objectForKey:@"minStay"];
        depositString = [listing objectForKey:@"deposit"];
        moveInString = [[listing objectForKey:@"moveInDate"]isEqualToString:@"(empty)"]?@"":[listing objectForKey:@"moveInDate"];
         moveOutString = [[listing objectForKey:@"moveOutDate"]isEqualToString:@"(empty)"]?@"":[listing objectForKey:@"moveOutDate"];
        listerInfoDict = [listing objectForKey:@"Lister_info"];
        imagesListArray = [listing objectForKey:@"Images_list"];
        amenityList = [[listing objectForKey:@"amenities"]mutableCopy];
        restrictionList = [[listing objectForKey:@"restrictions"]mutableCopy];
        
        for (int count = 0; count<[imagesListArray count]; count ++) {
            
            [imagesArray addObject:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imagesListArray[count]]]]];
        }
        
        [_tableView reloadData];
        _tableView.hidden = NO;
        _errView.hidden = YES;
        
        [AppDelegate hideLoadingIndicator];
        
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
      //  //NSLog(@"Error: %@", error);
        [AppDelegate hideLoadingIndicator];
      //  //NSLog(@"err..%@",error);
        _tableView.hidden = YES;
        _errView.hidden = NO;
        
        
    }];
    
}


-(void)postNewListingToServer
{
    
    
    [AppDelegate showLoadingView:self];
    
    NSMutableArray *tempImagesArray = [[NSMutableArray alloc]init];
    
    for (int i =0; i<[imagesArray count]; i++) {
        
        NSData *data1 = UIImagePNGRepresentation(imagesArray[i]);
        NSData *data2 = UIImagePNGRepresentation([UIImage imageNamed:@"defaultImage"]);
        
        if(![data1 isEqual:data2])
        {
            [tempImagesArray addObject:imagesArray[i]];
        }
        
    }
    
   // //NSLog(@"temp images Array...%@",tempImagesArray);
    
    NSData *amenityData = nil;
    NSData *restrictionData = nil;
    NSData *lookingForData = nil;
    
    if(amenityList)
    {
        amenityData = [NSJSONSerialization dataWithJSONObject:amenityList
                                                      options:kNilOptions error:nil];
        
    }
    
    if(lookingForArray)
    {
        lookingForData = [NSJSONSerialization dataWithJSONObject:lookingForArray
                                                      options:kNilOptions error:nil];
        
    }
    
    if(restrictionList)
    {
        restrictionData= [NSJSONSerialization dataWithJSONObject:restrictionList
                                                         options:kNilOptions error:nil];
    }

    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }

    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"user_id",nameString,@"name",descriptionString,@"description",lookingForData?lookingForData:@"",@"lookingFor",locationString,@"city",stateString,@"state",zipString,@"zipCode",areaString,@"size",priceString,@"price",[closestTransportString length]!=0?closestTransportString:@"",@"closestTransport",[transportDistanceString length]!=0?transportDistanceString:@"",@"closestTransportDistance",bedRoomString,@"bedrooms",bathroomString,@"bathrooms",[roommatesString length]!=0?roommatesString:@"Not Mentioned",@"roommates",[furnishStr length]!=0?furnishStr:@"",@"furnishDetails",[minStayString length]!=0?minStayString:@"",@"minStay",[depositString length]!=0?depositString:@"",@"deposit",bedTypeString,@"room",propertyTypeString,@"type",moveInString?moveInString:@"",@"moveInDate",moveOutString?moveOutString:@"",@"moveOutDate",amenityData?amenityData:@"",@"amenities",restrictionData?restrictionData:@"",@"restrictions",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email",expiryString,@"expiryDate",nil];
    
   //NSLog(@"subletDictionary....%@",parameters);
    
    NSString *urlAddress = [NSString stringWithFormat:@"sublets/createSublet.json"];
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    [client POST:urlAddress parameters:parameters  constructingBodyWithBlock:^(id<AFMultipartFormData> formData){
        
        for(int i=0;i<[tempImagesArray count];i++)
        {
            UIImage *eachImage  = [tempImagesArray objectAtIndex:i];
            NSData *imageData = UIImageJPEGRepresentation(eachImage,0.5);
            [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"file%d",i ] fileName:[NSString stringWithFormat:@"image%d.jpg",i ] mimeType:@"image/jpeg"];
        }
        
    } progress:nil  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
     //   //NSLog(@"JSON: %@", responseObject);
        responseObject = [responseObject objectForKey:@"response"];
        NSString *status =[responseObject objectForKey:@"status"];
        
        if([status boolValue]== 1)
        {
            
            
            if ([bedTypeString isEqualToString:@"0"]) {
                
                bedTypeString = @"Studio";
            }
            else if([bedTypeString isEqualToString:@"4"]){
                
                bedTypeString =@"3BED+";
            }
            else
            {
                
                bedTypeString = [bedTypeString stringByAppendingString:@"BED"];
            }
            
            NSDictionary *dict  =[NSDictionary dictionaryWithObjectsAndKeys:[responseObject objectForKey:@"listingID"],@"id",[responseObject objectForKey:@"listingImage"],@"image",locationString,@"city",stateString,@"state",nameString,@"name",[areaString stringByAppendingString:@" sqft"],@"size",bedTypeString,@"room",[NSNumber numberWithBool:NO],@"isFavourite", nil];
            [self.delegate addNewListingToListWithData:dict];
            
            [AppDelegate hideLoadingIndicator];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [AppDelegate hideLoadingIndicator];
            
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
      //  //NSLog(@"Error: %@", error);
        
        [AppDelegate hideLoadingIndicator];
        [self showAlertToUserwithTitle:@"Oops" message:@"Operation cannot be completed, Try again." andCancelTitle:@"OK"];

        
    }];
    
}

-(void)updateListingToServer
{
    [AppDelegate showLoadingView:self];
    NSMutableArray *tempImagesArray = [[NSMutableArray alloc]init];
    
    for (int i =0; i<[imagesArray count]; i++) {
        
        NSData *data1 = UIImagePNGRepresentation(imagesArray[i]);
        NSData *data2 = UIImagePNGRepresentation([UIImage imageNamed:@"defaultImage"]);
        
        if(![data1 isEqual:data2])
        {
            [tempImagesArray addObject:imagesArray[i]];
        }
        
    }
    
    NSData *amenityData = nil;
    NSData *restrictionData = nil;
    NSData *lookingForData = nil;

    
    if(amenityList)
    {
   amenityData = [NSJSONSerialization dataWithJSONObject:amenityList
                                                          options:kNilOptions error:nil];
    
    }
    
    if(restrictionList)
    {
    restrictionData= [NSJSONSerialization dataWithJSONObject:restrictionList
                                                              options:kNilOptions error:nil];
    }
    
    if(lookingForArray)
    {
        lookingForData = [NSJSONSerialization dataWithJSONObject:lookingForArray
                                                         options:kNilOptions error:nil];
        
    }
    
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }

    
 //   //NSLog(@"temp images Array...%@",tempImagesArray);
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"user_id",nameString,@"name",descriptionString,@"description",lookingForData?lookingForData:@"",@"lookingFor",locationString,@"city",stateString,@"state",zipString,@"zipCode",areaString,@"size",priceString,@"price",closestTransportString,@"closestTransport",transportDistanceString,@"closestTransportDistance",bedRoomString,@"bedrooms",bathroomString,@"bathrooms",roommatesString,@"roommates",furnishStr,@"furnishDetails",minStayString,@"minStay",depositString,@"deposit",moveInString,@"moveInDate",moveOutString,@"moveOutDate",amenityData?amenityData:@"",@"amenities",restrictionData?restrictionData:@"",@"restrictions",self.listingID,@"listingID",bedTypeString,@"room",propertyTypeString,@"type",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email",[self getExpiryDate],@"expiryDate",nil];
    
  //  //NSLog(@"subletDictionary....%@",parameters);
    
    
    NSString *urlAddress = [NSString stringWithFormat:@"sublets/createSublet.json"];
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    [client POST:urlAddress parameters:parameters  constructingBodyWithBlock:^(id<AFMultipartFormData> formData){
        
        for(int i=0;i<[tempImagesArray count];i++)
        {
            UIImage *eachImage  = [tempImagesArray objectAtIndex:i];
            NSData *imageData = UIImageJPEGRepresentation(eachImage,0.5);
            [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"file%d",i ] fileName:[NSString stringWithFormat:@"image%d.jpg",i ] mimeType:@"image/jpeg"];
        }
        
    } progress:nil  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
      //  //NSLog(@"JSON: %@", responseObject);
        responseObject = [responseObject objectForKey:@"response"];
        NSString *status =[responseObject objectForKey:@"status"];
        
        if([status boolValue]== 1)
        {
            
            
            if ([bedTypeString isEqualToString:@"0"]) {
                
                bedTypeString = @"Studio";
            }
            else if([bedTypeString isEqualToString:@"4"]){
                
                bedTypeString = @"3BED+";
            }
            else
            {
                
                bedTypeString = [bedTypeString stringByAppendingString:@"BED"];
            }
            
            NSDictionary *dict  =[NSDictionary dictionaryWithObjectsAndKeys:[responseObject objectForKey:@"listingID"],@"id",[responseObject objectForKey:@"listingImage"],@"image",locationString,@"city",stateString,@"state",nameString,@"name",[areaString stringByAppendingString:@" sqft"],@"size",bedTypeString,@"room",[NSNumber numberWithBool:NO],@"isFavourite", nil];
            
            [self.delegate addNewListingToListWithData:dict];
            [AppDelegate hideLoadingIndicator];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [AppDelegate hideLoadingIndicator];
            
        }
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
   //     //NSLog(@"Error: %@", error);
        [AppDelegate hideLoadingIndicator];
        [self showAlertToUserwithTitle:@"Oops" message:@"Operation cannot be completed, Try again." andCancelTitle:@"OK"];
        
    }];
    
}



#pragma mark -

-(void)updateAmenities :(NSNotification*)info
{
    [amenityList removeAllObjects];
    
    NSArray *array = [info.userInfo objectForKey:@"selectedIndex"];
    
    for(int i = 0 ; i< [array count]; i++)
    {
        NSIndexPath *index = array[i];
        [amenityList addObject:[dataObject amenityNameForNumber:(int)index.row]];
    }
    [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:17 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}

-(void)updateRestrictions :(NSNotification*)info
{
    [restrictionList removeAllObjects];
    
    NSArray *array = [info.userInfo objectForKey:@"selectedIndex"];
    
    for(int i = 0 ; i< [array count]; i++)
    {
        NSIndexPath *index = array [i];
        [restrictionList addObject:[dataObject restrictionNameForNumber:(int)index.row]];
    }
    
    [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:18 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}

-(void)showAlertToUserwithTitle :(NSString*)title message :(NSString*)message andCancelTitle:(NSString*)cancelTitle
{
    
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleSlideFromBottom;
    
    alertViewController.title =title;
    alertViewController.message =message;
    [alertViewController addAction:[NYAlertAction actionWithTitle:cancelTitle
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                              
                                                          }]];
    
    [self presentViewController:alertViewController animated:YES completion:nil];
    
    
}


#pragma mark -

- (IBAction)reloadData:(id)sender {
    
    [self loadSubletDetails];
}

-(IBAction)startEditing:(id)sender
{
    UIBarButtonItem *tmpButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveEditing:)];
    self.navigationItem.rightBarButtonItem = tmpButtonItem;
    
    self.isAddingNewListing = YES;
    [self.tableView reloadData];
}

-(IBAction)saveEditing:(id)sender
{
    
    [self.tableView reloadData];

    
    if(![self isFieldsFilled])
    {
        [self showAlertToUserwithTitle:@"Sorry !!" message:@"Some of the mandatory fields are left empty." andCancelTitle:@"Ok, I will fill them !!"];
        
    }
    else{
        
        
        if([imagesArray count]>0)
        {
            
            NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
            alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleSlideFromBottom;
            alertViewController.title =@"Save edited contents ?";
            alertViewController.message = @"Do you wish to save the listing with the edited contents.";
            alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleSlideFromBottom;
            alertViewController.buttonColor = [UIColor colorWithRed:86.0f/255.0f green:1.0f/255.0f blue:71.0f/255.0f alpha:0.90f];
            
            UIBarButtonItem *tmpButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(startEditing:)];
            UIBarButtonItem *tmpButtonItem1 = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveEditing:)];
            
            [alertViewController addAction:[NYAlertAction actionWithTitle:@"No, Not Yet!!"
                                                                    style:UIAlertActionStyleCancel
                                                                  handler:^(NYAlertAction *action) {
                                                                      self.isAddingNewListing =YES;
                                                                      self.navigationItem.rightBarButtonItem = tmpButtonItem1;
                                                                      [self.tableView reloadData];
                                                                      [self dismissViewControllerAnimated:YES completion:nil];
                                                                      
                                                                  }]];
            
            
            [alertViewController addAction:[NYAlertAction actionWithTitle:@"Save Listing"
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(NYAlertAction *action) {
                                                                      self.isAddingNewListing =NO;
                                                                      self.navigationItem.rightBarButtonItem = tmpButtonItem;
                                                                      [self.tableView reloadData];
                                                                      [self dismissViewControllerAnimated:YES completion:nil];
                                                                      [self updateListingToServer];
                                                                      
                                                                  }]];
            
            [self presentViewController:alertViewController animated:YES completion:nil];
        }
        
        else
        {
            [self showAlertToUserwithTitle:@"Sorry !!" message:@"Minimum of one image is required." andCancelTitle:@"Ok, I will add !!"];
        }
    }
    
    
    
    
}


-(IBAction)createListing:(id)sender
{
    
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleSlideFromBottom;
    
    
    if(![self isFieldsFilled])
    {
        alertViewController.title =@"Sorry !!";
        alertViewController.message =@"Some of the mandatory fields are left empty.";
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"Ok, I will fill them !!"
                                                                style:UIAlertActionStyleCancel
                                                              handler:^(NYAlertAction *action) {
                                                                  [self dismissViewControllerAnimated:YES completion:nil];
                                                                  
                                                              }]];
        
    }
    else{
        
        
        if([imagesArray count]>0)
        {
            
            self.isAddingNewListing = YES;
            
            
            alertViewController.title =@"Create a new listing ?";
            alertViewController.message =@"Do you wish to create a new listing with the saved contents.";
            
            [alertViewController addAction:[NYAlertAction actionWithTitle:@"Create Listing"
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(NYAlertAction *action) {
                                                                      [self dismissViewControllerAnimated:YES completion:nil];
                                                                      [self postNewListingToServer];
                                                                      
                                                                  }]];
            
            [alertViewController addAction:[NYAlertAction actionWithTitle:@"No, Not Yet!!"
                                                                    style:UIAlertActionStyleCancel
                                                                  handler:^(NYAlertAction *action) {
                                                                      [self dismissViewControllerAnimated:YES completion:nil];
                                                                      
                                                                  }]];
            
        }
        
        else
        {
            alertViewController.title =@"Sorry !!";
            alertViewController.message =@"Minimum of one image is required.";
            
            [alertViewController addAction:[NYAlertAction actionWithTitle:@"Ok, I will add !!"
                                                                    style:UIAlertActionStyleCancel
                                                                  handler:^(NYAlertAction *action) {
                                                                      [self dismissViewControllerAnimated:YES completion:nil];
                                                                      
                                                                  }]];
            
        }
    }
    [self presentViewController:alertViewController animated:YES completion:nil];
    
    
}
- (IBAction)showMoveOutPicker:(id)sender {
    
    showHiddenCell=YES;
    showMoveInPicker = NO;
    [_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];

}

- (IBAction)showDatePicker:(id)sender {
    
    showHiddenCell=YES;
    showMoveInPicker = YES;
    [_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    
}
- (IBAction)pickedMoveOutDate:(id)sender {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    moveOutString = [dateFormat stringFromDate:outDatePicker.date];
    showHiddenCell=NO;
    [_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];

}



-(IBAction)pickedDate:(id)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    moveInString = [dateFormat stringFromDate:datePicker.date];
    showHiddenCell=NO;
    [_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}


- (IBAction)pickedType:(id)sender {
    
    UISegmentedControl *segment = sender;
    bedTypeString =[NSString stringWithFormat:@"%ld",(long)segment.selectedSegmentIndex];
    
}

- (IBAction)pickedPropertyType:(id)sender {
    
    UISegmentedControl *segment = sender;
    propertyTypeString =[NSString stringWithFormat:@"%ld",(long)segment.selectedSegmentIndex];
    
}

- (IBAction)addNewImages:(id)sender {
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [imagePickerController setDelegate:self];
    
    
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"";
    alertViewController.message = @"Select a image source";
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleSlideFromBottom;
    alertViewController.buttonColor = [UIColor colorWithRed:86.0f/255.0f green:1.0f/255.0f blue:71.0f/255.0f alpha:0.90f];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Camera"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                              [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
                                                              [imagePickerController setCameraDevice:UIImagePickerControllerCameraDeviceRear];
                                                              self.tabBarController.tabBar.hidden = YES;
                                                              [self presentViewController:imagePickerController animated:YES completion:nil];
                                                              
                                                          }]];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Gallery"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                              ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initImagePicker];
                                                              
                                                              elcPicker.maximumImagesCount = defaultImageSlots; //Set the maximum number of images to select to 100
                                                              elcPicker.returnsOriginalImage = YES; //Only return the fullScreenImage, not the fullResolutionImage
                                                              elcPicker.returnsImage = YES; //Return UIimage if YES. If NO, only return asset location information
                                                              elcPicker.onOrder = YES; //For multiple image selection, display and return order of selected images
                                                              elcPicker.mediaTypes = @[(NSString *)kUTTypeImage]; //Supports only image
                                                              
                                                              elcPicker.imagePickerDelegate = self;
                                                              
                                                              [self presentViewController:elcPicker animated:YES completion:nil];
                                                              self.tabBarController.tabBar.hidden = YES;
                                                              
                                                          }]];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Cancel"
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                          }]];
    
    
    [self presentViewController:alertViewController animated:YES completion:nil];
    
    
}


-(BOOL)isFieldsFilled
{
    bool fieldsFilled = ([areaString length]!=0&&[locationString length]!=0&&[stateString length]!=0&&[zipString length]!=0&&[priceString length]!=0&&[nameString length]!=0&&[descriptionString length]!=0&&[bedRoomString length]!=0&&[bathroomString length]!=0);
    
    return fieldsFilled;
}


-(NSString *)getExpiryDate   // every listing will expiry in 2 months from creation, edititng a listing will increase the exiry date.
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDate *expiryDate = [cal dateByAddingUnit:NSCalendarUnitMonth value:2 toDate:[NSDate date] options:0];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    return  [dateFormat stringFromDate:expiryDate];
    
}

-(void)leavePage
{
    
    if(self.isAddingNewListing)
    {
        
        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
        alertViewController.title = @"Are you sure you want to leave this page?";
        alertViewController.message = @"All your unsaved progress will be lost";
        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleSlideFromBottom;
        alertViewController.buttonColor = [UIColor colorWithRed:86.0f/255.0f green:1.0f/255.0f blue:71.0f/255.0f alpha:0.90f];
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"Yes,Leave"
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(NYAlertAction *action) {
                                                                  [self dismissViewControllerAnimated:YES completion:nil];
                                                                  [self.navigationController popViewControllerAnimated:YES];
                                                                  
                                                              }]];
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"No,Stay"
                                                                style:UIAlertActionStyleCancel
                                                              handler:^(NYAlertAction *action) {
                                                                  [self dismissViewControllerAnimated:YES completion:nil];
                                                                  
                                                              }]];
        
        
        [self presentViewController:alertViewController animated:YES completion:nil];

        
    }
    else{
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

#pragma mark - Bubble Tag

- (IBAction)addNewTags:(id)sender {
    
    AddBubbleTagViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddBubbleTagViewController"];
    vc.delegate = self;
    vc.selectedTagList = self.tagView.selectedTags;
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)configureTags
{
    
    self.tagView.canSelectTags = NO;
    self.tagView.tagStrokeColor = [UIColor colorWithRed:66.0f/255.0f green:28.0f/255.0f blue:82.0f/255.0f alpha:1.0f];
    //    self.tagView.tagBackgroundColor = [UIColor orangeColor];
    self.tagView.tagTextColor = [UIColor colorWithRed:66.0f/255.0f green:28.0f/255.0f blue:82.0f/255.0f alpha:1.0f];
    self.tagView.tagSelectedBackgroundColor = [UIColor colorWithRed:66.0f/255.0f green:28.0f/255.0f blue:82.0f/255.0f alpha:1.0f];
    self.tagView.tagSelectedTextColor = [UIColor whiteColor];
    self.tagView.tagCornerRadius = 5.0f;
    
    [self.tagView.tags removeAllObjects];
    [self.tagView.selectedTags removeAllObjects];
    
    [self.tagView.tags addObjectsFromArray:lookingForArray];
    [self.tagView.selectedTags addObjectsFromArray:self.tagView.tags];
    [self.tagView.collectionView reloadData];
    
}

-(void)selectedTagsList:(NSArray*)arrayList
{
    
    [lookingForArray removeAllObjects];
    [self.tagView.tags removeAllObjects];
    [self.tagView.selectedTags removeAllObjects];
    [lookingForArray addObjectsFromArray:arrayList];
    [_tableView reloadData];
}


#pragma mark - Image Picker Delegates

- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    
    [imagesArray insertObject:image atIndex:0];
    
    int count = (int)[imagesArray count]-1;
    
    for(int i = count ; [imagesArray count]>5; i --)   // remove excess default images
    {
        [imagesArray removeObjectAtIndex:i];
    }
    
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    self.tabBarController.tabBar.hidden = NO;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    self.tabBarController.tabBar.hidden = NO;
    
}


#pragma mark - text view delegates

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    
    if ([textView.text isEqualToString:@"enter listings description.."]) {
        textView.text = @"";
    }
    
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    if(textView.text.length == 0){
        textView.text = @"enter listings description..";
        [textView resignFirstResponder];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        if(textView.text.length == 0){
            textView.text = @"enter listings description..";
            [textView resignFirstResponder];
        }
        return NO;
    }
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    
    if(descTxtView == textView)
        descriptionString = descTxtView.text;
    
    
}


#pragma mark - KeyBoard Notifications

- (void)keyboardWillShow:(NSNotification*)aNotification {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.tableView.contentInset.top, 0.0, kbSize.height, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillHide:(NSNotification*)aNotification {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.35];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.tableView.contentInset.top, 0.0, 0.0, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    [UIView commitAnimations];
    
}

#pragma mark - text field delegates

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if(textField == nameTxt)
        nameString = nameTxt.text;
    else if(textField == addCityTxt)
        locationString = addCityTxt.text;
    else if(textField == addStateTxt)
        stateString = addStateTxt.text;
    else if(textField == addressZipTxt)
        zipString = addressZipTxt.text;
    else if(textField == sizeTxt)
        areaString=  sizeTxt.text;
    else if(textField == priceTxt)
        priceString = priceTxt.text;
    else if(textField == nearStationTxt)
        closestTransportString = nearStationTxt.text;
    else if(textField == nearMilesTxt)
        transportDistanceString = nearMilesTxt.text;
    else if(textField == bedRoomTx)
        bedRoomString = bedRoomTx.text;
    else if(textField == bathroomTxt)
        bathroomString = bathroomTxt.text;
    else if(textField == roommateTxt)
        roommatesString = roommateTxt.text;
    else if(textField == minStayTxt)
        minStayString = minStayTxt.text;
    else if(textField == deposteTxt)
        depositString = deposteTxt.text;
    
    
}

#pragma mark - TableView Delegate & Data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return showHiddenCell?25:24;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell;
    
    switch (indexPath.row) {
            
        case 0:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"imageCell" forIndexPath:indexPath];
            defaultImageSlots = 0;
            NSMutableArray * tempArray = [imagesArray mutableCopy];
            
            for (int i = (int)[tempArray count]; i<5; i++) {
                
                [tempArray addObject:[UIImage imageNamed:@"defaultImage"]];
                defaultImageSlots ++;
            }
            
            UIImageView *imgView1 = (UIImageView*)[cell viewWithTag:70];
            imgView1.image = tempArray[0];
            UIImageView *imgView2 = (UIImageView*)[cell viewWithTag:71];
            imgView2.image = tempArray[1];
            UIImageView *imgView3 = (UIImageView*)[cell viewWithTag:72];
            imgView3.image = tempArray[2];
            UIImageView *imgView4 = (UIImageView*)[cell viewWithTag:73];
            imgView4.image = tempArray[3];
            UIImageView *imgView5 = (UIImageView*)[cell viewWithTag:74];
            imgView5.image = tempArray[4];
            
            addImgButton = (UIButton*)[cell viewWithTag:75];
            addImgButton.hidden = !self.isAddingNewListing;
            if (defaultImageSlots == 0)
                addImgButton.hidden = YES;
            
            break;
            
        }
        case 1:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"nameCell" forIndexPath:indexPath];
            
            nameTxt = (UITextField *)[cell viewWithTag:76];
            nameTxt.delegate =self;
            nameTxt.userInteractionEnabled = self.isAddingNewListing;
            nameTxt.text = nameString;
            break;
            
        }
        case 2:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"descriptionCell" forIndexPath:indexPath];
            descTxtView = (UITextView *)[cell viewWithTag:77];
            descTxtView.delegate = self;
            descTxtView.userInteractionEnabled = self.isAddingNewListing;
            descTxtView.text = descriptionString?descriptionString:@"enter listings description..";
            break;
            
        }
        case 3:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"lookingForCell" forIndexPath:indexPath];
            cell.userInteractionEnabled = self.isAddingNewListing;
            self.tagView = (JCTagListView*)[cell viewWithTag:774];
            UILabel *lbl = (UILabel*) [cell viewWithTag:997];
            
            if([lookingForArray count]){
            [self configureTags];
            lbl.hidden = YES;
            }
            else{
            lbl.hidden = NO;
            }
            break;
            
        }
            
        case 4:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
            cell.separatorInset = UIEdgeInsetsMake(0.f, 0.f, 0.f, cell.bounds.size.width);
            break;
            
        }
        case 5:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"locationCell" forIndexPath:indexPath];
            addCityTxt = (UITextField *)[cell viewWithTag:90];
            addCityTxt.delegate= self;
            addCityTxt.userInteractionEnabled = self.isAddingNewListing;
            addCityTxt.text = locationString;
            cell.separatorInset = UIEdgeInsetsMake(0.f, 0.f, 0.f, cell.bounds.size.width);
            break;
            
        }
            
        case 6:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"stateCell" forIndexPath:indexPath];
            addStateTxt = (UITextField *)[cell viewWithTag:91];
            addStateTxt.userInteractionEnabled = self.isAddingNewListing;
            addStateTxt.delegate = self;
            addStateTxt.text = stateString;
            cell.separatorInset = UIEdgeInsetsMake(0.f, 0.f, 0.f, cell.bounds.size.width);
            break;
            
        }
            
        case 7:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"ZipCodeCell" forIndexPath:indexPath];
            addressZipTxt = (UITextField *)[cell viewWithTag:92];
            addressZipTxt.delegate =self;
            addressZipTxt.userInteractionEnabled = self.isAddingNewListing;
            addressZipTxt.text = zipString;
            break;
            
        }
        case 8:
        {
            
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"areaCell" forIndexPath:indexPath];
            sizeTxt = (UITextField *)[cell viewWithTag:83];
            sizeTxt.delegate =self;
            sizeTxt.userInteractionEnabled = self.isAddingNewListing;
            sizeTxt.text = areaString;
            break;
            
            
        }
        case 9:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"priceCell" forIndexPath:indexPath];
            priceTxt = (UITextField *)[cell viewWithTag:84];
            priceTxt.delegate =self;
            priceTxt.userInteractionEnabled = self.isAddingNewListing;
            priceTxt.text= priceString;
            break;
        }
        case 10:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"transportCell" forIndexPath:indexPath];
            nearStationTxt = (UITextField *)[cell viewWithTag:85];
            nearStationTxt.delegate =self;
            nearStationTxt.userInteractionEnabled =self.isAddingNewListing;
            nearStationTxt.text = closestTransportString;
            
            nearMilesTxt = (UITextField *)[cell viewWithTag:86];
            nearMilesTxt.delegate =self;
            nearMilesTxt.userInteractionEnabled = self.isAddingNewListing;
            nearMilesTxt.text = transportDistanceString;
            
            break;
            
        }
        case 11:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"typeCell" forIndexPath:indexPath];
            UISegmentedControl *segment = (UISegmentedControl*)[cell viewWithTag:790];
            segment.selectedSegmentIndex = [propertyTypeString integerValue];
            segment.userInteractionEnabled = self.isAddingNewListing;
            
            break;
                        
        }

        case 12:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"bhkCell" forIndexPath:indexPath];
            UISegmentedControl *segment = (UISegmentedControl*)[cell viewWithTag:778];
            segment.selectedSegmentIndex = [bedTypeString integerValue];
            segment.userInteractionEnabled = self.isAddingNewListing;
            
            break;
            
            
            
        }
        case 13:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"bedroomsCell" forIndexPath:indexPath];
            bedRoomTx = (UITextField *)[cell viewWithTag:87];
            bedRoomTx.delegate =self;
            bedRoomTx.userInteractionEnabled = self.isAddingNewListing;
            bedRoomTx.text = bedRoomString;
            break;
            
        }
        case 14:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"bathroomsCell" forIndexPath:indexPath];
            bathroomTxt = (UITextField *)[cell viewWithTag:88];
            bathroomTxt.delegate =self;
            bathroomTxt.userInteractionEnabled = self.isAddingNewListing;
            bathroomTxt.text= bathroomString;
            
            break;
            
        }
            
        case 15:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"roommateCell" forIndexPath:indexPath];
            roommateTxt = (UITextField *)[cell viewWithTag:777];
            roommateTxt.delegate =self;
            roommateTxt.userInteractionEnabled = self.isAddingNewListing;
            roommateTxt.text = roommatesString;
            
            break;
            
        }
        case 16:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"furnishCell" forIndexPath:indexPath];
            cell.userInteractionEnabled = self.isAddingNewListing;
            UITextField *field = (UITextField *)[cell viewWithTag:69];
            
            if(furnishStr)
                field.text = furnishStr;
            
            break;
            
        }
        case 17:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"amenitiesCell" forIndexPath:indexPath];
            UILabel *lbl = (UILabel *)[cell viewWithTag:89];
            lbl.text =[NSString stringWithFormat:@"%lu",(unsigned long)[amenityList count]];
            break;
            
        }
        case 18:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"restrictionsCell" forIndexPath:indexPath];
            UILabel *lbl = (UILabel *)[cell viewWithTag:78];
            lbl.text =[NSString stringWithFormat:@"%lu",(unsigned long)[restrictionList count]];
            break;
            
        }
        case 19:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"minStayCell" forIndexPath:indexPath];
            cell.userInteractionEnabled = self.isAddingNewListing;
            minStayTxt = (UITextField *)[cell viewWithTag:770];
            minStayTxt.delegate =self;
            minStayTxt.text = [minStayString isEqualToString:@""]?@"Not Mentioned":minStayString;
            
            break;
            
        }
        case 20:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"depositCell" forIndexPath:indexPath];
            cell.userInteractionEnabled = self.isAddingNewListing;
            deposteTxt = (UITextField *)[cell viewWithTag:771];
            deposteTxt.delegate =self;
            deposteTxt.text = depositString;
            
            break;
            
        }
        case 21:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"moveInCell" forIndexPath:indexPath];
            cell.userInteractionEnabled = self.isAddingNewListing;
            moveInButton = (UIButton *)[cell viewWithTag:772];
            [moveInButton setTitle:[moveInString isEqualToString:@""]?@"Not Mentioned":moveInString forState:UIControlStateNormal];
            
            break;
            
        }
        case 22:
        {
            if(showHiddenCell && showMoveInPicker)
            {
                cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"datePickerCell" forIndexPath:indexPath];
                cell.userInteractionEnabled = self.isAddingNewListing;
                datePicker =(UIDatePicker*)[cell viewWithTag:773];
            }
            else{
                
                cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"moveOutCell" forIndexPath:indexPath];
                cell.userInteractionEnabled = self.isAddingNewListing;
                moveOutButton = (UIButton *)[cell viewWithTag:962];
                [moveOutButton setTitle:[moveOutString isEqualToString:@""]?@"Not Mentioned":moveOutString forState:UIControlStateNormal];
            }
            
            
            break;
            
        }
            
        case 23:
        {
            if(showHiddenCell && showMoveInPicker)
            {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"moveOutCell" forIndexPath:indexPath];
            cell.userInteractionEnabled = self.isAddingNewListing;
            moveOutButton = (UIButton *)[cell viewWithTag:962];
            [moveOutButton setTitle:[moveOutString isEqualToString:@""]?@"Not Mentioned":moveOutString forState:UIControlStateNormal];
            }
            else if (showHiddenCell && !showMoveInPicker){
                cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"outDatePickerCell" forIndexPath:indexPath];
                cell.userInteractionEnabled = self.isAddingNewListing;
                outDatePicker =(UIDatePicker*)[cell viewWithTag:963];
            }
            else
            {
                cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"expiryCell" forIndexPath:indexPath];
                cell.userInteractionEnabled = NO;
                UILabel *lbl =(UILabel*)[cell viewWithTag:964];
                lbl.text = expiryString;

            }
            break;
            
        }
            case 24:
        {
            
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"expiryCell" forIndexPath:indexPath];
            cell.userInteractionEnabled = NO;
            UILabel *lbl =(UILabel*)[cell viewWithTag:964];
            lbl.text = expiryString;
            
            break;
        }
       

            
            
        default:
            break;
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
        {
            
            NSMutableArray *tempArray = [[NSMutableArray alloc]init];
            
            for (int i =0; i<[imagesArray count]; i++) {
                
                NSData *data1 = UIImagePNGRepresentation(imagesArray[i]);
                NSData *data2 = UIImagePNGRepresentation([UIImage imageNamed:@"defaultImage"]);
                
                if(![data1 isEqual:data2])
                {
                    [tempArray addObject:imagesArray[i]];
                }
                
            }
            
            
            NSMutableArray *photos = [[NSMutableArray alloc] init];
            MWPhoto *photo;
            
            for (int i =0; i<[tempArray count]; i++) {
                
                photo = [MWPhoto photoWithImage:tempArray[i]];
                
                
                [photos addObject:photo];
            }
            
            
            self.photos = photos;
            
            browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
            browser.displayActionButton = NO;
            browser.displayDeleteButton = self.isAddingNewListing;
            browser.displayNavArrows = YES;
            browser.displaySelectionButtons = NO;
            browser.alwaysShowControls = YES;
            browser.zoomPhotosToFill = YES;
            browser.enableGrid = NO;
            browser.startOnGrid = NO;
            browser.enableSwipeToDismiss = NO;
            browser.autoPlayOnAppear = NO;
            [browser setCurrentPhotoIndex:0];
            
            [self.navigationController pushViewController:browser animated:YES];
            break;
        }
      case 16:
        {
            
            NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
            alertViewController.title = @"";
            alertViewController.message = @"Select Furnish Information";
            alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleSlideFromBottom;
            alertViewController.buttonColor = [UIColor colorWithRed:86.0f/255.0f green:1.0f/255.0f blue:71.0f/255.0f alpha:0.90f];
            
            [alertViewController addAction:[NYAlertAction actionWithTitle:@"Fully Furnished"
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(NYAlertAction *action) {
                                                                      [self dismissViewControllerAnimated:YES completion:nil];
                                                                      furnishStr = @"Fully Furnished";
                                                                      [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:16 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                                                                      
                                                                  }]];
            
            [alertViewController addAction:[NYAlertAction actionWithTitle:@"Semi Furnished"
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(NYAlertAction *action) {
                                                                      [self dismissViewControllerAnimated:YES completion:nil];
                                                                      furnishStr = @"Semi Furnished";
                                                                      [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:16 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];                                                                  }]];
            [alertViewController addAction:[NYAlertAction actionWithTitle:@"No Furnish"
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(NYAlertAction *action) {
                                                                      [self dismissViewControllerAnimated:YES completion:nil];
                                                                      furnishStr = @"No Furnish";
                                                                      [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:16 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];                                                                                                                                      }]];
            
            [alertViewController addAction:[NYAlertAction actionWithTitle:@"Cancel"
                                                                    style:UIAlertActionStyleCancel
                                                                  handler:^(NYAlertAction *action) {
                                                                      [self dismissViewControllerAnimated:YES completion:nil];
                                                                  }]];
            
            
            [self presentViewController:alertViewController animated:YES completion:nil];
            
            
            break;
            
        }
        case 17:
        {
            
            [[NSNotificationCenter defaultCenter ]addObserver:self selector:@selector(updateAmenities:) name:@"selectedAmenities" object:nil];
            amenitiesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AmenitiesViewController"];
            amenitiesVC.type = @"Amenities";
            amenitiesVC.allowEditing = self.isAddingNewListing;
            NSMutableArray *array = [[NSMutableArray alloc]init];
            
            for (int i =0; i<[amenityList count]; i ++) {
                
                int row = [dataObject getIndexForAmenity:amenityList[i]];
                [array addObject:[NSIndexPath indexPathForRow:row inSection:0]];
            }
            
            amenitiesVC.selectedDatas= array;
            
            [self.navigationController pushViewController:amenitiesVC animated:YES];
            break;
        }
        case 18:
        {
            [[NSNotificationCenter defaultCenter ]addObserver:self selector:@selector(updateRestrictions:) name:@"selectedRestrictions" object:nil];
            amenitiesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AmenitiesViewController"];
            amenitiesVC.type = @"Restrictions";
            
            NSMutableArray *array = [[NSMutableArray alloc]init];
            for (int i =0; i<[restrictionList count]; i ++) {
                
                int row = [dataObject getIndexForRestriction:restrictionList[i]];
                [array addObject:[NSIndexPath indexPathForRow:row inSection:0]];
            }
            
            amenitiesVC.selectedDatas= array;
            amenitiesVC.allowEditing = self.isAddingNewListing;
            [self.navigationController pushViewController:amenitiesVC animated:YES];
            break;
        }
        default:
            break;
            
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    switch (indexPath.row) {
        case 0:
            return 310;
        case 1:
            return 85;
        case 2:
            return 185;
        case 3:
            return 235;
        case 10:
            return 85;
        case 11:
            return 103;
        case 12:
            return 103;
        case 22:
            if(showHiddenCell && showMoveInPicker)
            return  193;
            else
                return 46;
            case 23:
            if(showHiddenCell && !showMoveInPicker)
                return  193;
            else
                return 46;
            
            
        default:
            return 46;
    }
    
    
}


#pragma mark - ELCImagePickerControllerDelegate Methods

- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    NSMutableArray *images = [NSMutableArray arrayWithCapacity:[info count]];
    for (NSDictionary *dict in info) {
        if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypePhoto){
            if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
                UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
                [images addObject:image];
            } else {
               // //NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
            }
        } else if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypeVideo){
            if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
                UIImage* image=[dict objectForKey:UIImagePickerControllerOriginalImage];
                
                [images addObject:image];
                
            } else {
              //  //NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
            }
        } else {
           // //NSLog(@"Uknown asset type");
        }
    }
    
    for (int i =0; i <[images count]; i ++) {
        
        [imagesArray insertObject:images[i] atIndex:i];
    }
    
    int count = (int)[imagesArray count]-1;
    
    for(int i = count ; [imagesArray count]>5; i --)   // remove excess default images
    {
        [imagesArray removeObjectAtIndex:i];
    }
    
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser deleteButtonPressedForPhotoAtIndex:(NSUInteger)index
{
   // //NSLog(@"image deleted at index...%lu",(unsigned long)index);
    
    [imagesArray removeObjectAtIndex:index];
    
    NSMutableArray *photos = [[NSMutableArray alloc] init];
    MWPhoto *photo;
    
    for (int i =0; i<[imagesArray count]; i++) {
        
        photo = [MWPhoto photoWithImage:imagesArray[i]];
        [photos addObject:photo];
        
    }
    
    self.photos = photos;
    [browser reloadData];
    
}

- (void)photoBrowserWillDismissPresentation:(MWPhotoBrowser *)photoBrowser
{
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}

@end
