//
//  AppDelegate.m
//  letSub
//
//  Created by Vishnu Varthan .P on 04/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "AppDelegate.h"
#import "OTPAuthViewController.h"
#import "MessagesListViewController.h"
static MBProgressHUD *HUD = nil;
@interface AppDelegate ()
{
    UIViewController *menuVC;
    UITabBarController *mainVC;
}
@end

@implementation AppDelegate
@synthesize slideMenuVC;

+(AppDelegate *)instance{
    
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
    
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
   
    [self registerForRemoteNotification];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    [self performSelector:@selector(updateInterfaceWithReachability:) withObject:self.internetReachability afterDelay:02 ];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];

    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    slideMenuVC = [[HKSlideMenu3DController alloc] init];
    slideMenuVC.view.frame =  [[UIScreen mainScreen] bounds];
    
    menuVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SlideMenuViewController"];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"loginScenes" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"LoginViewController"];
    slideMenuVC.mainViewController=vc;
    self.slideMenuVC.menuViewController = menuVC;
    
    slideMenuVC.backgroundImageContentMode = UIViewContentModeTopLeft;
    slideMenuVC.enablePan = NO;
    [self.window setRootViewController:slideMenuVC];
    [self.window makeKeyAndVisible];

    
    [[UINavigationBar appearance]setBarTintColor:[UIColor colorWithRed:66.0f/255.0f green:28.0f/255.0f blue:82.0f/255.0f alpha:1.0f]];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    [[UINavigationBar appearance] setBarStyle:UIBarStyleBlack];
    [[UITabBar appearance] setTranslucent: NO];
    [[UINavigationBar appearance]setTranslucent:NO];
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:66.0f/255.0f green:28.0f/255.0f blue:82.0f/255.0f alpha:1.0f]];
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTintColor:[UIColor whiteColor]];
    
    
    [[NSUserDefaults standardUserDefaults]synchronize];

    self.userIntrestArray = [[NSMutableArray alloc]init];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation
            ];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
    application.applicationIconBadgeNumber = 0;
   // NSLog( @"### running FB sdk version: %@", [FBSDKSettings sdkVersion] );
}

- (void)applicationWillTerminate:(UIApplication *)application {
       [self saveContext];
}


#pragma mark - APNS


- (void)registerForRemoteNotification
{
            UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:
                                                    UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
        //register to receive notifications
    if(notificationSettings.types!=UIUserNotificationTypeNone)
        [application registerForRemoteNotifications];
    
}


-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    
        
       // NSLog(@"Remote notification received with info... :%@", userInfo);
    
    NSString *apnsType = [[userInfo objectForKey:@"aps"]objectForKey:@"type"];
    
    if([apnsType isEqualToString:@"newMessage"])
    {
        MessagesListViewController *messVC =[[MessagesListViewController alloc]init];
        [messVC checkForNewMessages];
       // tabBarController.viewControllers[1].tabBarItem.badgeValue = "1";
        
        UITabBarController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"TabBarScene"];
        [[vc.viewControllers objectAtIndex:3]tabBarItem].badgeValue = @"1";
    }
    
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    
        
        NSString *deviceTokenString = [[[deviceToken description]
                                        stringByReplacingOccurrencesOfString: @"<" withString: @"&lt;"]
                                       stringByReplacingOccurrencesOfString: @">" withString: @"&gt;"];
        
        
       // NSLog(@"APNS token String for the device is :%@",deviceTokenString);
        self.apnsTokenString = deviceTokenString;
   }

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
       // NSLog(@"Failed to get APNS token with error: %@", error);
}

-(NSString *)apnsDeviceToken
{
    return self.apnsTokenString;
}



#pragma mark - Network Rechability

- (void) reachabilityChanged:(NSNotification *)note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [self updateInterfaceWithReachability:curReach];
}

- (void)updateInterfaceWithReachability:(Reachability *)reachability
{
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    
    switch (netStatus)
    {
        case NotReachable:        {
            
            self.isReachable = NO;
            
            break;
        }
            
        case ReachableViaWWAN:        {
            
            self.isReachable =YES;
            break;
        }
        case ReachableViaWiFi:        {
            
            self.isReachable = YES;
            break;
        }
    }
    
    
    if(self.isReachable)
    {
        [[TWMessageBarManager sharedInstance] hideAllAnimated:YES];
    }
    else
    {
        [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Network Unavailable !!"
                                                       description:@"Your device appears to have lost internet connection, Try reconnecting"
                                                              type:TWMessageBarMessageTypeNetworkUnavailable duration:5.0f];

    }

}

-(BOOL)isConnectionAvailable
{
    return self.isReachable;
    
}

#pragma mark- Menu Navigation

-(void)openMyListing
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"MyListingViewController"];
    UINavigationController *navigationController =
    [[UINavigationController alloc] initWithRootViewController:vc];
    slideMenuVC.mainViewController=navigationController;
    self.slideMenuVC.menuViewController = menuVC;
    
}

-(void)openLoginPage
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"loginScenes" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"LoginViewController"];
    slideMenuVC.mainViewController=vc;
    self.slideMenuVC.menuViewController = menuVC;
}
-(void)openProfileView
{
    UITabBarController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"TabBarScene"];
    [vc setSelectedIndex:4];
    slideMenuVC.mainViewController=vc;
    self.slideMenuVC.menuViewController = menuVC;
}

-(void)openAbout{
    
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"AboutViewController"];
    UINavigationController *navigationController =
    [[UINavigationController alloc] initWithRootViewController:vc];
    slideMenuVC.mainViewController=navigationController;
    self.slideMenuVC.menuViewController = menuVC;
}

-(void)openSupport
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SupportViewController"];
    UINavigationController *navigationController =
    [[UINavigationController alloc] initWithRootViewController:vc];
    slideMenuVC.mainViewController=navigationController;
    self.slideMenuVC.menuViewController = menuVC;
}

-(void)openHomeView
{
    UITabBarController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"TabBarScene"];
    [vc setSelectedIndex:2];
    slideMenuVC.mainViewController=vc;
    self.slideMenuVC.menuViewController = menuVC;
}

-(void)openFeedView
{
    UITabBarController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"TabBarScene"];
    [vc setSelectedIndex:0];
    slideMenuVC.mainViewController=vc;
    self.slideMenuVC.menuViewController = menuVC;
}

-(void)openFavoriteView
{
    UITabBarController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"TabBarScene"];
    [vc setSelectedIndex:1];
    slideMenuVC.mainViewController=vc;
    self.slideMenuVC.menuViewController = menuVC;
}

-(void)openChatView
{
    UITabBarController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"TabBarScene"];
    [vc setSelectedIndex:3];
    slideMenuVC.mainViewController=vc;
    self.slideMenuVC.menuViewController = menuVC;
}

-(void)openRegistrationPage{
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"loginScenes" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    slideMenuVC.mainViewController=vc;
    self.slideMenuVC.menuViewController = menuVC;
    
}

-(void)openPasswordChangePage{
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"loginScenes" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"PasswordChangeViewController"];
    slideMenuVC.mainViewController=vc;
    self.slideMenuVC.menuViewController = menuVC;
    
}

-(void)openForgotPassword{
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"loginScenes" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"ForgotPasswordViewController"];
    slideMenuVC.mainViewController=vc;
    self.slideMenuVC.menuViewController = menuVC;
    
}

-(void)openUserAuthenticationPageWithParameters :(NSDictionary*)dict
{
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"loginScenes" bundle:nil];

    OTPAuthViewController *vc = [sb instantiateViewControllerWithIdentifier:@"OTPAuthViewController"];
    vc.paramDictionary = [dict mutableCopy];
    slideMenuVC.mainViewController=vc;
    self.slideMenuVC.menuViewController = menuVC;

    
}

#pragma mark - LoadingHUDView

+ (void)showLoadingView:(UIViewController *)viewController
{
        [self showLoadingView:viewController withHeading:@"Loading" andText:@"Please wait.."];
}

+ (void)showLoadingView:(UIViewController *)viewController withHeading:(NSString*)heading andText:(NSString*)text
{
    
    [self hideLoadingIndicator];
    
    [HUD removeFromSuperview];
    HUD = [[MBProgressHUD alloc] initWithView:viewController.view];
    HUD.labelText = heading;
    HUD.labelFont = [UIFont fontWithName:@"Arial" size:13];
    HUD.detailsLabelText = text;
    HUD.dimBackground = YES;
    HUD.mode = MBProgressHUDModeIndeterminate;
    [viewController.view addSubview:HUD];
    [HUD show:YES];
    
}


+ (void)hideLoadingIndicator
{
    
    [HUD hide:YES];
    
}


#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.e2infosystems.iBall" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        
       // //NSLog(@"error while saving/adding coredata %@",error);
        
        }
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

-(NSMutableArray*)fetchPersistentDataforEntity:(NSString *)entityName
{
    
    NSError *error = nil;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName
                                              inManagedObjectContext: [self managedObjectContext]];
    
    [fetchRequest setEntity:entity];
    
    NSMutableArray *mutableFetchResults = [[[self managedObjectContext]
                                            executeFetchRequest:fetchRequest error:&error] mutableCopy];
    
    NSMutableArray *resultArray=[[NSMutableArray alloc]initWithArray:mutableFetchResults];
    
    return resultArray;
    
    
}

- (void)deleteAllEntities:(NSString *)nameEntity
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:nameEntity];
    [fetchRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError *error;
    NSArray *fetchedObjects = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject *object in fetchedObjects)
    {
        [[self managedObjectContext] deleteObject:object];
    }
    
    error = nil;
    [[self managedObjectContext] save:&error];
}


#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
           // //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            // abort();
        }
    }
}

@end
