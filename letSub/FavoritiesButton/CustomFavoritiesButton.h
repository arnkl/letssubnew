//
//  CustomFavoritiesButton.h
//  letSub
//
//  Created by Vishnu Varthan .P on 29/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomFavoritiesButton : UIButton
@property (nonatomic) int index;
@end
