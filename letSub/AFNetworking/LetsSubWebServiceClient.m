//
//  LetsSubWebServiceClient.m
//  letSub
//
//  Created by Vishnu Varthan .P on 02/05/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "LetsSubWebServiceClient.h"


@implementation LetsSubWebServiceClient

static NSString *kBaseUrl = @"http://operations.letssub.com/";
 //static NSString *kBaseUrl = @"http://139.59.8.42/letssubserver/";

+ (instancetype)sharedClient
{
    
    static id instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] initWithBaseURL:[NSURL URLWithString:kBaseUrl]];
    });
    return instance;
    
}

- (instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
      //  self.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    
    return self;
}
@end
