//
//  LetsSubWebServiceClient.h
//  letSub
//
//  Created by Vishnu Varthan .P on 02/05/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "AFHTTPSessionManager.h"

@interface LetsSubWebServiceClient : AFHTTPSessionManager

+ (instancetype)sharedClient;

@end
