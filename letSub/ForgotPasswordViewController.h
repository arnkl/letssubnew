//
//  ForgotPasswordViewController.h
//  letSub
//
//  Created by hmspl on 07/08/16.
//  Copyright © 2016 com.vishnuvarthan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *emailTxtField;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPassword;

@end
