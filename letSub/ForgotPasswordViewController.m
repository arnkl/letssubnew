//
//  ForgotPasswordViewController.m
//  letSub
//
//  Created by hmspl on 07/08/16.
//  Copyright © 2016 com.vishnuvarthan. All rights reserved.
//

#import "ForgotPasswordViewController.h"

@interface ForgotPasswordViewController ()<UITextFieldDelegate>

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.emailTxtField.delegate =self;
    
    
}



-(void)viewDidAppear:(BOOL)animated
{
    [[_btnForgotPassword layer] setCornerRadius:20.0];
    [[_btnForgotPassword layer] setBorderColor:(__bridge CGColorRef _Nullable)([UIColor clearColor])];
    [[_btnForgotPassword layer] setBorderWidth:0.1];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = _btnForgotPassword.bounds;
    [gradient setBorderWidth:0.1];
    [gradient setCornerRadius:20.0];
    [gradient setStartPoint:CGPointMake(0.0, 0.5)];
    [gradient setEndPoint:CGPointMake(1.0, 0.5)];
    [gradient setBorderColor:(__bridge CGColorRef _Nullable)([UIColor clearColor])];
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:0.267 green:0.176 blue:0.996 alpha:1.000] CGColor], (id)[[UIColor colorWithRed:0.467 green:0.000 blue:0.522 alpha:1.000] CGColor], nil];
    [_btnForgotPassword.layer insertSublayer:gradient atIndex:0];
}

- (IBAction)cancelRegistration:(id)sender {
    
    [[AppDelegate instance]openLoginPage];
}

- (IBAction)sendNewPassword:(id)sender {
    
    if([self.emailTxtField.text length]!=0)
    {
        [AppDelegate showLoadingView:self];
        
        NSString *urlAddress = [NSString stringWithFormat:@"users/forgotPassword.json"];
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:_emailTxtField.text,@"emailID",nil];
        
        LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
        
        [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            //NSLog(@"JSON: %@", responseObject);
            responseObject = [responseObject objectForKey:@"response"];
            NSString *status = [responseObject objectForKey:@"status"];
            [AppDelegate hideLoadingIndicator];
            if([status boolValue]==1)
            {
                [self showAlertWithMessage:@"New Password sent to your email."];
                
            }
            else{
                
                [self showAlertWithMessage:[responseObject objectForKey:@"message"]];
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            //NSLog(@"Error: %@", error);
            [AppDelegate hideLoadingIndicator];
        }];
    }
    else{
        
        [self showAlertWithMessage:@"Enter your registered email."];
        
    }
}

-(void)showAlertWithMessage :(NSString*)message
{
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"Error";
    alertViewController.message = message;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    alertViewController.buttonColor = [UIColor colorWithRed:86.0f/255.0f green:1.0f/255.0f blue:71.0f/255.0f alpha:0.90f];
    
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Ok"
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                          }]];
    
    
    [self presentViewController:alertViewController animated:YES completion:nil];
    
    
}


-(BOOL)isFieldsFilled
{
    bool fieldsFilled = ([self.emailTxtField.text length]!=0) ;
    
    return fieldsFilled;
}

#pragma mark - Text field delegates
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    return YES;
}
@end
