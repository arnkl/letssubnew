//
//  ChatViewController.m
//  letSub
//
//  Created by Vishnu Varthan .P on 18/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "ChatViewController.h"
#import "ListerInfoViewController.h"
#import "UINavigationController+M13ProgressViewBar.h"

@interface ChatViewController ()

@end

@implementation ChatViewController

#pragma mark - View lifecycle

/**
 *  Override point for customization.
 *
 *  Customize your view.
 *  Look at the properties on `JSQMessagesViewController` and `JSQMessagesCollectionView` to see what is possible.
 *
 *  Customize your layout.
 *  Look at the properties on `JSQMessagesCollectionViewFlowLayout` to see what is possible.
 */
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.senderId = [[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"];
    self.senderDisplayName = [[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserName"];
    
    self.inputToolbar.contentView.textView.pasteDelegate = self;
    
    self.modelData = [[MessageDataModel alloc]init];
    [self.modelData LoadmessagesforuserID:self.messageID];
    
    /**
     *  You can set custom avatar sizes
     */
       self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
        self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
    
    self.showLoadEarlierMessagesHeader = NO;
    self.collectionView.collectionViewLayout.springinessEnabled = NO;

    //    /**
    //     *  Register custom menu actions for cells.
    //     */
    //    [JSQMessagesCollectionViewCell registerMenuAction:@selector(customAction:)];
    //    [UIMenuController sharedMenuController].menuItems = @[ [[UIMenuItem alloc] initWithTitle:@"Custom Action"
    //                                                                                      action:@selector(customAction:)] ];
    
    /**
     *  OPT-IN: allow cells to be deleted
     */
    //   [JSQMessagesCollectionViewCell registerMenuAction:@selector(delete:)];
    
    /**
     *  Customize your toolbar buttons
     *
     *  self.inputToolbar.contentView.leftBarButtonItem = custom button or nil to remove
     *  self.inputToolbar.contentView.rightBarButtonItem = custom button or nil to remove
     */
    
    /**
     *  Set a maximum height for the input toolbar
     *
     *  self.inputToolbar.maximumHeight = 150;
     */
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.title = self.senderName;
    UIImage *faceImage = _senderImage;
    UIButton *face = [UIButton buttonWithType:UIButtonTypeCustom];
    face.bounds = CGRectMake( 0, 0, 40, 40);
    face.layer.cornerRadius = 20;
    face.layer.masksToBounds =YES;
    [face setBackgroundImage:faceImage forState:UIControlStateNormal];
    [face addTarget:self action:@selector(showUserProfile:) forControlEvents:UIControlEventTouchUpInside];
    [face setContentMode:UIViewContentModeScaleAspectFill];
    UIBarButtonItem *faceBtn = [[UIBarButtonItem alloc] initWithCustomView:face];
    [self navigationItem].rightBarButtonItem = faceBtn;
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleDone target:self action:@selector(closeChatView)];
    [leftButton setImage:[UIImage imageNamed:@"back"]];
    [self navigationItem].leftBarButtonItem = leftButton;
    
}

-(void)closeChatView
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)showUserProfile:(id)sender
{
    ListerInfoViewController *lister = [self.storyboard instantiateViewControllerWithIdentifier:@"ListerInfoViewController"];
    lister.listerName = _senderName;
    lister.listerId = _messageID;
    lister.hideMessageCell = YES;
    [self.navigationController pushViewController:lister animated:YES];
}


-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController finishProgress];  // stop the sending message progress bar on navigation bar when view is about to be dismissed.
    [super viewWillDisappear:YES];
}


//- (void)receiveMessagePressed:(UIBarButtonItem *)sender
//{
//    /**
//     *  DEMO ONLY
//     *
//     *  The following is simply to simulate received messages for the demo.
//     *  Do not actually do this.
//     */
//
//
//    /**
//     *  Show the typing indicator to be shown
//     */
//    self.showTypingIndicator = !self.showTypingIndicator;
//
//    /**
//     *  Scroll to actually view the indicator
//     */
//    [self scrollToBottomAnimated:YES];
//
//    /**
//     *  Copy last sent message, this will be the new "received" message
//     */
//    JSQMessage *copyMessage = [[self.demoData.messages lastObject] copy];
//
//    if (!copyMessage) {
//        copyMessage = [JSQMessage messageWithSenderId:kJSQDemoAvatarIdJobs
//                                          displayName:kJSQDemoAvatarDisplayNameJobs
//                                                 text:@"First received!"];
//    }
//
//    /**
//     *  Allow typing indicator to show
//     */
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//
//        NSMutableArray *userIds = [[self.demoData.users allKeys] mutableCopy];
//        [userIds removeObject:self.senderId];
//        NSString *randomUserId = userIds[arc4random_uniform((int)[userIds count])];
//
//        JSQMessage *newMessage = nil;
//        id<JSQMessageMediaData> newMediaData = nil;
//        id newMediaAttachmentCopy = nil;
//
//        if (copyMessage.isMediaMessage) {
//            /**
//             *  Last message was a media message
//             */
//            id<JSQMessageMediaData> copyMediaData = copyMessage.media;
//
//            if ([copyMediaData isKindOfClass:[JSQPhotoMediaItem class]]) {
//                JSQPhotoMediaItem *photoItemCopy = [((JSQPhotoMediaItem *)copyMediaData) copy];
//                photoItemCopy.appliesMediaViewMaskAsOutgoing = NO;
//                newMediaAttachmentCopy = [UIImage imageWithCGImage:photoItemCopy.image.CGImage];
//
//                /**
//                 *  Set image to nil to simulate "downloading" the image
//                 *  and show the placeholder view
//                 */
//                photoItemCopy.image = nil;
//
//                newMediaData = photoItemCopy;
//            }
//            else if ([copyMediaData isKindOfClass:[JSQLocationMediaItem class]]) {
//                JSQLocationMediaItem *locationItemCopy = [((JSQLocationMediaItem *)copyMediaData) copy];
//                locationItemCopy.appliesMediaViewMaskAsOutgoing = NO;
//                newMediaAttachmentCopy = [locationItemCopy.location copy];
//
//                /**
//                 *  Set location to nil to simulate "downloading" the location data
//                 */
//                locationItemCopy.location = nil;
//
//                newMediaData = locationItemCopy;
//            }
//            else if ([copyMediaData isKindOfClass:[JSQVideoMediaItem class]]) {
//                JSQVideoMediaItem *videoItemCopy = [((JSQVideoMediaItem *)copyMediaData) copy];
//                videoItemCopy.appliesMediaViewMaskAsOutgoing = NO;
//                newMediaAttachmentCopy = [videoItemCopy.fileURL copy];
//
//                /**
//                 *  Reset video item to simulate "downloading" the video
//                 */
//                videoItemCopy.fileURL = nil;
//                videoItemCopy.isReadyToPlay = NO;
//
//                newMediaData = videoItemCopy;
//            }
//            else {
//                //NSLog(@"%s error: unrecognized media item", __PRETTY_FUNCTION__);
//            }
//
//            newMessage = [JSQMessage messageWithSenderId:randomUserId
//                                             displayName:self.demoData.users[randomUserId]
//                                                   media:newMediaData];
//        }
//        else {
//            /**
//             *  Last message was a text message
//             */
//            newMessage = [JSQMessage messageWithSenderId:randomUserId
//                                             displayName:self.demoData.users[randomUserId]
//                                                    text:copyMessage.text];
//        }
//
//        /**
//         *  Upon receiving a message, you should:
//         *
//         *  1. Play sound (optional)
//         *  2. Add new id<JSQMessageData> object to your data source
//         *  3. Call `finishReceivingMessage`
//         */
//        [JSQSystemSoundPlayer jsq_playMessageReceivedSound];
//        [self.demoData.messages addObject:newMessage];
//        [self finishReceivingMessageAnimated:YES];
//
//
//        if (newMessage.isMediaMessage) {
//            /**
//             *  Simulate "downloading" media
//             */
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                /**
//                 *  Media is "finished downloading", re-display visible cells
//                 *
//                 *  If media cell is not visible, the next time it is dequeued the view controller will display its new attachment data
//                 *
//                 *  Reload the specific item, or simply call `reloadData`
//                 */
//
//                if ([newMediaData isKindOfClass:[JSQPhotoMediaItem class]]) {
//                    ((JSQPhotoMediaItem *)newMediaData).image = newMediaAttachmentCopy;
//                    [self.collectionView reloadData];
//                }
//                else if ([newMediaData isKindOfClass:[JSQLocationMediaItem class]]) {
//                    [((JSQLocationMediaItem *)newMediaData)setLocation:newMediaAttachmentCopy withCompletionHandler:^{
//                        [self.collectionView reloadData];
//                    }];
//                }
//                else if ([newMediaData isKindOfClass:[JSQVideoMediaItem class]]) {
//                    ((JSQVideoMediaItem *)newMediaData).fileURL = newMediaAttachmentCopy;
//                    ((JSQVideoMediaItem *)newMediaData).isReadyToPlay = YES;
//                    [self.collectionView reloadData];
//                }
//                else {
//                    //NSLog(@"%s error: unrecognized media item", __PRETTY_FUNCTION__);
//                }
//
//            });
//        }
//
//    });
//}
//
//- (void)closePressed:(UIBarButtonItem *)sender
//{
//    //[self.delegateModal didDismissJSQDemoViewController:self];
//}
//
//


#pragma mark - JSQMessagesViewController method overrides

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date
{
    

    self.navigationItem.leftBarButtonItem.enabled = NO;
    
    self.inputToolbar.contentView.rightBarButtonItem.enabled = NO;
    
    [self.navigationController showProgress];
    [self.navigationController setPrimaryColor:[UIColor orangeColor]];
     [self.navigationController setProgress:.25 animated:YES];
    NSMutableArray *resultArray=[[AppDelegate instance] fetchPersistentDataforEntity:@"Messages"];
    NSMutableDictionary * coreDataDict = [[NSMutableDictionary alloc]init];
    
    if([resultArray count] !=0)
    {
        messageModelObj =(Messages*)resultArray[0];
        coreDataDict = [messageModelObj messagesDict];
        
    }
    else
    {
        messageModelObj=(Messages*)[NSEntityDescription insertNewObjectForEntityForName:@"Messages" inManagedObjectContext:[[AppDelegate instance] managedObjectContext]];
    }

    
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }

    
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"fromUserID",text,@"message",_messageID,@"toUserID", [[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email",nil];
        NSString *urlAddress = [NSString stringWithFormat:@"messages/sendMessage.json"];
        LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
        [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    
            [self.navigationController setProgress:.50 animated:YES];
          //  //NSLog(@"JSON: %@", responseObject);
    
            JSQMessage *message = [[JSQMessage alloc] initWithSenderId:senderId
                                                     senderDisplayName:senderDisplayName
                                                                  date:date
                                                                  text:text status:@"sent"];
            
            
            [self.modelData.messagesList addObject:message];
            [self finishSendingMessageAnimated:YES];
            NSMutableDictionary * messagesDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.senderName,@"senderName",senderId,@"senderID",date,@"date",text,@"message",_senderImgURL,@"senderImage",@"sent",@"status",nil];
            
            NSMutableArray *array = [[NSMutableArray alloc]init];
            
            if([coreDataDict objectForKey:self.messageID])
                array = [[coreDataDict objectForKey:self.messageID]mutableCopy];
            
            [array addObject:messagesDictionary];
            [coreDataDict removeObjectForKey:self.messageID];
            [coreDataDict setObject:array forKey:self.messageID];
            
            [messageModelObj setMessagesDict:coreDataDict];
            [[AppDelegate instance] saveContext];
            [JSQSystemSoundPlayer jsq_playMessageSentSound];
            self.navigationItem.leftBarButtonItem.enabled = YES;
             [self.navigationController finishProgress];
    
        } failure:^(NSURLSessionTask *operation, NSError *error) {
           // //NSLog(@"Error: %@", error);
            [self.navigationController setProgress:.50 animated:YES];
            
            JSQMessage *message = [[JSQMessage alloc] initWithSenderId:senderId
                                                     senderDisplayName:senderDisplayName
                                                                  date:date
                                                                  text:text status:@"failed"];
            
            
            [self.modelData.messagesList addObject:message];
            [self finishSendingMessageAnimated:YES];
            
            NSMutableDictionary * messagesDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.senderName,@"senderName",senderId,@"senderID",date,@"date",text,@"message",_senderImgURL,@"senderImage",@"failed",@"status",nil];
            
            NSMutableArray *array = [[NSMutableArray alloc]init];
            
            if([coreDataDict objectForKey:self.messageID])
                array = [[coreDataDict objectForKey:self.messageID]mutableCopy];
            
            [array addObject:messagesDictionary];
            [coreDataDict removeObjectForKey:self.messageID];
            [coreDataDict setObject:array forKey:self.messageID];
            
            [messageModelObj setMessagesDict:coreDataDict];
            [[AppDelegate instance] saveContext];
            [JSQSystemSoundPlayer jsq_playMessageSentSound];
            [AppDelegate hideLoadingIndicator];
            self.navigationItem.leftBarButtonItem.enabled = YES;
            [self.navigationController finishProgress];
            
        }];

}

#pragma mark - JSQMessages CollectionView DataSource

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.modelData.messagesList  objectAtIndex:indexPath.item];
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didDeleteMessageAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.modelData.messagesList removeObjectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  You may return nil here if you do not want bubbles.
     *  In this case, you should set the background color of your collection view cell's textView.
     *
     *  Otherwise, return your previously created bubble image data objects.
     */
    
    
    JSQMessage *message = [self.modelData.messagesList objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.modelData.outgoingBubbleImageData;
    }
    
    return self.modelData.incomingBubbleImageData;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Return `nil` here if you do not want avatars.
     *  If you do return `nil`, be sure to do the following in `viewDidLoad`:
     *
     *  self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
     *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
     *
     *  It is possible to have only outgoing avatars or only incoming avatars, too.
     */
    
    /**
     *  Return your previously created avatar image data objects.
     *
     *  Note: these the avatars will be sized according to these values:
     *
     *  self.collectionView.collectionViewLayout.incomingAvatarViewSize
     *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize
     *
     *  Override the defaults in `viewDidLoad`
     */
//    JSQMessage *message = [self.modelData.messagesList objectAtIndex:indexPath.item];
//    
////    if ([message.senderId isEqualToString:self.senderId]) {
////        if (![NSUserDefaults outgoingAvatarSetting]) {
////            return nil;
////        }
////    }
////    else {
////        if (![NSUserDefaults incomingAvatarSetting]) {
////            return nil;
////        }
////    }
////    
//    
//    return [self.modelData.avatars objectForKey:message.senderId];
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
     *  The other label text delegate methods should follow a similar pattern.
     *
     *  Show a timestamp for every 3rd message
     */
    if (indexPath.item % 3 == 0) {
        JSQMessage *message = [self.modelData.messagesList  objectAtIndex:indexPath.item];
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    }
    
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    
    JSQMessage *message = [self.modelData.messagesList  objectAtIndex:indexPath.item];
    
    /**
     *  iOS7-style sender name labels
     */
    if ([message.senderId isEqualToString:self.senderId]) {
        return nil;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.modelData.messagesList  objectAtIndex:indexPath.item -1];
        if ([[previousMessage senderId] isEqualToString:message.senderId]) {
            return nil;
        }
    }
    
    /**
     *  Don't specify attributes to use the defaults.
     */
    return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
   
    JSQMessage *msg = [self.modelData.messagesList objectAtIndex:indexPath.item];
   
    if ([msg.senderId isEqualToString:self.senderId]) {
       
        UIColor * color;
        NSString * string;
        
        if([msg.messageStatus isEqualToString:@"failed"])
        {
        color = [UIColor redColor]; // select needed color
        string = @"failed"; // the string to colorize
         }
        else
            return nil;
        
    NSDictionary *attrs = @{ NSForegroundColorAttributeName : color };
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:string attributes:attrs];

        return attrStr;
    }
    else {
        return nil  ;
    }
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.modelData.messagesList count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Override point for customizing cells
     */
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    /**
     *  Configure almost *anything* on the cell
     *
     *  Text colors, label text, label colors, etc.
     *
     *
     *  DO NOT set `cell.textView.font` !
     *  Instead, you need to set `self.collectionView.collectionViewLayout.messageBubbleFont` to the font you want in `viewDidLoad`
     *
     *
     *  DO NOT manipulate cell layout information!
     *  Instead, override the properties you want on `self.collectionView.collectionViewLayout` from `viewDidLoad`
     */
    
    
    JSQMessage *msg = [self.modelData.messagesList objectAtIndex:indexPath.item];
    
    if (!msg.isMediaMessage) {
        
        if ([msg.senderId isEqualToString:self.senderId]) {
             cell.textView.textColor = [UIColor colorWithRed:0.871 green:0.851 blue:0.902 alpha:1.000];
        }
        else {
           cell.textView.textColor = [UIColor colorWithRed:0.106 green:0.106 blue:0.200 alpha:1.000];
        }
        
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }
    
    return cell;
}



#pragma mark - UICollectionView Delegate

#pragma mark - Custom menu items

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    //    if (action == @selector(customAction:)) {
    //        return YES;
    //    }
    
    return [super collectionView:collectionView canPerformAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    //    if (action == @selector(customAction:)) {
    //        [self customAction:sender];
    //        return;
    //    }
    
    [super collectionView:collectionView performAction:action forItemAtIndexPath:indexPath withSender:sender];
}

//- (void)customAction:(id)sender
//{
//    //NSLog(@"Custom action received! Sender: %@", sender);
//
//    [[[UIAlertView alloc] initWithTitle:@"Custom Action"
//                                message:nil
//                               delegate:nil
//                      cancelButtonTitle:@"OK"
//                      otherButtonTitles:nil]
//     show];
//}
//


#pragma mark - JSQMessages collection view flow layout delegate

#pragma mark - Adjusting cell label heights

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
     */
    
    /**
     *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
     *  The other label height delegate methods should follow similarly
     *
     *  Show a timestamp for every 3rd message
     */
    if (indexPath.item % 3 == 0) {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  iOS7-style sender name labels
     */
    JSQMessage *currentMessage= [self.modelData.messagesList  objectAtIndex:indexPath.item];
    if ([[currentMessage senderId] isEqualToString:self.senderId]) {
        return 0.0f;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage= [self.modelData.messagesList objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:[currentMessage senderId]]) {
            return 0.0f;
        }
    }
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    
    JSQMessage *msg = [self.modelData.messagesList objectAtIndex:indexPath.item];
    if ([msg.senderId isEqualToString:self.senderId]) {
        return 15.0f;
        }
        else {
            return 0.0f  ;
        }

}

#pragma mark - Responding to collection view tap events

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
                header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender
{
   // //NSLog(@"Load earlier messages!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView atIndexPath:(NSIndexPath *)indexPath
{
   // //NSLog(@"Tapped avatar!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath
{
  //  //NSLog(@"Tapped message bubble!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapCellAtIndexPath:(NSIndexPath *)indexPath touchLocation:(CGPoint)touchLocation
{
   // //NSLog(@"Tapped cell at %@!", NSStringFromCGPoint(touchLocation));
}

#pragma mark - JSQMessagesComposerTextViewPasteDelegate methods


- (BOOL)composerTextView:(JSQMessagesComposerTextView *)textView shouldPasteWithSender:(id)sender
{
    if ([UIPasteboard generalPasteboard].image) {
        // If there's an image in the pasteboard, construct a media item with that image and `send` it.
        JSQPhotoMediaItem *item = [[JSQPhotoMediaItem alloc] initWithImage:[UIPasteboard generalPasteboard].image];
        JSQMessage *message = [[JSQMessage alloc] initWithSenderId:self.senderId
                                                 senderDisplayName:self.senderDisplayName
                                                              date:[NSDate date]
                                                             media:item];
        [self.modelData.messagesList addObject:message ];
        [self finishSendingMessage];
        return NO;
    }
    return YES;
}

@end
