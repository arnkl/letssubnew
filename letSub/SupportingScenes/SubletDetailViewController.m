//
//  SubletDetailViewController.m
//  letSub
//
//  Created by Vishnu Varthan .P on 08/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "SubletDetailViewController.h"

@interface SubletDetailViewController ()
{
    NSString *stateString,*zipcodeString,*areaString,*bathroomString,*bedRoomString,*closestTransportString,*transportDistanceString,*descriptionString,*furnishString,*cityString,*nameString,*priceString,*roommatesString,*moveInString,*moveOutString,*minStayString,*depositString,*bedTypeString,*propertyTypeString;
    NSMutableDictionary *listerInfoDict;
    NSMutableArray *imagesListArray,*lookingForArray;

}

@end

@implementation SubletDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    listerInfoDict = [[NSMutableDictionary alloc]init];
    imagesListArray = [[NSMutableArray alloc]init];

    amenityList = [[NSMutableArray alloc]init];
    restrictionList = [[NSMutableArray alloc]init];
    lookingForArray = [[NSMutableArray alloc]init];
    
  
    [self performSelector:@selector(loadSubletDetails) withObject:nil afterDelay:01];
    // Do any additional setup after loading the view.
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    self.title = self.listingName;
    self.navigationController.navigationBar.topItem.backBarButtonItem = [[UIBarButtonItem alloc]
                                                                         initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil];
}


-(void)loadSubletDetails{
    
    [AppDelegate showLoadingView:self];
    
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }

    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:self.listingID,@"listingID",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email", nil];
    NSString *urlAddress = [NSString stringWithFormat:@"sublets/getSubletDetails.json"];
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        //NSLog(@"JSON: %@", responseObject);
        responseObject = [responseObject objectForKey:@"response"];
        NSString *status = [responseObject objectForKey:@"status"];
        if([status boolValue]== 1)
        {
        NSDictionary *listing = [responseObject objectForKey:@"detail_listing"];
        cityString = [listing objectForKey:@"city"];
        stateString = [listing objectForKey:@"state"];
        zipcodeString = [listing objectForKey:@"zipCode"];
        areaString = [listing objectForKey:@"size"];
        bathroomString = [listing objectForKey:@"bathrooms"];
        bedRoomString = [listing objectForKey:@"bedrooms"];
        roommatesString = [listing objectForKey:@"roommates"];
        bedTypeString =[listing objectForKey:@"room"];
        propertyTypeString =[listing objectForKey:@"type"];
        closestTransportString = [listing objectForKey:@"closestTransport"];
        transportDistanceString = [listing objectForKey:@"closestTransportDistance"];
        descriptionString = [listing objectForKey:@"description"];
        furnishString = [listing objectForKey:@"furnishDetails"];
        nameString = [listing objectForKey:@"name"];
        priceString = [listing objectForKey:@"price"];
        lookingForArray = [[listing objectForKey:@"lookingFor"]mutableCopy];
        minStayString =[[listing objectForKey:@"minStay"]isEqualToString:@"(empty)"]?@"Not Mentioned":[listing objectForKey:@"minStay"];
        depositString = [listing objectForKey:@"deposit"];
        moveInString = [[listing objectForKey:@"moveInDate"]isEqualToString:@"(empty)"]?@"Not Mentioned":[listing objectForKey:@"moveInDate"];
        moveOutString = [[listing objectForKey:@"moveOutDate"]isEqualToString:@"(empty)"]?@"Not Mentioned":[listing objectForKey:@"moveOutDate"];
            
        listerInfoDict = [listing objectForKey:@"Lister_info"];
        imagesListArray = [[listing objectForKey:@"Images_list"]mutableCopy];
        
        amenityList = [listing objectForKey:@"amenities"];
        restrictionList = [listing objectForKey:@"restrictions"];
        
        [_tableView reloadData];
            [self loadScrollView];
        [AppDelegate hideLoadingIndicator];

        _errView.hidden =YES;
        _tableView.hidden = NO;
        pageControl.hidden = NO;
        _imageScrollView.hidden = NO;
            
        }
        else{
            
            [AppDelegate hideLoadingIndicator];
            _errView.hidden =NO;
            _tableView.hidden = YES;
            pageControl.hidden = YES;
            _imageScrollView.hidden = YES;

        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        //NSLog(@"Error: %@", error);
        [AppDelegate hideLoadingIndicator];
        _errView.hidden =NO;
        _tableView.hidden = YES;
        pageControl.hidden = YES;
        _imageScrollView.hidden = YES;

        
    }];
    
}

#pragma mark -


- (IBAction)messageUser:(id)sender {
    
    ChatViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
    vc.senderName = [listerInfoDict objectForKey:@"name"];
    vc.messageID = [listerInfoDict objectForKey:@"listerID"];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)openDetailImages:(id)sender {
    
    
    NSMutableArray *photos = [[NSMutableArray alloc] init];
                MWPhoto *photo;
    
                for (int i =0; i<[imagesListArray count]; i++) {
    
                    photo = [MWPhoto photoWithURL:[NSURL URLWithString:imagesListArray[i]]];
                    [photos addObject:photo];
                }
    
    
                self.photos = photos;
                browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
                browser.displayActionButton = NO;
                browser.displayDeleteButton = NO;
                browser.displayNavArrows = YES;
                browser.displaySelectionButtons = NO;
                browser.alwaysShowControls = YES;
                browser.zoomPhotosToFill = YES;
                browser.enableGrid = NO;
                browser.startOnGrid = NO;
                browser.enableSwipeToDismiss = NO;
                browser.autoPlayOnAppear = NO;
                [browser setCurrentPhotoIndex:pageControl.currentPage];
    
                [self.navigationController pushViewController:browser animated:YES];
}

-(void)flagListing
{
    [AppDelegate showLoadingView:self];
    
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:self.listingID,@"listingID",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserId"],@"userID",@"true",@"flag_report", nil];
    NSString *urlAddress = [NSString stringWithFormat:@"sublets/hideSublet.json"];
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        responseObject = [responseObject objectForKey:@"response"];
        NSString *status = [responseObject objectForKey:@"status"];
        if([status boolValue]== 1)
        {
            [KSToastView ks_showToast:@"Listing flaged Successfully !!"];

        }
        
        [AppDelegate hideLoadingIndicator];
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
       // NSLog(@"Error: %@", error);
        [AppDelegate hideLoadingIndicator];
        
    }];

    
}

- (IBAction)reloadData:(id)sender {
    
    [self loadSubletDetails];
}

-(void)configureTags
{
    
    self.tagView.canSelectTags = NO;
    self.tagView.tagStrokeColor = [UIColor colorWithRed:66.0f/255.0f green:28.0f/255.0f blue:82.0f/255.0f alpha:1.0f];
    //    self.tagView.tagBackgroundColor = [UIColor orangeColor];
    self.tagView.tagTextColor = [UIColor colorWithRed:66.0f/255.0f green:28.0f/255.0f blue:82.0f/255.0f alpha:1.0f];
    self.tagView.tagSelectedBackgroundColor = [UIColor colorWithRed:66.0f/255.0f green:28.0f/255.0f blue:82.0f/255.0f alpha:1.0f];
    self.tagView.tagSelectedTextColor = [UIColor whiteColor];
    self.tagView.tagCornerRadius = 5.0f;
    
    [self.tagView.tags removeAllObjects];
    [self.tagView.selectedTags removeAllObjects];
    
    [self.tagView.tags addObjectsFromArray:lookingForArray];
    [self.tagView.selectedTags addObjectsFromArray:self.tagView.tags];
    [self.tagView.collectionView reloadData];

}

-(void)loadScrollView
{
   // [imagesListArray enumerateObjectsUsingBlock:^(NSString *imageName, NSUInteger idx, BOOL *stop)
    
    pageControl.numberOfPages = [imagesListArray count];
    for(int idx =0; idx<[imagesListArray count]; idx ++){
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.imageScrollView.frame) * idx, 0, CGRectGetWidth(self.imageScrollView.frame), CGRectGetHeight(self.imageScrollView.frame))];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        if([imagesListArray count]>0)
        [imageView setImageWithURL:imagesListArray[idx] placeholderImage:[UIImage imageNamed:@"defaultImage"]];
      
        self.imageScrollView.delegate = self;
        self.imageScrollView.pagingEnabled = YES;
        [self.imageScrollView addSubview:imageView];
        
    }
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    self.imageScrollView.contentSize = CGSizeMake(CGRectGetWidth(self.imageScrollView.frame) * [imagesListArray count], CGRectGetHeight(self.imageScrollView.frame));
}
// update the frame of your views ...

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.imageScrollView) {

    NSInteger pageIndex = scrollView.contentOffset.x / CGRectGetWidth(scrollView.frame);
    pageControl.currentPage = pageIndex;
        
    }
}

#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return self.hideListerInfo?20:21;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell;
    
    switch (indexPath.row) {
            
        case 0:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"descpCell" forIndexPath:indexPath];
            
            UITextView *view = (UITextView*)[cell viewWithTag:23];
            view.text = descriptionString;
            break;

        }
        case 1:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"lookingForCell" forIndexPath:indexPath];
            
            self.tagView = (JCTagListView*)[cell viewWithTag:36];
            UILabel *lbl = (UILabel*) [cell viewWithTag:997];
            
            if([lookingForArray count]){
                lbl.hidden = YES;
                [self configureTags];
            }
            else{
                lbl.hidden = NO;
            }

            break;
            
        }
        case 2:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"depositCell" forIndexPath:indexPath];
            
            UILabel * Lbl = (UILabel*)[cell viewWithTag:37];
            Lbl.text = [depositString stringByAppendingString:@" $"];
            break;
            
        }
        case 3:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"moveInCell" forIndexPath:indexPath];
            
            UILabel * Lbl = (UILabel*)[cell viewWithTag:38];
            Lbl.text = moveInString;
            break;
            
        }
        case 4:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"moveOutCell" forIndexPath:indexPath];
            
            UILabel * Lbl = (UILabel*)[cell viewWithTag:961];
            Lbl.text = moveOutString;
            break;
            
        }

        case 5:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"minStayCell" forIndexPath:indexPath];
            
            UILabel * Lbl = (UILabel*)[cell viewWithTag:39];
           Lbl.text = [minStayString stringByAppendingString:@" months"];;
            break;
            
        }
        case 6:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"areaCell" forIndexPath:indexPath];
            UILabel * Lbl = (UILabel*)[cell viewWithTag:30];
            Lbl.text = [areaString stringByAppendingString:@" sqft"];;
            break;

        }
        case 7:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"priceCell" forIndexPath:indexPath];
            UILabel * Lbl = (UILabel*)[cell viewWithTag:31];
            Lbl.text = [priceString stringByAppendingString:@" $"];
            break;
        }
        case 8:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"typeCell" forIndexPath:indexPath];
            UISegmentedControl * segment = (UISegmentedControl*)[cell viewWithTag:960];
            segment.selectedSegmentIndex = [propertyTypeString integerValue];
            break;
            
        }
        case 9:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"bhkCell" forIndexPath:indexPath];
            UISegmentedControl * segment = (UISegmentedControl*)[cell viewWithTag:42];
            segment.selectedSegmentIndex = [bedTypeString integerValue];
            break;
            
        }
        case 10:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"bedRoomsCell" forIndexPath:indexPath];
            UILabel * Lbl = (UILabel*)[cell viewWithTag:32];
            Lbl.text = [bedRoomString stringByAppendingString:@" rooms"];
            break;

        }
        case 11:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"bathroomsCell" forIndexPath:indexPath];
            UILabel * Lbl = (UILabel*)[cell viewWithTag:33];
            Lbl.text = [bathroomString stringByAppendingString:@" rooms"];
            break;

        }
        case 12:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"furnishedCell" forIndexPath:indexPath];
            UILabel * Lbl = (UILabel*)[cell viewWithTag:34];
            Lbl.text = furnishString;
            break;

        }
        case 13:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"roommatesCell" forIndexPath:indexPath];
            UILabel * Lbl = (UILabel*)[cell viewWithTag:35];
            Lbl.text = [roommatesString stringByAppendingString:@" no"];

            break;
            
        }
        case 14:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"amenitiesCell" forIndexPath:indexPath];
            UILabel * Lbl = (UILabel*)[cell viewWithTag:40];
            Lbl.text = [NSString stringWithFormat:@"%d",(int)[amenityList count]];
            break;

        }
        case 15:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"restrictionsCell" forIndexPath:indexPath];
            UILabel * Lbl = (UILabel*)[cell viewWithTag:41];
            Lbl.text = [NSString stringWithFormat:@"%d",(int)[restrictionList count]];
            break;
            
        }

        case 16:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"transportCell" forIndexPath:indexPath];
            
            UILabel * transportLbl = (UILabel*)[cell viewWithTag:24];
            transportLbl.text = closestTransportString;
            UILabel * distanceLbl = (UILabel*)[cell viewWithTag:25];
            distanceLbl.text= [transportDistanceString stringByAppendingString:@" miles"];
            
            break;

        }
        case 17:
        {
        cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"locationCell" forIndexPath:indexPath];
        self.mapView = (MKMapView*)[cell viewWithTag:36];
        self.locationUnavailableView = (UIImageView *)[cell viewWithTag:999];
            
            CLGeocoder *geocoder = [[CLGeocoder alloc] init];
            [geocoder geocodeAddressString:[NSString stringWithFormat:@"%@,%@,%@",cityString,stateString,zipcodeString] completionHandler:^(NSArray *placemarks, NSError *error) {
                if (error) {
                    
                    self.mapView.hidden = YES;
                    self.locationUnavailableView.hidden = NO;
                    
                    //NSLog(@"%@", error);
                } else {
                    CLPlacemark *placemark = [placemarks lastObject];
                    CLLocation *location =   placemark.location;
                    self.mapView.delegate = self;
                    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(location.coordinate, 3000, 3000);
                    double radius = 500.0;
                    MKCircle *circle = [MKCircle circleWithCenterCoordinate:location.coordinate radius:radius];
                    [_mapView addOverlay:circle];
                    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
                    self.mapView.hidden = NO;
                    self.locationUnavailableView.hidden = YES;

                }
            }];
            
            break;
            
        }

        case 18:
        {
            if(!self.hideListerInfo)
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"builderinfoCell" forIndexPath:indexPath];
            else
             cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"flagCell" forIndexPath:indexPath];
    
            break;
        }
        
        case 19:
        {
            if(!self.hideListerInfo)
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"flagCell" forIndexPath:indexPath];
            else
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"messageUserCell" forIndexPath:indexPath];

            
            break;
        }
        case 20:
        {
          
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"messageUserCell" forIndexPath:indexPath];
            
            break;
        }
            
        default:
            break;
    }
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    switch (indexPath.row) {
        case 0:
            return 179;
        case 1:
            return 235;
        case 8:
            return 103;
        case 9:
            return 103;
        case 16:
            return 75;
        case 17:
            return 243;
        default:
           return 46;
    }

    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.row) {
//        case 0:
//        {
//            NSMutableArray *photos = [[NSMutableArray alloc] init];
//            MWPhoto *photo;
//            
//            for (int i =0; i<[imagesListArray count]; i++) {
//                
//                photo = [MWPhoto photoWithURL:[NSURL URLWithString:imagesListArray[i]]];
//                [photos addObject:photo];
//            }
//            
//            
//            self.photos = photos;            
//            browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
//            browser.displayActionButton = NO;
//            browser.displayDeleteButton = NO;
//            browser.displayNavArrows = YES;
//            browser.displaySelectionButtons = NO;
//            browser.alwaysShowControls = YES;
//            browser.zoomPhotosToFill = YES;
//            browser.enableGrid = NO;
//            browser.startOnGrid = NO;
//            browser.enableSwipeToDismiss = NO;
//            browser.autoPlayOnAppear = NO;
//            [browser setCurrentPhotoIndex:0];
//            
//            [self.navigationController pushViewController:browser animated:YES];
//            
//            break;
//
//        }
        case 14:
        {
            amenitiesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AmenitiesViewController"];
            amenitiesVC.type = @"Amenities";
            amenitiesVC.allowEditing = NO;
            NSMutableArray *array = [[NSMutableArray alloc]init];
            
            for (int i =0; i<[amenityList count]; i ++) {
                
                int row = [dataObject getIndexForAmenity:amenityList[i]];
                [array addObject:[NSIndexPath indexPathForRow:row inSection:0]];
            }
            
            amenitiesVC.selectedDatas= array;
            [self.navigationController pushViewController:amenitiesVC animated:YES];
            break;
        }
        case 15:
        {
            amenitiesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AmenitiesViewController"];
            amenitiesVC.type = @"Restrictions";
            amenitiesVC.allowEditing = NO;
            NSMutableArray *array = [[NSMutableArray alloc]init];
            
            for (int i =0; i<[restrictionList count]; i ++) {
                
                int row = [dataObject getIndexForRestriction:restrictionList[i]];
                [array addObject:[NSIndexPath indexPathForRow:row inSection:0]];
            }
            
            amenitiesVC.selectedDatas= array;
            [self.navigationController pushViewController:amenitiesVC animated:YES];
            break;
        }
        case 18:
        {
             if(!self.hideListerInfo)
             {
            ListerInfoViewController *lister = [self.storyboard instantiateViewControllerWithIdentifier:@"ListerInfoViewController"];
            lister.listerName = [listerInfoDict objectForKey:@"name"];
            lister.listerId = [listerInfoDict objectForKey:@"listerID"];
            lister.hideMessageCell = NO;
            [self.navigationController pushViewController:lister animated:YES];
             }
             else{
                 [self flagListing];
             }
           
            break;
        }
        case 19:
        {
            if(!self.hideListerInfo)
            {
                [self flagListing];
            }
            
            break;
        }

        default:
            break;
    }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


#pragma mark - MKMapView

- (MKOverlayRenderer *) mapView:(MKMapView *)mapView rendererForOverlay:(id)overlay
{
    if([overlay isKindOfClass:[MKCircle class]])
    {
        MKCircleRenderer* aRenderer = [[MKCircleRenderer
                                        alloc]initWithCircle:(MKCircle *)overlay];
        
        aRenderer.fillColor = [UIColor colorWithRed:135.0/255.0 green:206.0/255.0 blue:250.0/255.0 alpha:1.0];
    //  aRenderer.strokeColor = [[UIColor grayColor] colorWithAlphaComponent:0.9];
        aRenderer.lineWidth = 2;
        aRenderer.alpha = 0.5;
        
        return aRenderer;
    }
   else
   {
       return nil;
   }
}


#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    if (index < _thumbs.count)
        return [_thumbs objectAtIndex:index];
    return nil;
}


@end
