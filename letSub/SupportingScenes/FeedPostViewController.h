//
//  FeedPostViewController.h
//  letSub
//
//  Created by Vishnu Varthan on 18/06/16.
//  Copyright © 2016 com.vishnuvarthan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTRangeSlider.h"
#import "FeedsViewController.h"

@interface FeedPostViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,TTRangeSliderDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate>

- (IBAction)cancel:(id)sender;
- (IBAction)postFeeds:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *table;
@end
