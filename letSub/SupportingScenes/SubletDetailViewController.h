//
//  SubletDetailViewController.h
//  letSub
//
//  Created by Vishnu Varthan .P on 08/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWPhotoBrowser.h"
#import "AmenitiesViewController.h"
#import "ListerInfoViewController.h"
#import "ChatViewController.h"
#import <MessageUI/MessageUI.h>
#import <MapKit/MapKit.h>
#import "JCTagListView.h"



@interface SubletDetailViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MWPhotoBrowserDelegate,MKMapViewDelegate,UIScrollViewDelegate>
{
    MWPhotoBrowser *browser;
    AmenitiesViewController *amenitiesVC;
    NSMutableArray *selectedAmenitiesArray,*selectedRestrictionsArray,*amenityList,*restrictionList;

    __weak IBOutlet UIPageControl *pageControl;
}
- (IBAction)openDetailImages:(id)sender;
- (IBAction)reloadData:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *imageScrollView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) UIImage *userImage;
@property (nonatomic, strong) NSMutableArray *thumbs;
@property (nonatomic, strong) NSString *listingID,*listingName;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *errView;
@property (weak, nonatomic) IBOutlet UIImageView *locationUnavailableView;;
@property (weak, nonatomic) IBOutlet JCTagListView *tagView;

@property (nonatomic)BOOL hideListerInfo;

@end
