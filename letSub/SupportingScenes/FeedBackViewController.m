//
//  FeedBackViewController.m
//  letSub
//
//  Created by Vishnu Varthan .P on 04/04/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "FeedBackViewController.h"
#import "FeedsViewController.h"

@interface FeedBackViewController ()
{
    UIActivityIndicatorView* activityIndicator;
}
@end

@implementation FeedBackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if(_isPostingFeeds)
    {
        self.postTextView.text= @"What are you planning to share ?";
        self.title = @"Create Feeds";
    }
    else
    {
        self.postTextView.text= @"How can we help you ?";
        self.title = @"Contact Us";

    }
    
    // Do any additional setup after loading the view.
}

#pragma mark -
- (IBAction)cancelPost:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)postFeeds:(id)sender {
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [activityIndicator startAnimating];
    [activityIndicator hidesWhenStopped];
    
    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    [self navigationItem].rightBarButtonItem = barButton;
    
    if(_isPostingFeeds)
    {
        [self postNewFeeds];
    }
    else
    {
        [self postNewSupport];
        
    }

    
}

-(void)postNewSupport
{
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"user_id",self.postTextView.text,@"message",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email", nil];
    
    NSString *urlAddress = [NSString stringWithFormat:@"users/sendSupportEmail.json"];
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        //  //NSLog(@"JSON: %@", responseObject);
        NSDictionary *response = [responseObject objectForKey:@"response"];
        NSString *status = [response objectForKey:@"status"];
        
        if([status boolValue] ==1)
        {
            
            [activityIndicator stopAnimating];
            [KSToastView ks_showToast:@"Message submitted successfully !!" duration:2.0f  completion:^{
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        [activityIndicator stopAnimating];
        [KSToastView ks_showToast:@"We encountered a problem try again !!" duration:2.0f  completion:^{
            
        }];
        
        
    }];

}

// this is used for text only feeds, currently not used.

-(void)postNewFeeds
{
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +zzzz"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"]];
    NSString *dateFromString = [dateFormatter stringFromDate:[NSDate date]];
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"userID",self.postTextView.text,@"message",dateFromString,@"time",@"3",@"feedType",[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserName"],@"userName",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email", nil];
    
    NSString *urlAddress = [NSString stringWithFormat:@"posts/newPost.json"];
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        //  //NSLog(@"JSON: %@", responseObject);
        NSDictionary *response = [responseObject objectForKey:@"response"];
        NSString *status = [response objectForKey:@"status"];
        
        if([status boolValue] ==1)
        {
            
            [activityIndicator stopAnimating];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"getNewFeed" object:self userInfo:nil];
            
            [KSToastView ks_showToast:@"Feed shared successfully !!" duration:2.0f  completion:^{
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        //  //NSLog(@"Error: %@", error);
        
        [activityIndicator stopAnimating];
        [KSToastView ks_showToast:@"We encountered a problem try again !!" duration:2.0f  completion:^{

        }];
        
    }];

}

#pragma mark - text view delegates

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if(_isPostingFeeds)
    {
        if ([textView.text isEqualToString:@"What are you planning to share ?"]) {
            textView.text = @"";
            textView.textColor = [UIColor blackColor];
        }
    }
    else
    {
        if ([textView.text isEqualToString:@"How can we help you ?"]) {
            textView.text = @"";
            textView.textColor = [UIColor blackColor];
        }

        
    }

    return YES;
}


@end
