//
//  ChatViewController.h
//  letSub
//
//  Created by Vishnu Varthan .P on 18/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSQMessages.h"
#import "MessageDataModel.h"
#import "Messages.h"

@interface ChatViewController :JSQMessagesViewController <JSQMessagesComposerTextViewPasteDelegate>
{
    Messages * messageModelObj;
}

@property (strong,nonatomic) MessageDataModel *modelData;
@property (strong,nonatomic) NSString *messageID,*senderName,*senderImgURL;
@property  (strong,nonatomic) UIImage *senderImage;


@end
