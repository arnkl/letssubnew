//
//  MoreFiltersViewController.m
//  letSub
//
//  Created by Vishnu Varthan .P on 06/04/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "MoreFiltersViewController.h"

@interface MoreFiltersViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSString *minStayString,*moveInString;
    
}
@end

@implementation MoreFiltersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Add Filters";
    
    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated{
    
    //NSLog(@"datas..%@",self.moreFilterDatas);
    
    if(!self.selectedRows)
        self.selectedRows = [[NSMutableArray alloc]init];
    
    NSIndexPath * index = [self.moreFilterDatas objectForKey:@"indexPaths"][0];
    
    if(index.section == 0  && index)
    {
        self.selectedRows = [self.moreFilterDatas objectForKey:@"indexPaths"];
        
        if([self.selectedRows count]>0)
            [self.selectedRows removeObjectAtIndex:0];
        else
            [self.selectedRows removeAllObjects];
    }
    else
        self.selectedRows = [self.moreFilterDatas objectForKey:@"indexPaths"];
    
    minStayString = [self.moreFilterDatas objectForKey: @"minimumStay"];
    moveInString = [self.moreFilterDatas objectForKey:@"moveInDate"];
    [super viewWillAppear:YES];
}

#pragma mark -

- (IBAction)minimumStayChanged:(id)sender {
    
    UISlider *slider = sender;
    minStayString = [NSString stringWithFormat:@"%.f",slider.value];
    [_tableView reloadData];
}

- (IBAction)cancelAddingMoreFilters:(id)sender {
    
    [self.navigationController popViewControllerAnimated: YES];
}

- (IBAction)datePickerStatedPicking:(id)sender {
    
    UIDatePicker *picker = sender;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    moveInString = [dateFormat stringFromDate:picker.date];
    [_tableView reloadData];
    
}

- (IBAction)DoneSelectingFilters:(id)sender {
    
    if(!self.selectedRows)
        self.selectedRows = [[NSMutableArray alloc]init];
        
    int count = (int)[self.selectedRows count];
    if(![minStayString isEqualToString:@"None"])
        count =count+1;
    if(![moveInString isEqualToString:@"Any"])
    {
        count = count+1;
    }
    
    if(![_selectedRows count])
        [_selectedRows addObject:[NSIndexPath indexPathForRow:0 inSection:0]];   // adding a temp index in case of an empty amenities/restriction selection.
    
   
    
    NSDictionary *dict =[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",count],@"count",moveInString,@"moveInDate",minStayString,@"minimumStay",_selectedRows,@"indexPaths",nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"AddMoreFilters" object:self userInfo:dict];
    
    [self.navigationController popViewControllerAnimated: YES];
}


#pragma mark - table view delegate & data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 3;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return @"";
            break;
        case 1:
            return @"Amenities";
            break;
        case 2:
            return @"Restrictions";
            break;
        default:
            break;
    }
    
    return @"";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    switch (section) {
        case 0:
            return 2;
            break;
        case 1:
            return [[dataObject amenityArray] count];
            break;
        case 2:
            return [[dataObject restrictionArray] count];
            break;
        default:
            break;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell;
    
    switch (indexPath.section) {
        case 0:
        {
            if(indexPath.row == 0)
            {
                cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"minStayCell" forIndexPath:indexPath];
                UILabel *lbl = (UILabel*)[cell viewWithTag:500];
                minStayString = minStayString?minStayString:@"None";
                lbl.text = minStayString;
                self.slider = (UISlider *)[cell viewWithTag:501];
                self.slider.value = [minStayString isEqualToString:@"None"]?0:[minStayString floatValue];
            }
            else
            {
                cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"moveInCell" forIndexPath:indexPath];
                UILabel *lbl = (UILabel*)[cell viewWithTag:502];
                moveInString = moveInString?moveInString:@"Any";
                lbl.text = moveInString;
                self.datePicker = (UIDatePicker *)[cell viewWithTag:503];
                
                if(![moveInString isEqualToString:@"Any"])
                {
                    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
                    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
                    NSDate *date = [dateFormatter dateFromString:moveInString];
                    self.datePicker.date = date;
                }
            }
        }
            break;
        case 1:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"amenitiesCell" forIndexPath:indexPath];
            cell.textLabel.text = [dataObject amenityNameForNumber:(int)indexPath.row];
            cell.imageView.image = [UIImage imageNamed:[dataObject amenityNameForNumber:(int)indexPath.row]];
            
            if(![self.selectedRows containsObject:indexPath])
                cell.accessoryType = UITableViewCellAccessoryNone;
            else
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
            break;
        case 2:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"restrictionsCell" forIndexPath:indexPath];
            cell.textLabel.text = [dataObject restrictionNameForNumber:(int)indexPath.row];
            cell.imageView.image = [UIImage imageNamed:[dataObject restrictionNameForNumber:(int)indexPath.row]];
            
            if(![self.selectedRows containsObject:indexPath])
                cell.accessoryType = UITableViewCellAccessoryNone;
            else
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
            break;
        default:
            break;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    if (indexPath.section == 0) {
        
        if(indexPath.row == 0)
            return 129;
        else
            return 218;
    }
    
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    if(!self.selectedRows)
        self.selectedRows = [[NSMutableArray alloc]init];
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.section !=0)
    {
        if(cell.accessoryType == UITableViewCellAccessoryNone)
        {
            if(![self.selectedRows containsObject:indexPath])
                [self.selectedRows addObject:indexPath];
            
            
            [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            if([self.selectedRows containsObject:indexPath])
                [self.selectedRows removeObject:indexPath];
            
            [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
            
        }
        
    }
    
    
}

@end
