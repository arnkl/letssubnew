//
//  SubletListingViewController.m
//  letSub
//
//  Created by Vishnu Varthan .P on 07/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "SubletListingViewController.h"
#import "SubletDetailViewController.h"
#import "FeedPostViewController.h"
#import "ListerInfoViewController.h"
#import "FeedsViewController.h"

@interface SubletListingViewController ()
{
    SubletDetailViewController *subLetDetailVC;
    ListerInfoViewController *listerProfileVC;
    
    UIActivityIndicatorView *spinner;
    NSMutableArray *idArray,*imageArray,*locationArray,*nameArray,*posterID,*posterName,*listingInfo,*listingPrice,*posterImage;
    NSInteger cardsLoadedIndex; //%%% the index of the card you have loaded into the loadedCards array last
    NSMutableArray *loadedCards;
}
@end

@implementation SubletListingViewController

static const int MAX_BUFFER_SIZE = 2; //%%% max number of cards loaded at any given time, must be greater than 1
@synthesize allCards;//%%% all the cards

- (void)viewDidLoad {
    [super viewDidLoad];
    
    idArray= [[NSMutableArray alloc]init];
    imageArray = [[NSMutableArray alloc]init];
    locationArray = [[NSMutableArray alloc]init];
    nameArray = [[NSMutableArray alloc]init];
    posterName =[[NSMutableArray alloc]init];
    posterID =[[NSMutableArray alloc]init];
    listingInfo =[[NSMutableArray alloc]init];
    listingPrice =[[NSMutableArray alloc]init];
    posterImage =[[NSMutableArray alloc]init];
    
    loadedCards = [[NSMutableArray alloc] init];
    allCards = [[NSMutableArray alloc] init];
    cardsLoadedIndex = 0;
    
    
    [self performSelector:@selector(getSubletResultforParamerters:) withObject:_searchParameters afterDelay:01];
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.title = @"Listings";
   
    [super viewWillAppear:YES];
    
}

#pragma mark -

-(void)getSubletResultforParamerters :(NSDictionary *)parameters
{
    
      [AppDelegate showLoadingView:self];
    
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }

    
    NSMutableDictionary *parametersDict = [parameters mutableCopy];
    [parametersDict setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"] forKey:authenticationKey];
    [parametersDict setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"] forKey:@"email"];
    
    
        NSString *urlAddress = [NSString stringWithFormat:@"sublets/subletSearch.json"];
        LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
        [client POST:urlAddress parameters:parametersDict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    
            [AppDelegate hideLoadingIndicator];
            //NSLog(@"JSON: %@", responseObject);
    
            NSDictionary *response = [responseObject objectForKey:@"response"];
            NSString *status = [response objectForKey:@"status"];
            
            if([status boolValue]==1)
            {
                
                
                NSArray *array = [response objectForKey:@"listing"];
                
                NSMutableDictionary *data1 =[[NSMutableDictionary alloc]init];
                
                
                for (int i=0; i<[array count]; i++) {
                    
                    data1 = [array objectAtIndex:i];
                    
                    [idArray addObject:[data1 valueForKey:@"id"]];
                    [imageArray addObject:[data1 valueForKey:@"image"]];
                    [locationArray addObject:[data1 valueForKey:@"location"]];
                    [nameArray addObject:[data1 valueForKey:@"name"]];
                    [posterID addObject:[data1 valueForKey:@"posterID"]];
                    [posterName addObject:[data1 valueForKey:@"posterName"]];
                    [listingInfo addObject:[data1 valueForKey:@"info"]];
                    [listingPrice addObject:[data1 valueForKey:@"price"]];
                    [posterImage addObject:[data1 valueForKey:@"posterImage"]];
                    
                    
                }
                
                [self loadCards];
                
            }
            
            else
            {
                self.leftSwipeBtn.hidden = self.rightSwipeBtn.hidden = YES;
                self.inviteFriendsButton.hidden = self.noListingFoundImg.hidden = NO;
                self.DragView.backgroundColor = [UIColor clearColor];
                self.noDataLbl.hidden = YES;
            }
            
            [AppDelegate hideLoadingIndicator];
    
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            //NSLog(@"Error: %@", error);
            [AppDelegate hideLoadingIndicator];
            
            self.leftSwipeBtn.hidden = self.rightSwipeBtn.hidden = YES;
            self.inviteFriendsButton.hidden = self.noListingFoundImg.hidden = NO;
            self.DragView.backgroundColor = [UIColor clearColor];
            self.noDataLbl.hidden = YES; // revent it to no is you dont want sad puppy when data is not loaded/reached last data.

       }];
    
}


-(DraggableView *)createDraggableViewWithDataAtIndex:(NSInteger)index
{
    //NSLog(@"frame..%@",NSStringFromCGRect(self.DragView.frame));
    DraggableView *draggableView = [[DraggableView alloc]initWithFrame:CGRectMake(0, 0, self.DragView.frame.size.width, self.DragView.frame.size.height)];
    
    draggableView.listingName.text = [nameArray objectAtIndex:index];
    draggableView.listingLocation.text = [locationArray objectAtIndex:index];
    draggableView.listingPrice.text =[@"$" stringByAppendingString:[listingPrice objectAtIndex:index]];
    draggableView.listingInfo.text = [listingInfo objectAtIndex:index];
   [draggableView.listingImageView setImageWithURL:[imageArray objectAtIndex:index] placeholderImage:[UIImage imageNamed:@"defaultImage"]];
    [draggableView.profilePicView setImageWithURL:[posterImage objectAtIndex:index] placeholderImage:[UIImage imageNamed:@"defaultProfilePic"]];
    draggableView.cardID = [idArray objectAtIndex:index]; //%%% placeholder for card-specific information
    draggableView.delegate = self;
    return draggableView;
}

//%%% loads all the cards and puts the first x in the "loaded cards" array
-(void)loadCards
{
    
    if([nameArray count] > 0) {
        
        self.leftSwipeBtn.hidden = self.rightSwipeBtn.hidden = NO;
        self.inviteFriendsButton.hidden = self.noListingFoundImg.hidden = YES;
        self.DragView.backgroundColor = [UIColor whiteColor];
        self.noDataLbl.hidden = YES; //
        
        NSInteger numLoadedCardsCap =(([nameArray count] > MAX_BUFFER_SIZE)?MAX_BUFFER_SIZE:[nameArray count]);
        //%%% if the buffer size is greater than the data size, there will be an array error, so this makes sure that doesn't happen
        
        for (int i = 0; i<[nameArray count]; i++) {
            DraggableView* newCard = [self createDraggableViewWithDataAtIndex:i];
            [allCards addObject:newCard];
            
            if (i<numLoadedCardsCap) {
                //%%% adds a small number of cards to be loaded
                [loadedCards addObject:newCard];
            }
        }
        
        //%%% displays the small number of loaded cards dictated by MAX_BUFFER_SIZE so that not all the cards
        // are showing at once and clogging a ton of data
        for (int i = 0; i<[loadedCards count]; i++) {
            if (i>0) {
                [self.DragView insertSubview:[loadedCards objectAtIndex:i] belowSubview:[loadedCards objectAtIndex:i-1]];
            } else {
                [self.DragView addSubview:[loadedCards objectAtIndex:i]];
            }
            cardsLoadedIndex++; //%%% we loaded a card into loaded cards, so we have to increment
        }
    }
    
    else
    {
        // no results found, show sad puppy image
        self.leftSwipeBtn.hidden = self.rightSwipeBtn.hidden = YES;
        self.inviteFriendsButton.hidden = self.noListingFoundImg.hidden = NO;
        self.DragView.backgroundColor = [UIColor clearColor];
        self.noDataLbl.hidden = YES;
    }
    
    
}


#pragma mark -
// This should be customized with your own action
-(void)cardSwipedLeft:(UIView *)card;
{
    //do whatever you want with the card that was swiped
    DraggableView *c = (DraggableView *)card;
    //NSLog(@"swiped left ..%ld", (long)c.tag);
    
    [loadedCards removeObjectAtIndex:0]; //%%% card was swiped, so it's no longer a "loaded card"
    [idArray removeObjectAtIndex:0];
    [imageArray removeObjectAtIndex:0];
    [locationArray removeObjectAtIndex:0];
    [nameArray removeObjectAtIndex:0];
    [posterID removeObjectAtIndex:0];
    [posterName removeObjectAtIndex:0];
    [posterImage removeObjectAtIndex:0];
    [listingInfo removeObjectAtIndex:0];
    [listingPrice removeObjectAtIndex:0];
    
    [self swipedListingforListingID:c.cardID];
    
    if (cardsLoadedIndex < [allCards count]) { //%%% if we haven't reached the end of all cards, put another into the loaded cards
        [loadedCards addObject:[allCards objectAtIndex:cardsLoadedIndex]];
        cardsLoadedIndex++;//%%% loaded a card, so have to increment count
        [self.DragView insertSubview:[loadedCards objectAtIndex:(MAX_BUFFER_SIZE-1)] belowSubview:[loadedCards objectAtIndex:(MAX_BUFFER_SIZE-2)]];
    }
    
    if([idArray count]<= 0)
    {
        self.leftSwipeBtn.hidden = self.rightSwipeBtn.hidden = YES;
        self.inviteFriendsButton.hidden = self.noListingFoundImg.hidden = NO;
        self.DragView.backgroundColor = [UIColor clearColor];
        self.noDataLbl.hidden = YES; //
    }
    
}

//%%% action called when the card goes to the right.
// This should be customized with your own action
-(void)cardSwipedRight:(UIView *)card
{
    //do whatever you want with the card that was swiped
    DraggableView *c = (DraggableView *)card;
    //NSLog(@"swiped right ..%ld", (long)c.tag);
    
    [loadedCards removeObjectAtIndex:0]; //%%% card was swiped, so it's no longer a "loaded card"
    [idArray removeObjectAtIndex:0];
    [imageArray removeObjectAtIndex:0];
    [locationArray removeObjectAtIndex:0];
    [nameArray removeObjectAtIndex:0];
    [posterID removeObjectAtIndex:0];
    [posterName removeObjectAtIndex:0];
    [listingInfo removeObjectAtIndex:0];
    [listingPrice removeObjectAtIndex:0];
    [posterImage removeObjectAtIndex:0];
    
    [self addToFavoritesforListingID:c.cardID];
    
    
    if (cardsLoadedIndex < [allCards count]) { //%%% if we haven't reached the end of all cards, put another into the loaded cards
        [loadedCards addObject:[allCards objectAtIndex:cardsLoadedIndex]];
        cardsLoadedIndex++;//%%% loaded a card, so have to increment count
        [self.DragView insertSubview:[loadedCards objectAtIndex:(MAX_BUFFER_SIZE-1)] belowSubview:[loadedCards objectAtIndex:(MAX_BUFFER_SIZE-2)]];
    }
    
    if([idArray count]<= 0)
    {
        self.leftSwipeBtn.hidden = self.rightSwipeBtn.hidden = YES;
        self.inviteFriendsButton.hidden = self.noListingFoundImg.hidden = NO;
        self.DragView.backgroundColor = [UIColor clearColor];
        self.noDataLbl.hidden = YES;  //
    }
    
}

- (IBAction)leftSwipe:(id)sender {
    
    DraggableView *dragView = [loadedCards firstObject];
    dragView.overlayView.mode = GGOverlayViewModeLeft;
    [UIView animateWithDuration:0.2 animations:^{
        dragView.overlayView.alpha = 1;
    }];
    [dragView leftClickAction];
    
    
    
}


- (IBAction)rightSwipe:(id)sender {
    
    DraggableView *dragView = [loadedCards firstObject];
    dragView.overlayView.mode = GGOverlayViewModeRight;
    [UIView animateWithDuration:0.2 animations:^{
        dragView.overlayView.alpha = 1;
    }];
    
    [dragView rightClickAction];
}


#pragma mark -

-(void)swipedListingforListingID:(NSString *)listingID    // a left swiped listing will no more be listing again to that user. 
{
    // [AppDelegate showLoadingView:self];
    
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }
    
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"userID",listingID,@"listingID",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email", nil];
    NSString *urlAddress = [NSString stringWithFormat:@"sublets/hideSublet.json"];
   
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //NSLog(@"JSON: %@", responseObject);
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        //NSLog(@"Error: %@", error);
    }];
    
}


-(void)addToFavoritesforListingID:(NSString *)listingID
{
    // [AppDelegate showLoadingView:self];
    
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }

    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"userID",listingID,@"listingID",@"1",@"isFavourite",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email", nil];
    NSString *urlAddress = [NSString stringWithFormat:@"sublets/toggleFavouriteSublet.json"];
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //NSLog(@"JSON: %@", responseObject);
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        //NSLog(@"Error: %@", error);
    }];
    
}


-(void)cardSelected:(UIView *)card{
    
    DraggableView *c = (DraggableView *)card;
    
    subLetDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SubletDetailViewController"];
    subLetDetailVC.listingID = c.cardID;
    subLetDetailVC.hideListerInfo = NO;
    subLetDetailVC.listingName = nameArray[c.tag];
    [self.navigationController pushViewController:subLetDetailVC animated:YES];
}

-(void)openProfile:(UIView *)card{
    
    DraggableView *c = (DraggableView *)card;
    
    listerProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ListerInfoViewController"];
    listerProfileVC.listerId = posterID[c.tag];
    listerProfileVC.listerName = posterName[c.tag];
    listerProfileVC.hideMessageCell = NO;
    [self.navigationController pushViewController:listerProfileVC animated:YES];
}


- (IBAction)knowMore:(id)sender {
    
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    
    alertViewController.title = @"INFO";
    alertViewController.message = @"Sharing your search will allow you to post your search to all logged in users of lets-sub.Your posted search will appear on feeds sections to near by users, allowing them to contact you for help.";
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    alertViewController.buttonColor = [UIColor colorWithRed:86.0f/255.0f green:1.0f/255.0f blue:71.0f/255.0f alpha:0.90f];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Done"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                          }]];
    
    [self presentViewController:alertViewController animated:YES completion:nil];
    
    
}

- (IBAction)shareSearch:(id)sender {
   
    FeedPostViewController *postController = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedPostViewController"];
    UINavigationController *navigationController =
    [[UINavigationController alloc] initWithRootViewController:postController];
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (IBAction)inviteFriends:(id)sender {
    
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"LetsSub Invite";
    alertViewController.message = @"Invite friends using";
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleSlideFromBottom;
    alertViewController.buttonColor = [UIColor colorWithRed:86.0f/255.0f green:1.0f/255.0f blue:71.0f/255.0f alpha:0.90f];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Facebook"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                              [self facebookInvite];
                                                              
                                                          }]];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Email"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                              [self eMailInvite];
                                                          }]];
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Text Message"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                              [self smsInvite];
                                                          }]];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Cancel"
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                          }]];
    
    
    [self presentViewController:alertViewController animated:YES completion:nil];
    
    
}


-(void)facebookInvite
{
    FBSDKAppInviteContent *content =[[FBSDKAppInviteContent alloc] init];
    content.appLinkURL = [NSURL URLWithString:@"https://fb.me/508511066018718"];
    //optionally set previewImageURL
     content.appInvitePreviewImageURL = [NSURL URLWithString:@"http://letssub.com/images/fb_share_v1.png"];
    
    // Present the dialog. Assumes self is a view controller
    // which implements the protocol `FBSDKAppInviteDialogDelegate`.
    [FBSDKAppInviteDialog showFromViewController:self
                                     withContent:content
                                        delegate:nil];
    
    
}

-(void)eMailInvite
{
    
    if(![MFMailComposeViewController canSendMail]) {
        return;
    }
    
    NSArray *toRecipents = [NSArray arrayWithObject:@""];
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [[controller navigationBar] setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName]];
    [controller setSubject:@"Lets-Sub Invite !!"];
    [controller setMessageBody:@"Hey, Have you heard about Lets-Sub, the easiest way to find sublets with a swipe. </br> </br> Click to download LETS-Sub from <a href=\"https://appsto.re/in/ellncb.i\">AppStore - LetsSub</a> " isHTML:YES];
    
    [controller setToRecipients:toRecipents];
    
    if(controller)
        [self presentViewController:controller animated:YES completion:NULL];
}

-(void)smsInvite
{
    if(![MFMessageComposeViewController canSendText]) {
        return;
    }
    
    NSArray *recipents = @[];
    NSString *message = [NSString stringWithFormat:@"Hey, Have you heard about Lets-Sub, the easiest way to find sublets with a swipe. Click to download LETS-Sub from https://appsto.re/in/ellncb.i"];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [[messageController navigationBar] setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName]];
    [messageController setRecipients:recipents];
    [messageController setBody:message];
    
    // Present message view controller on screen
    if(messageController)
    [self presentViewController:messageController animated:YES completion:nil];
}


#pragma mark -
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            //NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            //NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            //NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            //NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
            break;
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
