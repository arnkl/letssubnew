//
//  ListerInfoViewController.m
//  letSub
//
//  Created by Vishnu Varthan .P on 01/04/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "ListerInfoViewController.h"
#import "ChatViewController.h"


@interface ListerInfoViewController ()
{
     NSString *minBudgetString,*maxBudgetString,*moveInString,*userNameString,*locationString,*preferredLocationString,*jobString,*companyString,*aboutString,*imageString,*coverImageString,*ageString,*educationString,*fbIDString;
    
    NSArray *intrestArray;
    NSMutableArray *friendsArray;
}

@end

@implementation ListerInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     [self performSelector:@selector(loadUserDetails) withObject:nil afterDelay:01];
    intrestArray = [[NSArray alloc]init];
    friendsArray = [[NSMutableArray alloc]init];
       // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.title = @"Profile";
}

-(void)viewDidAppear:(BOOL)animated
{
//    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
//    // add effect to an effect view
//    UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
//    effectView.frame = bgImageView.frame;
//    // add the effect view to the image view
//    [bgImageView addSubview:effectView];

}

#pragma mark -

-(UIScrollView *)createIntrestContentWithScrollView :(UIScrollView*)scrollView
{
    
    NSArray *viewsToRemove = [scrollView subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    
    if([intrestArray count]== 0)
    {
        UILabel *lable =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 125, 30)];
        lable.text = @"(No Interest)";
        lable.font = [UIFont systemFontOfSize:12];
        lable.textColor= [UIColor colorWithRed:128/255.0 green:128/255.0 blue:128/255.0 alpha:1.0];
        lable.textAlignment = NSTextAlignmentCenter;
        [scrollView addSubview:lable];
        lable.center = scrollView.center;
        scrollView.contentSize = CGSizeMake(10, scrollView.frame.size.height);
        return scrollView;
        
    }
    
    int x = 10;
    for (int i = 0; i < [intrestArray count]; i++) {
        
        NSDictionary *dict = intrestArray [i];
        
        
        UIView *View = [[UIView alloc] initWithFrame:CGRectMake(x, 0, 150, 180)];
        //View.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:0.70];
        View.layer.cornerRadius = 5.0f;
        
        UIImageView *intrestImg =[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 140, 100)];
        intrestImg.contentMode = UIViewContentModeScaleAspectFill;
        intrestImg.layer.masksToBounds = YES;
        [intrestImg.layer setBorderColor: [[UIColor lightGrayColor] CGColor]];
        [intrestImg.layer setBorderWidth: 0.1];
        [intrestImg setImageWithURL:[dict objectForKey:@"picture"] placeholderImage:[UIImage imageNamed:@"defaultImage"]];
        [View addSubview:intrestImg];
        
        UILabel *intrestLable =[[UILabel alloc]initWithFrame:CGRectMake(2, 110, 146, 50)];
        intrestLable.text = [dict objectForKey:@"name"];
        intrestLable.font = [UIFont systemFontOfSize:12];
        intrestLable.textColor= [UIColor colorWithRed:128/255.0 green:128/255.0 blue:128/255.0 alpha:1.0];
        intrestLable.numberOfLines = 2;
        intrestLable.textAlignment = NSTextAlignmentCenter;
        [View addSubview:intrestLable];
        
        [scrollView addSubview:View];
        x += View.frame.size.width + 10;
    }
    
    scrollView.contentSize = CGSizeMake(x, scrollView.frame.size.height);
    return scrollView;
}

-(UIScrollView *)createFriendsContentWithScrollView :(UIScrollView*)scrollView
{
    NSArray *viewsToRemove = [scrollView subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    
    if([friendsArray count] == 0)
    {
        UILabel *lable =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 125, 30)];
        lable.text = @"(No Mutual Friends)";
        lable.font = [UIFont systemFontOfSize:12];
        lable.textColor= [UIColor colorWithRed:128/255.0 green:128/255.0 blue:128/255.0 alpha:1.0];
        lable.textAlignment = NSTextAlignmentCenter;
        [scrollView addSubview:lable];
        lable.center = scrollView.center;
        scrollView.contentSize = CGSizeMake(10, scrollView.frame.size.height);
        return scrollView;
    }

    
    
    int x = 10;
    for (int i = 0; i < [friendsArray count]; i++) {
        
        NSDictionary *dict = friendsArray [i];
        
        
        UIView *View = [[UIView alloc] initWithFrame:CGRectMake(x, 0, 100, 130)];
        View.layer.cornerRadius = 5.0f;
        
        UIImageView *intrestImg =[[UIImageView alloc]initWithFrame:CGRectMake(15, 5, 70, 70)];
        intrestImg.contentMode = UIViewContentModeScaleAspectFill;
        intrestImg.layer.cornerRadius = 35;
        intrestImg.layer.masksToBounds = YES;
        [intrestImg.layer setBorderColor: [[UIColor lightGrayColor] CGColor]];
        [intrestImg.layer setBorderWidth: 0.1];
        [intrestImg setImageWithURL:[dict objectForKey:@"picture"] placeholderImage:[UIImage imageNamed:@"defaultImage"]];
        [View addSubview:intrestImg];
        
        UILabel *intrestLable =[[UILabel alloc]initWithFrame:CGRectMake(0, 76, 100, 30)];
        intrestLable.text = [dict objectForKey:@"name"];
        intrestLable.font = [UIFont systemFontOfSize:12];
        intrestLable.textColor= [UIColor colorWithRed:128/255.0 green:128/255.0 blue:128/255.0 alpha:1.0];
        intrestLable.numberOfLines = 2;
        intrestLable.textAlignment = NSTextAlignmentCenter;
        [View addSubview:intrestLable];
        
        [scrollView addSubview:View];
        x += View.frame.size.width + 10;
    }
    
    scrollView.contentSize = CGSizeMake(x, scrollView.frame.size.height);
    return scrollView;
}

-(void)getUserFriendsDetailsWithID :(NSString*)userID andName:(NSString*)userName
{
    NSString *graphPath = [NSString stringWithFormat:@"/%@",userID];
    NSDictionary *params1 = @{
                              @"fields": @"picture.width(300).height(300)",
                              };
    
    FBSDKGraphRequest *request1 = [[FBSDKGraphRequest alloc]
                                   initWithGraphPath:graphPath
                                   parameters:params1
                                   HTTPMethod:@"GET"];
    [request1 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                           id result,
                                           NSError *error) {
        
        NSString *intrestImageUrl = [[[result objectForKey:@"picture"]objectForKey:@"data"]objectForKey:@"url"];
        
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:userID,@"id",userName,@"name",intrestImageUrl,@"picture", nil];
        [friendsArray addObject:dict];
        [_tableView reloadData];
    }];
    
}


-(void)loadUserDetails{
    
    [AppDelegate showLoadingView:self];
    
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }

    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:self.listerId,@"userID",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email", nil];
    NSString *urlAddress = [NSString stringWithFormat:@"users/getUserDetails.json"];
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [AppDelegate hideLoadingIndicator];
        //NSLog(@"JSON: %@", responseObject);
        
        responseObject = [responseObject objectForKey:@"response"];
        NSDictionary *listing = [responseObject objectForKey:@"response"];
        imageString = [listing objectForKey:@"picture"];
        coverImageString = [listing objectForKey:@"coverPicture"];
        userNameString = [listing objectForKey:@"username"];
        locationString = [listing objectForKey:@"location"];
        intrestArray = [listing objectForKey:@"interest"];
        aboutString = [[listing objectForKey:@"description"]isEqualToString:@"(empty)"]?@"Not Mentioned":[listing objectForKey:@"description"];
        jobString = [[listing objectForKey:@"jobTitle"]isEqualToString:@"(empty)"]?@"Not Mentioned":[listing objectForKey:@"jobTitle"];
        companyString =[[listing objectForKey:@"company"]isEqualToString:@"(empty)"]?@"Not Mentioned":[listing objectForKey:@"company"];
         ageString =[[listing objectForKey:@"age_range"]isEqualToString:@"(empty)"]?@"Not Mentioned":[listing objectForKey:@"age_range"];
        educationString =[[listing objectForKey:@"education"]isEqualToString:@"(empty)"]?@"Not Mentioned":[listing objectForKey:@"education"];
         fbIDString =[[listing objectForKey:@"oauth_id"]isEqualToString:@"(empty)"]?@"Not Mentioned":[listing objectForKey:@"oauth_id"];
        preferredLocationString = [[listing objectForKey:@"preferredLocation"]isEqualToString:@"(empty)"]?@"Not Mentioned":[listing objectForKey:@"preferredLocation"];
        minBudgetString =[@"$" stringByAppendingString:[listing objectForKey:@"minBudget"]];
        maxBudgetString =[@"$" stringByAppendingString:[listing objectForKey:@"maxBudget"]];
        moveInString = [listing objectForKey:@"moveInDate"];
        _listerId = [listing objectForKey:@"id"];
        
        NSDictionary *params = @{
                                 @"fields": @"context.fields(mutual_friends)",
                                 };

        NSString*path = [NSString stringWithFormat:@"/%@",fbIDString];
        /* make the API call */
        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                      initWithGraphPath:path
                                      parameters:params
                                      HTTPMethod:@"GET"];
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                              id result,
                                              NSError *error) {
            
            // dXNlcl9jb250ZAXh0OgGQqvHoDVwO4GsJvYOQsYNNriZATozHDkxFUSCO39ZBMSDkeT2fOS9cVon32cmeQtJkkNqK5W9FrR0Ux4Nc0BZBkWHZBKYkSnYwc8xj9zlzd4Dht5kZD
            
                      //  NSLog(@"mutual friends found.....%@",result);
            
            NSArray *array = [[[result objectForKey:@"context"] objectForKey:@"mutual_friends"]objectForKey:@"data"];
            
            for (NSDictionary *intrestDict in array) {
                
                [self getUserFriendsDetailsWithID:[intrestDict objectForKey:@"id"] andName:[intrestDict objectForKey:@"name"]];
            }
            
        }];

        
        [_tableView reloadData];
        _tableView.hidden = NO;
        _errView.hidden = YES;
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        //NSLog(@"Error: %@", error);
        _tableView.hidden = YES;
        _errView.hidden = NO;

        [AppDelegate hideLoadingIndicator];
        
    }];
}


- (IBAction)messageUser:(id)sender {
    
    ChatViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
    vc.messageID = self.listerId;
    vc.senderName = self.listerName;
    vc.senderImage = dpImageView.image;
    [self.navigationController pushViewController:vc animated:YES];
}

- (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (IBAction)reloadContent:(id)sender {
    
    [self loadUserDetails];
}

#pragma mark - table view delegates & data sources

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return self.hideMessageCell?13:14;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    switch (indexPath.row) {
        case 0:{
            
            
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"picCell" forIndexPath:indexPath];
            bgImageView = (UIImageView*)[cell viewWithTag:800];
            dpImageView = (UIImageView*)[cell viewWithTag:801];
            
            [bgImageView setImageWithURL:[NSURL URLWithString:coverImageString] placeholderImage:[self imageFromColor:[UIColor lightGrayColor]]];
            [dpImageView setImageWithURL:[NSURL URLWithString:imageString] placeholderImage:[UIImage imageNamed:@"defaultProfilePic"]];
            UILabel *lbl = (UILabel*)[cell viewWithTag:802];
            lbl.text = userNameString;
            UILabel *lbl2 = (UILabel*)[cell viewWithTag:803];
            lbl2.text = locationString?locationString:@"";
            break;
        }
        case 1:{
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"aboutCell" forIndexPath:indexPath];
            UITextView *view = (UITextView *)[cell viewWithTag:804];
            view.text = aboutString;
            break;
        }
        case 2:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"friendsCell" forIndexPath:indexPath];
            UIScrollView *scrollView = (UIScrollView*)[cell viewWithTag:891];
            scrollView =[self createFriendsContentWithScrollView:scrollView];
            break;
        }
        case 3:{
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
            cell.separatorInset = UIEdgeInsetsMake(0.f, 0.f, 0.f, cell.bounds.size.width);
            
            break;
        }
        case 4:{
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"workCell" forIndexPath:indexPath];
            UILabel *lbl = (UILabel*)[cell viewWithTag:808];
            lbl.text = jobString;
            break;
        }
        case 5:{
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"companyCell" forIndexPath:indexPath];
            UILabel *lbl = (UILabel*)[cell viewWithTag:809];
            lbl.text = companyString;
            
            
            break;
        }
        case 6:{
            
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"educationCell" forIndexPath:indexPath];
            UILabel *lbl = (UILabel*)[cell viewWithTag:892];
            lbl.text =  educationString;
            break;
            
        }
        case 7:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"ageCell" forIndexPath:indexPath];
            UILabel *lbl = (UILabel*)[cell viewWithTag:893];
            lbl.text =  ageString;
            break;
        }

        case 8:{
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"preferredLocationCell" forIndexPath:indexPath];
            UILabel *lbl = (UILabel*)[cell viewWithTag:805];
            lbl.text = preferredLocationString;
            
            break;
        }
        case 9:{
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"budgetCell" forIndexPath:indexPath];
            UILabel *lbl = (UILabel*)[cell viewWithTag:806];
            lbl.text = [NSString stringWithFormat:@"%@ - %@",minBudgetString,maxBudgetString];
            
            break;
        }
        case 10:{
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"moveInCell" forIndexPath:indexPath];
            UILabel *lbl = (UILabel*)[cell viewWithTag:807];
            lbl.text = moveInString;
            
            break;
        }
        case 11:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"intrestCell" forIndexPath:indexPath];
            UIScrollView *scrollView = (UIScrollView*)[cell viewWithTag:890];
            scrollView =[self createIntrestContentWithScrollView:scrollView];
            
            break;
        }
        case 12:{
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"userListingCell" forIndexPath:indexPath];
            
            
            break;
        }
        case 13:{
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"messageUserCell" forIndexPath:indexPath];
            cell.separatorInset = UIEdgeInsetsMake(0.f, 0.f, 0.f, cell.bounds.size.width);
            
            break;
        }
            
            
        default:
            break;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    switch (indexPath.row) {
        case 0:
            return 297;
            break;
        case 1:
            return 172;
        case 2:
            return 153;
        case 11:
            return 208;
            break;
        default:
            break;
    }
    
    return 46;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 12:
            
            listingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MyListingViewController"];
            listingVC.othersListing = YES;
            listingVC.listerId = _listerId;
            [self.navigationController pushViewController:listingVC animated:YES];

            break;
            default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end
