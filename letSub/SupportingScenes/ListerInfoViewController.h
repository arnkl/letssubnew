//
//  ListerInfoViewController.h
//  letSub
//
//  Created by Vishnu Varthan .P on 01/04/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MyListingViewController.h"



@interface ListerInfoViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>
{
    UIImageView *bgImageView,*dpImageView;
    MyListingViewController *listingVC;
}

- (IBAction)messageUser:(id)sender;
- (IBAction)reloadContent:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *errView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong,nonatomic)NSString *listerName,*listerId;
@property (nonatomic)BOOL hideMessageCell;
@end
