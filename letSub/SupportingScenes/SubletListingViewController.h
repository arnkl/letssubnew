//
//  SubletListingViewController.h
//  letSub
//
//  Created by Vishnu Varthan .P on 07/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DraggableView.h"
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MessageUI.h>


@interface SubletListingViewController : UIViewController <DraggableViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate>

- (IBAction)knowMore:(id)sender;
- (IBAction)shareSearch:(id)sender;
- (IBAction)inviteFriends:(id)sender;

-(void)cardSwipedLeft:(UIView *)card;
-(void)cardSwipedRight:(UIView *)card;

@property (weak) id <DraggableViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *leftSwipeBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightSwipeBtn;
@property (weak, nonatomic) IBOutlet UIView *DragView;
@property (retain,nonatomic)NSArray* exampleCardLabels; //%%% the labels the cards
@property (retain,nonatomic)NSMutableArray* allCards; //%%% the labels the cards
@property (weak, nonatomic) IBOutlet UIButton *inviteFriendsButton;
@property (weak, nonatomic) IBOutlet UIImageView *noListingFoundImg;
@property (weak, nonatomic) IBOutlet UILabel *noDataLbl;
@property (strong ,nonatomic) NSDictionary * searchParameters;


@end
