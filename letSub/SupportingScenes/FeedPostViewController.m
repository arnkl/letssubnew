//
//  FeedPostViewController.m
//  letSub
//
//  Created by Vishnu Varthan on 18/06/16.
//  Copyright © 2016 com.vishnuvarthan. All rights reserved.
//

#import "FeedPostViewController.h"

@interface FeedPostViewController ()
{
    UIActivityIndicatorView* activityIndicator;
    UITextField  *locationTextField;
    NSString *priceMax,*priceMin,*type,*bed;
    
}
@end

@implementation FeedPostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    priceMax =@"3000";
    priceMin = @"1000";
    type = @"Rental";
    bed = @"2BED";
    self.title = @"Create Feed";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    self.table.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)typeSelected:(id)sender {
  
    UISegmentedControl *segment = sender;
    
    switch (segment.selectedSegmentIndex) {
        case 0:
            type = @"Private room";
            break;
        case 1:
            type = @"Shared room";
            break;
        case 2:
            type = @"Whole place";
            
        default:
            break;
    }
    
    
    
}
- (IBAction)bedSelected:(id)sender {
    
    UISegmentedControl *segment = sender;
    
    switch (segment.selectedSegmentIndex) {
        case 0:
            bed = @"Studio";
            break;
        case 1:
            bed = @"1BED";
            break;
        case 2:
            bed = @"2BED";
            break;
        case 3:
            bed = @"3BED";
            break;
        case 4:
            bed = @"+3BED";
            break;
            
        default:
            break;
    }

    
}

- (IBAction)cancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)postFeeds:(id)sender {
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [activityIndicator startAnimating];
    [activityIndicator hidesWhenStopped];
    
    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    [self navigationItem].rightBarButtonItem = barButton;
    
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }
    
    
    NSString * message = [NSString stringWithFormat:@"Looking for a %@ with %@ in %@ in the price range $%@ - $%@. ",type,bed,locationTextField.text,priceMin,priceMax];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +zzzz"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"]];
    NSString *dateFromString = [dateFormatter stringFromDate:[NSDate date]];
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"userID",message,@"message",dateFromString,@"time",@"3",@"feedType",[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserName"],@"userName",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email", nil];
    
    NSString *urlAddress = [NSString stringWithFormat:@"posts/newPost.json"];
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        //  //NSLog(@"JSON: %@", responseObject);
        NSDictionary *response = [responseObject objectForKey:@"response"];
        NSString *status = [response objectForKey:@"status"];
        
        if([status boolValue] ==1)
        {
            
            [activityIndicator stopAnimating];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"getNewFeed" object:self userInfo:nil];
            
            [KSToastView ks_showToast:@"Feed shared successfully !!" duration:2.0f  completion:^{
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        //  //NSLog(@"Error: %@", error);
        
        [activityIndicator stopAnimating];
        [KSToastView ks_showToast:@"We encountered a problem try again !!" duration:2.0f  completion:^{
            
        }];
        
    }];
    
}



#pragma mark - Tableview Delegate & Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
            return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell;
    
 
        switch (indexPath.row) {
            case 0:
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"headingCell" forIndexPath:indexPath];
                
                break;
            }
            case 1:
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"typeCell" forIndexPath:indexPath];
                
                break;
            }
            case 2:
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"bhkCell" forIndexPath:indexPath];
                
                break;
            }case 3:
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"locationCell" forIndexPath:indexPath];
                locationTextField = (UITextField *)[cell viewWithTag:412];
                break;
            }
            case 4:
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"priceCell" forIndexPath:indexPath];
                TTRangeSlider *slider = (TTRangeSlider *)[cell viewWithTag:413];
                slider.tintColorBetweenHandles = [UIColor orangeColor];
                slider.delegate = self;
                slider.handleImage = [UIImage imageNamed:@"slider-default7-handle.png"];
                [slider setSelectedMaximum:[priceMax floatValue]];
                [slider setSelectedMinimum:[priceMin floatValue]];
                break;
                
            }
                
            default:
                break;
        }
        
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
        switch (indexPath.row) {
                case 0:
                return 80;
                break;
            case 1:
                return 103;
                break;
            case 2:
                return 103;
                break;
                case 3:
                return 95;
                break;
            case 4:
                return 114;
                break;
                
            default:
                break;
        }
        return 95;

    
}

#pragma mark - TTRangeSliderViewDelegate

-(void)rangeSlider:(TTRangeSlider *)sender didChangeSelectedMinimumValue:(float)selectedMinimum andMaximumValue:(float)selectedMaximum{
    
    priceMax = [NSString stringWithFormat:@"%.0f",selectedMaximum];
    priceMin = [NSString stringWithFormat:@"%.0f",selectedMinimum];
    
}

#pragma mark - KeyBoard Notifications

- (void)keyboardWillShow:(NSNotification*)aNotification {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.table.contentInset.top, 0.0, kbSize.height, 0.0);
    self.table.contentInset = contentInsets;
    self.table.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillHide:(NSNotification*)aNotification {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.35];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.table.contentInset.top, 0.0, 0.0, 0.0);
    self.table.contentInset = contentInsets;
    self.table.scrollIndicatorInsets = contentInsets;
    [UIView commitAnimations];
    
}



@end
