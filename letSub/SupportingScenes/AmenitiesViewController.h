//
//  AmenitiesViewController.h
//  letSub
//
//  Created by Vishnu Varthan .P on 08/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AmenitiesViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *selectedDatas;
@property(strong,nonatomic) NSString *type;
@property(nonatomic) BOOL allowEditing;
@end
