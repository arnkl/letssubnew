//
//  FeedBackViewController.h
//  letSub
//
//  Created by Vishnu Varthan .P on 04/04/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FeedBackViewController : UIViewController <UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *postTextView;
@property (nonatomic) BOOL isPostingFeeds;
@end
