//
//  AmenitiesViewController.m
//  letSub
//
//  Created by Vishnu Varthan .P on 08/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "AmenitiesViewController.h"

@interface AmenitiesViewController () <UITableViewDelegate,UITableViewDataSource>

@end

@implementation AmenitiesViewController
@synthesize allowEditing;

- (void)viewDidLoad {
    [super viewDidLoad];
   
    
    if(!self.selectedDatas)
        self.selectedDatas = [[NSMutableArray alloc]init];
    
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    self.navigationController.navigationBar.topItem.title = self.type;
     _tableView.allowsSelection = allowEditing;
}

#pragma mark - table view delegate & data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return [self.type isEqualToString: @"Amenities"]?[[dataObject amenityArray] count]:[[dataObject restrictionArray] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    cell.textLabel.text = [self.type isEqualToString: @"Amenities"]?[dataObject amenityNameForNumber:(int)indexPath.row]:[dataObject restrictionNameForNumber:(int)indexPath.row];
    cell.imageView.image = [UIImage imageNamed:[self.type isEqualToString: @"Amenities"]?[dataObject amenityNameForNumber:(int)indexPath.row]:[dataObject restrictionNameForNumber:(int)indexPath.row]];
    
    if(![self.selectedDatas containsObject:indexPath])
        cell.accessoryType = UITableViewCellAccessoryNone;
    else
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    if(!self.selectedDatas)
    self.selectedDatas = [[NSMutableArray alloc]init];
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    if(cell.accessoryType == UITableViewCellAccessoryNone)
    {
    if(![self.selectedDatas containsObject:indexPath])
        [self.selectedDatas addObject:indexPath];
        
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    else
    {
        if([self.selectedDatas containsObject:indexPath])
            [self.selectedDatas removeObject:indexPath];
        
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    NSDictionary *dict =[NSDictionary dictionaryWithObjectsAndKeys:self.selectedDatas,@"selectedIndex",nil];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:[self.type isEqualToString: @"Amenities"]?@"selectedAmenities":@"selectedRestrictions" object:self userInfo:dict];
    
}

@end
