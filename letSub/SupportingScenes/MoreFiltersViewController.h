//
//  MoreFiltersViewController.h
//  letSub
//
//  Created by Vishnu Varthan .P on 06/04/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreFiltersViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property(weak,nonatomic) IBOutlet UISlider *slider;
@property (nonatomic, strong) NSMutableArray *selectedRows;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong , nonatomic) NSDictionary *moreFilterDatas;
@end
