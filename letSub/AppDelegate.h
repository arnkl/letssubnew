//
//  AppDelegate.h
//  letSub
//
//  Created by Vishnu Varthan .P on 04/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HKSlideMenu3DController.h"
#import "Reachability.h"
#import "TWMessageBarManager.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate>


@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *apnsTokenString;
@property (strong, nonatomic) NSMutableArray *userIntrestArray;
@property (strong, nonatomic)HKSlideMenu3DController *slideMenuVC;
@property (nonatomic) BOOL isReachable;
@property (nonatomic) Reachability *internetReachability;

+(AppDelegate *)instance;
+ (void)showLoadingView:(UIViewController *)viewController;
+ (void)showLoadingView:(UIViewController *)viewController withHeading:(NSString*)heading andText:(NSString*)text;
+ (void)hideLoadingIndicator;
-(NSString *)apnsDeviceToken;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
- (void)saveContext;
-(NSMutableArray*)fetchPersistentDataforEntity:(NSString *)entityName;
- (void)deleteAllEntities:(NSString *)nameEntity;
- (NSURL *)applicationDocumentsDirectory;

-(void)openProfileView;
-(void)openHomeView;
-(void)openLoginPage;
-(void)openRegistrationPage;
-(void)openMyListing;
-(void)openAbout;
-(void)openPasswordChangePage;
-(void)openSupport;
-(void)openUserAuthenticationPageWithParameters :(NSDictionary*)dict;
-(void)openForgotPassword;
-(void)openFeedView;
-(void)openFavoriteView;
-(void)openChatView;
-(BOOL)isConnectionAvailable;

@end

