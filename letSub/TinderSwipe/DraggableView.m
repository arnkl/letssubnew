//
//  DraggableView.m
//  testing swiping
//
//  Created by Richard Kim on 5/21/14.
//  Copyright (c) 2014 Richard Kim. All rights reserved.
//
//  @cwRichardKim for updates and requests

#define ACTION_MARGIN 120 //%%% distance from center where the action applies. Higher = swipe further in order for the action to be called
#define SCALE_STRENGTH 4 //%%% how quickly the card shrinks. Higher = slower shrinking
#define SCALE_MAX .93 //%%% upper bar for how much the card shrinks. Higher = shrinks less
#define ROTATION_MAX 1 //%%% the maximum rotation allowed in radians.  Higher = card can keep rotating longer
#define ROTATION_STRENGTH 320 //%%% strength of rotation. Higher = weaker rotation
#define ROTATION_ANGLE M_PI/8 //%%% Higher = stronger rotation angle


#import "DraggableView.h"

@implementation DraggableView {
    CGFloat xFromCenter;
    CGFloat yFromCenter;
}

//delegate is instance of ViewController
@synthesize delegate;

@synthesize panGestureRecognizer;
@synthesize listingName,listingLocation,background,listingImageView,listingInfo,listingPrice,profilePicView;
@synthesize overlayView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupView];
        
        background = [[UIView alloc]initWithFrame:CGRectMake(0, self.frame.size.height-129,self.frame.size.width , 79)];
        background.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.40];
        
        listingImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0,self.frame.size.width,self.frame.size.height-50)];
        listingImageView.backgroundColor = [UIColor clearColor];
        listingImageView.image = [UIImage imageNamed:@"defaultImage"];
        listingImageView.contentMode = UIViewContentModeScaleAspectFill;
        listingImageView.layer.cornerRadius = 4;
        listingImageView.layer.masksToBounds = YES;
        
        profilePicView = [[UIImageView alloc]initWithFrame:CGRectMake(15,self.frame.size.height-90 ,70,70)];
        profilePicView.backgroundColor = [UIColor clearColor];
        profilePicView.image = [UIImage imageNamed:@"defaultProfilePic"];
        profilePicView.contentMode = UIViewContentModeScaleAspectFill;
        profilePicView.layer.masksToBounds = YES;
        profilePicView.layer.cornerRadius = 35;
        profilePicView.layer.borderColor =[UIColor colorWithRed:0.451 green:0.000 blue:0.549 alpha:1.000].CGColor;
        profilePicView.layer.borderWidth = 2;

        
        listingName = [[UILabel alloc]initWithFrame:CGRectMake(8,8 ,self.frame.size.width-15 ,36 )];
        [listingName setTextAlignment:NSTextAlignmentRight];
        [listingName setFont:[UIFont fontWithName:@"GillSans" size:24]];
        listingName.textColor = [UIColor whiteColor];
        
        listingLocation = [[UILabel alloc]initWithFrame:CGRectMake(8,52,self.frame.size.width-15 ,21 )];
        [listingLocation setFont:[UIFont preferredFontForTextStyle:(UIFontTextStyleSubheadline)]];
        listingLocation.textColor = [UIColor whiteColor];
        [listingLocation setTextAlignment:NSTextAlignmentRight];
        
        [self.background addSubview:listingName];
        [self.background addSubview:listingLocation];
        
        
        listingInfo = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width-190,self.frame.size.height-35 ,100 ,25 )];
        [listingInfo setFont:[UIFont fontWithName:@"verdana" size:13]];
        listingInfo.textColor = [UIColor colorWithRed:86.0f/255.0f green:1.0f/255.0f blue:72.0f/255.0f alpha:1.0f];
        [listingInfo setTextAlignment:NSTextAlignmentRight];
        
        UILabel *dot = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width-80,self.frame.size.height-25 ,6 ,6 )];
        dot.backgroundColor = [UIColor lightGrayColor];
        dot.layer.cornerRadius = 3.0f;
        dot.layer.masksToBounds = YES;
        
        UILabel *line = [[UILabel alloc]initWithFrame:CGRectMake(0,self.frame.size.height-45 ,self.frame.size.width,0.7 )];
        line.backgroundColor = [UIColor lightGrayColor];
        
        listingPrice = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width-62,self.frame.size.height-35 ,60 ,25 )];
        [listingPrice setFont:[UIFont fontWithName:@"verdana" size:14]];
        listingPrice.textColor = [UIColor whiteColor];
        [listingPrice setTextAlignment:NSTextAlignmentLeft];
        
        [self addSubview:listingInfo];
        [self addSubview:listingPrice];
        [self addSubview:dot];
        [self addSubview:line];
        
        self.backgroundColor = [UIColor whiteColor];
        UIButton *clickButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 295, 271)];
        [clickButton addTarget:self action:@selector(selectedIndex) forControlEvents:UIControlEventTouchUpInside];
        clickButton.backgroundColor = [UIColor clearColor];
        
        UIButton *profileButton = [[UIButton alloc]initWithFrame:CGRectMake(15,self.frame.size.height-90 ,70,70)];
        [profileButton addTarget:self action:@selector(openUserProfile) forControlEvents:UIControlEventTouchUpInside];
        profileButton.backgroundColor = [UIColor clearColor];
        
        panGestureRecognizer = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(beingDragged:)];
        
        
        [self addGestureRecognizer:panGestureRecognizer];
        [self addSubview:listingImageView];
        [self addSubview:background];
        [self addSubview: profilePicView];
        [self addSubview:clickButton];
        [self addSubview:profileButton];
        
        overlayView = [[OverlayView alloc]initWithFrame:CGRectMake(self.frame.size.width/2-30, 0, 100, 100)];
        overlayView.alpha = 0;
        [self addSubview:overlayView];
    }
    return self;
}


-(void)setupView
{
    self.layer.cornerRadius = 4;
    self.layer.shadowRadius = 3;
    self.layer.shadowOpacity = 0.2;
    self.layer.shadowOffset = CGSizeMake(1, 1);
}

//%%% called when you move your finger across the screen.
// called many times a second
-(void)beingDragged:(UIPanGestureRecognizer *)gestureRecognizer
{
    //%%% this extracts the coordinate data from your swipe movement. (i.e. How much did you move?)
    xFromCenter = [gestureRecognizer translationInView:self].x; //%%% positive for right swipe, negative for left
    yFromCenter = [gestureRecognizer translationInView:self].y; //%%% positive for up, negative for down
    
    //%%% checks what state the gesture is in. (are you just starting, letting go, or in the middle of a swipe?)
    switch (gestureRecognizer.state) {
            //%%% just started swiping
        case UIGestureRecognizerStateBegan:{
            self.originalPoint = self.center;
            break;
        };
            //%%% in the middle of a swipe
        case UIGestureRecognizerStateChanged:{
            //%%% dictates rotation (see ROTATION_MAX and ROTATION_STRENGTH for details)
            CGFloat rotationStrength = MIN(xFromCenter / ROTATION_STRENGTH, ROTATION_MAX);
            
            //%%% degree change in radians
            CGFloat rotationAngel = (CGFloat) (ROTATION_ANGLE * rotationStrength);
            
            //%%% amount the height changes when you move the card up to a certain point
            CGFloat scale = MAX(1 - fabsf(rotationStrength) / SCALE_STRENGTH, SCALE_MAX);
            
            //%%% move the object's center by center + gesture coordinate
            self.center = CGPointMake(self.originalPoint.x + xFromCenter, self.originalPoint.y + yFromCenter);
            
            //%%% rotate by certain amount
            CGAffineTransform transform = CGAffineTransformMakeRotation(rotationAngel);
            
            //%%% scale by certain amount
            CGAffineTransform scaleTransform = CGAffineTransformScale(transform, scale, scale);
            
            //%%% apply transformations
            self.transform = scaleTransform;
            [self updateOverlay:xFromCenter];
            
            break;
        };
            //%%% let go of the card
        case UIGestureRecognizerStateEnded: {
            [self afterSwipeAction];
            break;
        };
        case UIGestureRecognizerStatePossible:break;
        case UIGestureRecognizerStateCancelled:break;
        case UIGestureRecognizerStateFailed:break;
    }
}

//%%% checks to see if you are moving right or left and applies the correct overlay image
-(void)updateOverlay:(CGFloat)distance
{
    if (distance > 0) {
        overlayView.mode = GGOverlayViewModeRight;
    } else {
        overlayView.mode = GGOverlayViewModeLeft;
    }
    
    overlayView.alpha = MIN(fabs(distance)/100, 1.0);
}

//%%% called when the card is let go
- (void)afterSwipeAction
{
    if (xFromCenter > ACTION_MARGIN) {
        [self rightAction];
    } else if (xFromCenter < -ACTION_MARGIN) {
        [self leftAction];
    } else { //%%% resets the card
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.center = self.originalPoint;
                             self.transform = CGAffineTransformMakeRotation(0);
                             overlayView.alpha = 0;
                         }];
    }
}

//%%% called when a swipe exceeds the ACTION_MARGIN to the right
-(void)rightAction
{
    CGPoint finishPoint = CGPointMake(500, 2*yFromCenter +self.originalPoint.y);
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.center = finishPoint;
                     }completion:^(BOOL complete){
                         [self removeFromSuperview];
                     }];
    
    [delegate cardSwipedRight:self];
    
   // //NSLog(@"YES");
}

//%%% called when a swip exceeds the ACTION_MARGIN to the left
-(void)leftAction
{
    CGPoint finishPoint = CGPointMake(-500, 2*yFromCenter +self.originalPoint.y);
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.center = finishPoint;
                     }completion:^(BOOL complete){
                         [self removeFromSuperview];
                     }];
    
    [delegate cardSwipedLeft:self];
    
   // //NSLog(@"NO");
}

-(void)rightClickAction
{
    CGPoint finishPoint = CGPointMake(600, self.center.y);
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.center = finishPoint;
                         self.transform = CGAffineTransformMakeRotation(1);
                     }completion:^(BOOL complete){
                         [self removeFromSuperview];
                     }];
    
    [delegate cardSwipedRight:self];
    
   // //NSLog(@"YES");
}

-(void)leftClickAction
{
    CGPoint finishPoint = CGPointMake(-600, self.center.y);
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.center = finishPoint;
                         self.transform = CGAffineTransformMakeRotation(-1);
                     }completion:^(BOOL complete){
                         [self removeFromSuperview];
                     }];
    
    [delegate cardSwipedLeft:self];
    
  //  //NSLog(@"NO");
}

-(void)selectedIndex {
    
    [delegate cardSelected:self];
    
}

-(void)openUserProfile
{
    [delegate openProfile:self];
}

@end
