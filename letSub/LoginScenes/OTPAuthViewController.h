//
//  OTPAuthViewController.h
//  letSub
//
//  Created by Vishnu Varthan on 10/06/16.
//  Copyright © 2016 com.vishnuvarthan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OTPAuthViewController : UIViewController
@property(strong,nonatomic) NSMutableDictionary * paramDictionary;
@property (weak, nonatomic) IBOutlet UITextField *secretKeyTextField;

@end
