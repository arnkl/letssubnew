//
//  RegisterViewController.m
//  letSub
//
//  Created by Vishnu Varthan on 08/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillChangeFrameNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(keyboardFrameChanged:) name:UIKeyboardWillChangeFrameNotification object:nil ];
    

    // Do any additional setup after loading the view.
}

-(void)viewDidDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillChangeFrameNotification];
    [super viewDidDisappear:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

#pragma mark -

-(BOOL)confirmPassword
{
    return ([self.passwordTxtField.text isEqualToString:self.confirmPasswordTxtField.text]);
    
}

-(BOOL)isFieldsFilled
{
    bool fieldsFilled = ([self.emailTxtField.text length]!=0 && [self.passwordTxtField.text length]!=0&& [self.cityTxtField.text length]!=0&& [self.usernameTxtField.text length]!=0);
    
    return fieldsFilled;
}

- (IBAction)registerNewUser:(id)sender {
    
    
    
    if([self isFieldsFilled])
    {
        if([self confirmPassword])
        {
            
            [self logOutExistingUser];
            NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:self.emailTxtField.text,@"emailID",self.passwordTxtField.text,@"password",self.usernameTxtField.text,@"username",self.cityTxtField.text,@"location", nil];
            
            NSString *urlAddress = [NSString stringWithFormat:@"users/sendOtp.json"];
            
            [AppDelegate showLoadingView:self];
            
            LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
            [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
                //NSLog(@"Success: %@", responseObject);
                
                responseObject = [responseObject objectForKey:@"response"];
                NSString *status =[responseObject objectForKey:@"status"];
                
                [AppDelegate hideLoadingIndicator];
                
                if([status boolValue] ==1)
                {
                    [[AppDelegate instance]openUserAuthenticationPageWithParameters:dict];
                    
                }
                else
                {
                    NSString *message =[responseObject objectForKey:@"message"];
                    [self showAlertWithMessage:message];
                }
                
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                [self showAlertWithMessage:@"User registration failed."];
                [AppDelegate hideLoadingIndicator];
            }];

        }
        else
        {
            [self showAlertWithMessage:@"Passwords do not match."];
        }
    }
    else
    {
        [self showAlertWithMessage:@"One or more fields are left empty."];
    }
    
    
    
}


-(void)logOutExistingUser
{
    
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"TermsAccepted"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [[AppDelegate instance]deleteAllEntities:@"Messages"];
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logOut];

}

- (IBAction)cancelRegistration:(id)sender {
    
    [[AppDelegate instance]openLoginPage];
}


-(void)showAlertWithMessage :(NSString*)message
{
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"Error";
    alertViewController.message = message;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    alertViewController.buttonColor = [UIColor colorWithRed:86.0f/255.0f green:1.0f/255.0f blue:71.0f/255.0f alpha:0.90f];
    
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Ok"
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                          }]];
    
    
    [self presentViewController:alertViewController animated:YES completion:nil];
    

}

#pragma mark - UITextField Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
        [textField resignFirstResponder];
     _table.scrollEnabled = NO;
    
        
        return YES;
}



#pragma mark - TableView Delegate & Data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return 2;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if(indexPath.row==0)
    {
        cell =(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        
    }
    else
    {
        cell =(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"contentCell" forIndexPath:indexPath];
        _emailTxtField = (UITextField*)[cell viewWithTag:800];
        _usernameTxtField = (UITextField*)[cell viewWithTag:801];
        _passwordTxtField = (UITextField*)[cell viewWithTag:802];
        _cityTxtField = (UITextField*)[cell viewWithTag:803];
        _confirmPasswordTxtField = (UITextField*)[cell viewWithTag:805];
        _btnSignUp = (UIButton*)[cell viewWithTag:806];
        [[_btnSignUp layer] setCornerRadius:20.0];
        [[_btnSignUp layer] setBorderColor:(__bridge CGColorRef _Nullable)([UIColor clearColor])];
        [[_btnSignUp layer] setBorderWidth:0.1];
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = _btnSignUp.bounds;
        [gradient setBorderWidth:0.1];
        [gradient setCornerRadius:20.0];
        [gradient setStartPoint:CGPointMake(0.0, 0.5)];
        [gradient setEndPoint:CGPointMake(1.0, 0.5)];
        [gradient setBorderColor:(__bridge CGColorRef _Nullable)([UIColor clearColor])];
        gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:0.267 green:0.176 blue:0.996 alpha:1.000] CGColor], (id)[[UIColor colorWithRed:0.467 green:0.000 blue:0.522 alpha:1.000] CGColor], nil];
        [_btnSignUp.layer insertSublayer:gradient atIndex:0];
        
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    if(indexPath.row == 0)
        return  180;
    return 600;
}


#pragma mark - KeyBoard Notifications
- (void)keyboardFrameChanged:(NSNotification *)aNotification
{
    NSDictionary *userInfo = aNotification.userInfo;
    _table.scrollEnabled = YES;
    
    //
    // Get keyboard size.
    
    NSValue *beginFrameValue = userInfo[UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardBeginFrame = [self.view convertRect:beginFrameValue.CGRectValue fromView:nil];
    
    NSValue *endFrameValue = userInfo[UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardEndFrame = [self.view convertRect:endFrameValue.CGRectValue fromView:nil];
    
    //
    // Get keyboard animation.
    
    NSNumber *durationValue = userInfo[UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration = durationValue.doubleValue;
    
    NSNumber *curveValue = userInfo[UIKeyboardAnimationCurveUserInfoKey];
    UIViewAnimationCurve animationCurve = curveValue.intValue;
    
    //
    // Create animation.
    
    CGRect viewFrame = self.view.frame;
    viewFrame.size.height = (keyboardBeginFrame.origin.y - viewFrame.origin.y);
    self.view.frame = viewFrame;
    
    void (^animations)() = ^() {
        CGRect viewFrame = self.view.frame;
        viewFrame.size.height = (keyboardEndFrame.origin.y - viewFrame.origin.y);
        self.view.frame = viewFrame;
    };
    
    //
    // Begin animation.
    [UIView animateWithDuration:animationDuration
                          delay:0.0
                        options:(animationCurve << 16)
                     animations:animations
                     completion:nil];
}


@end
