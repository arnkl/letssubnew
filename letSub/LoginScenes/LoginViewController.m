//
//  LoginViewController.m
//  letSub
//
//  Created by Vishnu Varthan on 08/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()<UITextFieldDelegate>

@end

@implementation LoginViewController

int terms=0,privacy=0;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.emailTxtField.delegate =self;
    self.passwordTxtField.delegate =self;
 
}



-(void)viewDidAppear:(BOOL)animated
{
    [[_btnSignIn layer] setCornerRadius:20.0];
    [[_btnSignIn layer] setBorderColor:(__bridge CGColorRef _Nullable)([UIColor clearColor])];
    [[_btnSignIn layer] setBorderWidth:0.1];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = _btnSignIn.bounds;
    [gradient setBorderWidth:0.1];
    [gradient setCornerRadius:20.0];
    [gradient setStartPoint:CGPointMake(0.0, 0.5)];
    [gradient setEndPoint:CGPointMake(1.0, 0.5)];
    [gradient setBorderColor:(__bridge CGColorRef _Nullable)([UIColor clearColor])];
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:0.267 green:0.176 blue:0.996 alpha:1.000] CGColor], (id)[[UIColor colorWithRed:0.467 green:0.000 blue:0.522 alpha:1.000] CGColor], nil];
    [_btnSignIn.layer insertSublayer:gradient atIndex:0];
    
  
    
    
    [[_btnSignUp layer] setCornerRadius:20.0];
    [[_btnSignUp layer] setBorderWidth:1.0];
    [[_btnSignUp layer] setBorderColor:[[UIColor colorWithRed:0.384 green:0.000 blue:0.686 alpha:1.000] CGColor]];
    
    
    [[_btnFb layer] setCornerRadius:20.0];
    [[_btnFb layer] setBorderColor:(__bridge CGColorRef _Nullable)([UIColor clearColor])];
    [[_btnFb layer] setBorderWidth:0.1];
    
    _btnFb.enabled = NO;
    [_btnFb setBackgroundColor:[UIColor grayColor]];
    [_btnFb setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
    
    //_checkbox = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 64, 64)];
    [_checkbox setBackgroundImage:[UIImage imageNamed:@"CheckboxOff.png"] forState:UIControlStateNormal];
    [_checkbox setBackgroundImage:[UIImage imageNamed:@"CheckboxOn.png"] forState:UIControlStateSelected];
    [_checkbox addTarget:self action:@selector(checkboxSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    [_checkbox1 setBackgroundImage:[UIImage imageNamed:@"CheckboxOff.png"] forState:UIControlStateNormal];
    [_checkbox1 setBackgroundImage:[UIImage imageNamed:@"CheckboxOn.png"] forState:UIControlStateSelected];
    [_checkbox1 addTarget:self action:@selector(checkbox1Selected:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
        if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
        {
        
                if ([FBSDKAccessToken currentAccessToken]) {      //  ckeck if user is logged in using Facebook.
                    
                    //NSLog(@"user login present %@",[FBSDKAccessToken currentAccessToken]);
                    
                    [self fetchUserDetails];
                    _splashScreen.hidden = NO;
                    
                }
                
                else
                {
                    //NSLog(@"fb login unavailable..");
                    _splashScreen.hidden = YES;
                }
            }
    
        else
        {
            if([[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"])
            {
                
                self.emailTxtField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"];
                self.passwordTxtField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"];
                [self userLogin:self];
                
            }
            else
            {
                //NSLog(@"user not logged in..");
                _splashScreen.hidden = YES;
            }
            
        }

    [super viewDidAppear:YES];
}

#pragma mark -

-(void)checkboxSelected:(id)sender{
    
    
    if([_checkbox isSelected]==YES)
    {
        [_checkbox setSelected:NO];
        terms = 0;
        _btnFb.enabled = NO;
        [_btnFb setBackgroundColor:[UIColor grayColor]];
        [_btnFb setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
    }
    else{
        [_checkbox setSelected:YES];
        terms = 1;
        if(terms ==1 && privacy==1){
        _btnFb.enabled = YES;
        [_btnFb setBackgroundColor:[UIColor colorWithRed:30.0f/255.0f green:90.0f/255.0f blue:192.0f/255.0f alpha:1.0f]];
        [_btnFb setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
    }
    
}

-(void)checkbox1Selected:(id)sender{
    
    
    if([_checkbox1 isSelected]==YES)
    {
        [_checkbox1 setSelected:NO];
        privacy = 0;
        _btnFb.enabled = NO;
        [_btnFb setBackgroundColor:[UIColor grayColor]];
        [_btnFb setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
    }
    else{
        [_checkbox1 setSelected:YES];
        privacy = 1;
        if(terms ==1 && privacy==1){
        _btnFb.enabled = YES;
        [_btnFb setBackgroundColor:[UIColor colorWithRed:30.0f/255.0f green:90.0f/255.0f blue:192.0f/255.0f alpha:1.0f]];
        [_btnFb setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
    }
    
}

- (IBAction)onClickTerms:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://letssub.com/terms-condition.php"]];
}

- (IBAction)onClickPrivacyPolicy:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://letssub.com/privacy-policy.php"]];
}

-(void)showLoginFailureWithmessage :(NSString*) message
{
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"Error";
    alertViewController.message = message;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    alertViewController.buttonColor = [UIColor colorWithRed:86.0f/255.0f green:1.0f/255.0f blue:71.0f/255.0f alpha:0.90f];
    
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Ok"
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                          }]];
    
    
    [self presentViewController:alertViewController animated:YES completion:nil];
    
}


- (IBAction)userLogin:(id)sender
{
    
    if([self isFieldsFilled])
    {
    
    [AppDelegate showLoadingView:self];

        
    NSString *urlAddress = [NSString stringWithFormat:@"users/login.json"];
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:self.emailTxtField.text,@"email",self.passwordTxtField.text,@"password",[[AppDelegate instance]apnsTokenString],@"deviceAPNSToken", nil];
    
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        responseObject = [responseObject objectForKey:@"response"];
        NSString *status =[responseObject objectForKey:@"status"];
        
        [AppDelegate hideLoadingIndicator];
        
        if([status boolValue] ==1)
        {
            [[NSUserDefaults standardUserDefaults] setObject:self.emailTxtField.text forKey:@"loggedUserEmail"];
            [[NSUserDefaults standardUserDefaults] setObject:self.passwordTxtField.text forKey:@"loggedUserAuthentictionID"];
            [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"user_id"] forKey:@"loggedUserId"];
            [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"username"] forKey:@"loggedUserName"];
            [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"location"]forKey:@"loggedUserLocation"];
            [[NSUserDefaults standardUserDefaults] setObject:[[responseObject objectForKey:@"education"] isEqualToString:@"(empty)"]?@"":[responseObject objectForKey:@"education"] forKey:@"loggedUserEducation"];
            [[NSUserDefaults standardUserDefaults] setObject:[[responseObject objectForKey:@"age_range"] isEqualToString:@"(empty)"]?@"":[responseObject objectForKey:@"age_range"] forKey:@"loggedUserAge"];
            [[NSUserDefaults standardUserDefaults] setObject:[[responseObject objectForKey:@"description"] isEqualToString:@"(empty)"]?@"write a description about yourself ....":[responseObject objectForKey:@"description"] forKey:@"loggedUserdescription"];
            [[NSUserDefaults standardUserDefaults] setObject:[[responseObject objectForKey:@"preferredLocation"] isEqualToString:@"(empty)"]?@"":[responseObject objectForKey:@"preferredLocation"] forKey:@"loggedUserPreferredLocation"];
            [[NSUserDefaults standardUserDefaults] setObject:[[responseObject objectForKey:@"jobTitle"] isEqualToString:@"(empty)"]?@"":[responseObject objectForKey:@"jobTitle"] forKey:@"loggedUserJob"];
            [[NSUserDefaults standardUserDefaults] setObject:[[responseObject objectForKey:@"company"] isEqualToString:@"(empty)"]?@"":[responseObject objectForKey:@"company"] forKey:@"loggedUserCompany"];
            [[NSUserDefaults standardUserDefaults] setObject:[[responseObject objectForKey:@"minBudget"] isEqualToString:@"(empty)"]?@"1000":[responseObject objectForKey:@"minBudget"] forKey:@"loggedUserMinBudget"];
            [[NSUserDefaults standardUserDefaults] setObject:[[responseObject objectForKey:@"maxBudget"] isEqualToString:@"(empty)"]?@"3000":[responseObject objectForKey:@"maxBudget"] forKey:@"loggedUserMaxBudget"];
            [[NSUserDefaults standardUserDefaults] setObject:[[responseObject objectForKey:@"moveInDate"] isEqualToString:@"(empty)"]?@"Any":[responseObject objectForKey:@"moveInDate"] forKey:@"loggedUserMoveInDate"];
            
            
            
            [[NSUserDefaults standardUserDefaults] setObject:@"Normal" forKey:@"loginType"];
            [[NSUserDefaults standardUserDefaults]setObject:UIImagePNGRepresentation([UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[responseObject valueForKey:@"picture"]]]]) forKey:@"loggedUserPicture"];
            [[NSUserDefaults standardUserDefaults]setObject:UIImagePNGRepresentation([UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[responseObject valueForKey:@"coverPicture"]]]]) forKey:@"loggedUserCover"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            [self showAppScene];
            
        }
        else
        {
            NSString *message =[responseObject objectForKey:@"message"];
            //NSLog(@"login error...%@",message);
             _splashScreen.hidden = YES;
            [self showLoginFailureWithmessage:message];
        }

               
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        //NSLog(@"Error: %@", error);
        [AppDelegate hideLoadingIndicator];
        _splashScreen.hidden = YES;
        [self showLoginFailureWithmessage:@"Login Failed !!"];
    }];
    }
    else
    {
        [self showLoginFailureWithmessage:@"One or more fields are empty."];
        
    }
}

-(BOOL)isFieldsFilled
{
    bool fieldsFilled = ([self.emailTxtField.text length]!=0 && [self.passwordTxtField.text length]!=0);
    
    return fieldsFilled;
}


#pragma mark - Facebook Login


- (IBAction)openTC:(id)sender {
    
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsViewController"];
    [self presentViewController:vc animated:YES completion:nil];
    
}

- (IBAction)guestLogin:(id)sender {
    
    
  //  [self showAppScene];
    
}

- (IBAction)faceBookLogin:(id)sender {
    
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc]init];
    [loginManager logInWithReadPermissions:@[@"public_profile", @"email",@"user_friends",@"user_location",@"user_about_me",@"user_work_history",@"user_education_history",@"user_likes"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        //NSLog(@"result...%@",result);
        
        if(!error && result.token){
            [self fetchUserDetails];
        }
        else{
            [self showLoginFailureWithmessage:@"Login Failed !!"];
        }
        
    }];
    
    
}


-(void)fetchUserDetails
{
    @try {
        
        [AppDelegate showLoadingView:self];
        
        NSMutableDictionary *userParameters = [[NSMutableDictionary alloc]init];
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{ @"fields" : @"id,name,picture.width(300).height(300),email,location,work,cover.width(300).height(300),age_range,bio,education,likes"}]startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            if (!error) {
                
                NSDictionary *ageDict = [result objectForKey:@"age_range"];
                NSString * ageString;
                
                if([ageDict objectForKey:@"min"] && [ageDict objectForKey:@"max"])
                {
                    ageString = [NSString stringWithFormat:@"%@ < %@",[ageDict objectForKey:@"min"],[ageDict objectForKey:@"max"]];
                }
                else if ([ageDict objectForKey:@"max"])
                {
                    ageString = [NSString stringWithFormat:@"< %@",[ageDict objectForKey:@"max"]];
                }
                else{
                    
                    ageString = [NSString stringWithFormat:@"%@ +",[ageDict objectForKey:@"min"]];
                }
                
                
                NSDictionary*educationDict = [[result objectForKey:@"education"]lastObject];
                educationDict = [educationDict objectForKey:@"school"];
                NSString *education = [educationDict objectForKey:@"name"];
                
                NSDictionary * work = [result objectForKey:@"work"][0];
                NSString *company = [[work objectForKey:@"employer"]objectForKey:@"name"];
                NSString *title = [[work objectForKey:@"position"]objectForKey:@"name"];
                
                NSString* userID=[result valueForKey:@"id"];
                NSString *emailOfLoginUser =[result valueForKey:@"email"];
                [[NSUserDefaults standardUserDefaults] setObject:@"Facebook" forKey:@"loginType"];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"TermsAccepted"];
                [[NSUserDefaults standardUserDefaults]synchronize];  //  remove every stored data from nsuser default other than terms&condition accepted data.
                
                                    
                    if(emailOfLoginUser)
                    {
                        [userParameters setObject:emailOfLoginUser forKey:@"email"];
                        [[NSUserDefaults standardUserDefaults]setObject:emailOfLoginUser forKey:@"loggedUserEmail"];

                    }
                
                if(![[[NSUserDefaults standardUserDefaults]objectForKey:@"customUserImage"] isEqualToString:@"YES"]) {
                    
                    NSString *imageStringOfLoginUser = [[[result valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"];
                    
                    if(imageStringOfLoginUser)
                    [userParameters setObject:imageStringOfLoginUser forKey:@"picture"];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:UIImagePNGRepresentation([UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageStringOfLoginUser]]]) forKey:@"loggedUserPicture"];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"customUserImage"];
                    
                }
                
                
                if(![[[NSUserDefaults standardUserDefaults]objectForKey:@"customUserCover"] isEqualToString:@"YES"]) {
                    
                    NSString *imageStringOfLoginUser = [[result valueForKey:@"cover"] valueForKey:@"source"];
                    
                    if(imageStringOfLoginUser)
                    [userParameters setObject:imageStringOfLoginUser forKey:@"coverPicture"];

                    
                    [[NSUserDefaults standardUserDefaults]setObject:UIImagePNGRepresentation([UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageStringOfLoginUser]]]) forKey:@"loggedUserCover"];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"customUserCover"];
                    
                }
                
                if(![[[NSUserDefaults standardUserDefaults]objectForKey:@"customUserName"] isEqualToString:@"YES"])
                {
                    if([result valueForKey:@"name"])
                    [userParameters setObject: [result valueForKey:@"name"] forKey:@"username"];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:[result valueForKey:@"name"] forKey:@"loggedUserName"];
                    [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"customUserName"];
                }
                
                if(![[[NSUserDefaults standardUserDefaults]objectForKey:@"customUserJobTitle"] isEqualToString:@"YES"]) {
                    
                    if(title)
                    [userParameters setObject: title forKey:@"jobTitle"];

                    [[NSUserDefaults standardUserDefaults]setObject:title forKey:@"jobTitle"];
                    [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"customUserJobTitle"];
                    
                }
                
                if(![[[NSUserDefaults standardUserDefaults]objectForKey:@"customUserCompany"] isEqualToString:@"YES"]) {
                  
                    if(company)
                    [userParameters setObject: company forKey:@"company"];

                    [[NSUserDefaults standardUserDefaults]setObject:company forKey:@"company"];
                    [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"customUserCompany"];
                    
                }
                
                
                if(![[[NSUserDefaults standardUserDefaults]objectForKey:@"customUserBio"] isEqualToString:@"YES"]) {
                    
                    if([result objectForKey:@"bio"])
                    [userParameters setObject: [result objectForKey:@"bio"] forKey:@"description"];

                    [[NSUserDefaults standardUserDefaults]setObject:[result objectForKey:@"bio"] forKey:@"loggedUserdescription"];
                    [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"customUserBio"];
                    
                }
                
                
                if(![[[NSUserDefaults standardUserDefaults]objectForKey:@"customUserAge"] isEqualToString:@"YES"]) {
                    
                    if(ageString)
                    [userParameters setObject: ageString forKey:@"age_range"];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:ageString forKey:@"loggedUserAge"];
                    [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"customUserAge"];
                    
                }
                
                if(![[[NSUserDefaults standardUserDefaults]objectForKey:@"customUserEducation"] isEqualToString:@"YES"]) {
                   
                    if(education)
                    [userParameters setObject:education forKey:@"education"];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:education forKey:@"loggedUserEducation"];
                    [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"customUserEducation"];
                    
                }
                
                if(![[[NSUserDefaults standardUserDefaults]objectForKey:@"customUserLocation"] isEqualToString:@"YES"]) {
                    
                    if([[result valueForKey:@"location"] valueForKey:@"name"])
                    [userParameters setObject:[[result valueForKey:@"location"] valueForKey:@"name"] forKey:@"location"];

                    [[NSUserDefaults standardUserDefaults]setObject:[[result valueForKey:@"location"] valueForKey:@"name"] forKey:@"loggedUserLocation"];
                    [[NSUserDefaults standardUserDefaults]setObject:@"NO" forKey:@"customUserLocation"];
                    
                }
                
                [userParameters setObject:userID forKey:@"oauth_id"];
                
                if([[AppDelegate instance]apnsTokenString])
                   [userParameters setObject:[[AppDelegate instance]apnsTokenString] forKey:@"deviceAPNSToken"];
                
               


                
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                // email can be empty too for fb login
                NSString *urlAddress = [NSString stringWithFormat:@"users/oauth_login.json"];
                
                LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
                [client POST:urlAddress parameters:userParameters  progress:nil  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    
                    NSLog(@"JSON: %@", responseObject);
                    responseObject = [responseObject objectForKey:@"response"];
                    NSString *status =[responseObject objectForKey:@"status"];
                    
                    [AppDelegate hideLoadingIndicator];
                    if([status boolValue] == 1)
                    {
                        
                        [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"userid"] forKey:@"loggedUserId"];
                        [[NSUserDefaults standardUserDefaults] setObject:userID forKey:@"loggedUserAuthentictionID"];
                        [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"username"] forKey:@"loggedUserName"];
                        [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"location"]forKey:@"loggedUserLocation"];
                        [[NSUserDefaults standardUserDefaults] setObject:[[responseObject objectForKey:@"description"] isEqualToString:@"(empty)"]?@"write a description about yourself ....":[responseObject objectForKey:@"description"] forKey:@"loggedUserdescription"];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:[[responseObject objectForKey:@"preferredLocation"] isEqualToString:@"(empty)"]?@"":[responseObject objectForKey:@"preferredLocation"] forKey:@"loggedUserPreferredLocation"];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:[[responseObject objectForKey:@"jobTitle"] isEqualToString:@"(empty)"]?@"":[responseObject objectForKey:@"jobTitle"] forKey:@"loggedUserJob"];
                        [[NSUserDefaults standardUserDefaults] setObject:[[responseObject objectForKey:@"company"] isEqualToString:@"(empty)"]?@"":[responseObject objectForKey:@"company"] forKey:@"loggedUserCompany"];
                        [[NSUserDefaults standardUserDefaults] setObject:[[responseObject objectForKey:@"minBudget"] isEqualToString:@"(empty)"]?@"1000":[responseObject objectForKey:@"minBudget"] forKey:@"loggedUserMinBudget"];
                        [[NSUserDefaults standardUserDefaults] setObject:[[responseObject objectForKey:@"age_range"] isEqualToString:@"(empty)"]?@"":[responseObject objectForKey:@"age_range"] forKey:@"loggedUserAge"];
                        [[NSUserDefaults standardUserDefaults] setObject:[[responseObject objectForKey:@"education"] isEqualToString:@"(empty)"]?@"":[responseObject objectForKey:@"education"] forKey:@"loggedUserEducation"];
                        [[NSUserDefaults standardUserDefaults] setObject:[[responseObject objectForKey:@"maxBudget"] isEqualToString:@"(empty)"]?@"3000":[responseObject objectForKey:@"maxBudget"] forKey:@"loggedUserMaxBudget"];
                        [[NSUserDefaults standardUserDefaults] setObject:[[responseObject objectForKey:@"moveInDate"] isEqualToString:@"(empty)"]?@"Any":[responseObject objectForKey:@"moveInDate"] forKey:@"loggedUserMoveInDate"];
                        
                        [[NSUserDefaults standardUserDefaults]synchronize];
                        [self showAppScene];
                    }
                    else
                    {
                        //NSLog(@"login error...%@",message);
                        [self showLoginFailureWithmessage:[responseObject objectForKey:@"message"]];
                        _splashScreen.hidden = YES;
                        
                    }
                    
                } failure:^(NSURLSessionTask *operation, NSError *error) {
                    NSLog(@"Error: %@", error);
                    [AppDelegate hideLoadingIndicator];
                    _splashScreen.hidden = YES;
                    [self showLoginFailureWithmessage:@"Login Failed !!"];
                    
                }];
                
                NSArray *intrestArray = [[result objectForKey:@"likes"]objectForKey:@"data"];
                
                for (NSDictionary *intrestDict in intrestArray) {
                    
                    [self getUserInterestDetailsWithID:[intrestDict objectForKey:@"id"] andName:[intrestDict objectForKey:@"name"]];
                }

            }
            else
            {
                //NSLog(@"cannot connect to fb");
                [AppDelegate hideLoadingIndicator];
                _splashScreen.hidden = YES;
            }
            
        }];
    }
    @catch (NSException *exception) {
        
        //NSLog(@"exc %@",exception);
    }
}

-(void)getUserInterestDetailsWithID :(NSString*)intrestID andName:(NSString*)intrestName
{
    
    NSString *graphPath = [NSString stringWithFormat:@"/%@",intrestID];
    NSDictionary *params1 = @{
                              @"fields": @"picture.width(300).height(300)",
                              };
    
    FBSDKGraphRequest *request1 = [[FBSDKGraphRequest alloc]
                                   initWithGraphPath:graphPath
                                   parameters:params1
                                   HTTPMethod:@"GET"];
    [request1 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                           id result,
                                           NSError *error) {
        
        NSString *intrestImageUrl = [[[result objectForKey:@"picture"]objectForKey:@"data"]objectForKey:@"url"];
        
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:intrestID,@"id",intrestName,@"name",intrestImageUrl,@"picture", nil];
        [[AppDelegate instance].userIntrestArray addObject:dict];
    }];

}


#pragma mark -

- (IBAction)openPasswordChangePage:(id)sender {
    
    [[AppDelegate instance]openForgotPassword];

}

- (IBAction)openRegistrationPage:(id)sender {
    
    [[AppDelegate instance]openRegistrationPage];
}

-(void)showAppScene
{
    [[AppDelegate instance]openHomeView];
    
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
    
        [self performSelector:@selector(sendUserIntrest) withObject:nil afterDelay:20];
    
    }
}

-(void)sendUserIntrest
{
    NSString *urlAddress = [NSString stringWithFormat:@"users/storeInterest.json"];
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    
    NSData* data = [ NSJSONSerialization dataWithJSONObject:[AppDelegate instance].userIntrestArray options:NSJSONWritingPrettyPrinted error:nil ];
    NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:jsonString,@"interest",[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"userID", nil];
    
  //  NSLog(@"sending intrest...%@",dict);

    [client POST:urlAddress parameters:dict  progress:nil  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject){
        
    }failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];

}

#pragma mark - Text field delegates
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    return YES;
}

@end
