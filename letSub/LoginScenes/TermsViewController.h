//
//  TermsViewController.h
//  letSub
//
//  Created by Vishnu Varthan .P on 11/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *termsTextView;

@end
