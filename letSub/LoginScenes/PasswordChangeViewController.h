//
//  PasswordChangeViewController.h
//  letSub
//
//  Created by Vishnu Varthan .P on 18/04/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PasswordChangeViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UITextField *oldPasswordTxtField;
@property (weak, nonatomic) IBOutlet UITextField *newpasswordTxtField,*confirmPasswordTxtField;
@property (weak, nonatomic) IBOutlet UITextField *emailTxtField;
@end
