//
//  LoginViewController.h
//  letSub
//
//  Created by Vishnu Varthan on 08/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *emailTxtField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTxtField;
@property (weak, nonatomic) IBOutlet UIImageView *splashScreen;

- (IBAction)openTC:(id)sender;
- (IBAction)guestLogin:(id)sender;
-(IBAction)faceBookLogin:(id)sender;
- (IBAction)userLogin:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSignIn;
@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;
@property (weak, nonatomic) IBOutlet UIButton *btnFb;

@property(strong, nonatomic) IBOutlet UIButton *checkbox;
@property (strong, nonatomic) IBOutlet UIButton *checkbox1;

-(void)checkboxSelected:(id)sender;
-(void)checkbox1Selected:(id)sender;

@end
