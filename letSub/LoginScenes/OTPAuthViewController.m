//
//  OTPAuthViewController.m
//  letSub
//
//  Created by Vishnu Varthan on 10/06/16.
//  Copyright © 2016 com.vishnuvarthan. All rights reserved.
//

#import "OTPAuthViewController.h"

@interface OTPAuthViewController ()

@end

@implementation OTPAuthViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)cancelVerification:(id)sender {
    
    [[AppDelegate instance]openRegistrationPage];
}

- (IBAction)verifyAndRegister:(id)sender {
    
    if([self isFieldsFilled])
    {
        
        NSString *urlAddress = [NSString stringWithFormat:@"users/signup.json"];
        [AppDelegate showLoadingView:self];
        
        [self.paramDictionary setValue:self.secretKeyTextField.text forKey:@"securityKey"];
        
       // NSLog(@"paramaters...%@",self.paramDictionary);
        
        
        LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
        [client POST:urlAddress parameters:self.paramDictionary progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            //NSLog(@"Success: %@", responseObject);
            
            responseObject = [responseObject objectForKey:@"response"];
            NSString *status =[responseObject objectForKey:@"status"];
            
            [AppDelegate hideLoadingIndicator];
            
            if([status boolValue] ==1)
            {
                [[NSUserDefaults standardUserDefaults]setObject:[responseObject valueForKey:@"user_id"] forKey:@"loggedUserId"];
                [[NSUserDefaults standardUserDefaults] setObject:[self.paramDictionary objectForKey:@"username"] forKey:@"loggedUserName"];
                [[NSUserDefaults standardUserDefaults] setObject:[self.paramDictionary objectForKey:@"emailID"] forKey:@"loggedUserEmail"];
                [[NSUserDefaults standardUserDefaults] setObject:[self.paramDictionary objectForKey:@"password"] forKey:@"loggedUserAuthentictionID"];
                [[NSUserDefaults standardUserDefaults] setObject:[self.paramDictionary objectForKey:@"location"] forKey:@"loggedUserLocation"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                [[AppDelegate instance]openHomeView];
                
            }
            else
            {
                NSString *message =[responseObject objectForKey:@"message"];
                [self showAlertWithMessage:message];
            }
            
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            [self showAlertWithMessage:@"User registration failed."];
            [AppDelegate hideLoadingIndicator];
        }];
        
    }
    else{
        
        [self showAlertWithMessage:@"enter the secret key"];
    }
    
}

- (IBAction)resendOTP:(id)sender {
    
    
}

-(void)showAlertWithMessage :(NSString*)message
{
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"Error";
    alertViewController.message = message;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    alertViewController.buttonColor = [UIColor colorWithRed:86.0f/255.0f green:1.0f/255.0f blue:71.0f/255.0f alpha:0.90f];
    
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Ok"
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                          }]];
    
    
    [self presentViewController:alertViewController animated:YES completion:nil];
    
    
}

-(BOOL)isFieldsFilled
{
    bool fieldsFilled = [self.secretKeyTextField.text length]!=0 ;
    
    return fieldsFilled;
}

@end
