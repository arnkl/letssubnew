//
//  TermsViewController.m
//  letSub
//
//  Created by Vishnu Varthan .P on 11/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "TermsViewController.h"

@interface TermsViewController ()
{
     BOOL acceptedUserAgreement;
}
- (IBAction)closePage:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet UIButton *checkButton;
@end

@implementation TermsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    NSAttributedString *attributedStringWithRtf = [[NSAttributedString alloc]initWithFileURL:[NSURL fileURLWithPath:[[NSBundle mainBundle]pathForResource:@"terms&conditions" ofType:@"rtf"] ] options:@{NSDocumentTypeDocumentAttribute:NSRTFTextDocumentType} documentAttributes:nil error:nil];
    self.termsTextView.attributedText=attributedStringWithRtf;
}

#pragma mark - 

- (IBAction)termsAccepted:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"TermsAccepted"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (IBAction)agreeButtonCLicked:(id)sender {
    
    if (!acceptedUserAgreement) {
        [_checkButton setBackgroundImage:[UIImage imageNamed:@"check_mark.png"] forState:UIControlStateNormal];
        acceptedUserAgreement = YES;
        _continueButton.hidden=NO;
    }
    
    else if (acceptedUserAgreement) {
        [_checkButton setBackgroundImage:[UIImage imageNamed:@"check_box.png"] forState:UIControlStateNormal];
        acceptedUserAgreement = NO;
        _continueButton.hidden=YES;
    }

    
}


- (IBAction)closePage:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
