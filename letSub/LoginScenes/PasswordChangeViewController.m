//
//  PasswordChangeViewController.m
//  letSub
//
//  Created by Vishnu Varthan .P on 18/04/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "PasswordChangeViewController.h"

@interface PasswordChangeViewController ()
{
    bool  isForgotPassword;
}
@end

@implementation PasswordChangeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isForgotPassword = NO;
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillChangeFrameNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(keyboardFrameChanged:) name:UIKeyboardWillChangeFrameNotification object:nil ];
    
    
    // Do any additional setup after loading the view.
}

-(void)viewDidDisappear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillChangeFrameNotification];
}

#pragma mark -

-(IBAction)updatePassword:(id)sender{
   
    
    if([self isFieldsFilled])
    {
        
        if([self confirmPassword])
        {
            
            [AppDelegate showLoadingView:self];
            NSString *urlAddress = [NSString stringWithFormat:@"users/changePassword.json"];
            NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:_oldPasswordTxtField.text,@"oldPassword",_newpasswordTxtField.text,@"newPassword",_emailTxtField.text,@"emailID",nil];
            
            LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
            
            [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                //NSLog(@"JSON: %@", responseObject);
                NSString *status =[responseObject objectForKey:@"status"];
                
                [AppDelegate hideLoadingIndicator];
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                //NSLog(@"Error: %@", error);
                [AppDelegate hideLoadingIndicator];
            }];
        }
        else{
            
            [self showAlertWithMessage:@"New passwords do not  match."];
        }
    }
    else{
         [self showAlertWithMessage:@"One or more fields are left empty."];
    }
    
}

-(void)showAlertWithMessage :(NSString*)message
{
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"Error";
    alertViewController.message = message;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    alertViewController.buttonColor = [UIColor colorWithRed:86.0f/255.0f green:1.0f/255.0f blue:71.0f/255.0f alpha:0.90f];
    
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Ok"
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                          }]];
    
    
    [self presentViewController:alertViewController animated:YES completion:nil];
    
    
}


- (IBAction)cancelPasswordChange:(id)sender {
    
    [[AppDelegate instance]openLoginPage];
}


-(IBAction)forgotPassword:(id)sender{
    
    isForgotPassword = YES;
    [self.table reloadData];
    
}
- (IBAction)sendNewPassword:(id)sender {
    
    if([self.emailTxtField.text length]!=0)
    {
        [AppDelegate showLoadingView:self];

        NSString *urlAddress = [NSString stringWithFormat:@"users/forgotPassword.json"];
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:_emailTxtField.text,@"emailID",nil];
        
        LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
        
        [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            //NSLog(@"JSON: %@", responseObject);
            responseObject = [responseObject objectForKey:@"response"];
            NSString *status = [responseObject objectForKey:@"status"];
            [AppDelegate hideLoadingIndicator];
            if([status boolValue]==1)
            {
                [self showAlertWithMessage:@"New Password sent to your email."];

            }
            else{
                
                [self showAlertWithMessage:[responseObject objectForKey:@"message"]];
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            //NSLog(@"Error: %@", error);
            [AppDelegate hideLoadingIndicator];
        }];
    }
    else{
        
        [self showAlertWithMessage:@"Enter your registered email."];
        
    }
}


-(BOOL)confirmPassword
{
    return ([self.newpasswordTxtField.text isEqualToString:self.confirmPasswordTxtField.text]);
    
}

-(BOOL)isFieldsFilled
{
    bool fieldsFilled = ([self.emailTxtField.text length]!=0 && [self.newpasswordTxtField.text length]!=0 && [self.oldPasswordTxtField.text length]!=0);
    
    return fieldsFilled;
}

#pragma mark - TableView Delegate & Data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return 3;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    switch (indexPath.row) {
        case 0:
        {
            cell =(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
            break;
        }
        case 1:
        {
            cell =(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"PasswordCell" forIndexPath:indexPath];
            _oldPasswordTxtField = (UITextField*)[cell viewWithTag:661];
            _newpasswordTxtField = (UITextField*)[cell viewWithTag:662];
            _emailTxtField = (UITextField*)[cell viewWithTag:660];
            _confirmPasswordTxtField = (UITextField*)[cell viewWithTag:664];
            UIButton *btn = (UIButton*)[cell viewWithTag:663];
            btn.hidden = !isForgotPassword;
            break;
        }
        case 2:
        {
            cell =(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"UpdateCell" forIndexPath:indexPath];
            break;
        }
        default:
            break;
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    switch (indexPath.row) {
        case 0:
            return 127;
            break;
        case 1:
            return 318;
            break;
        case 2:
            return 64;
            break;
        
            default:
            break;
    }
    
    return 64;
}


#pragma mark - KeyBoard Notifications
- (void)keyboardFrameChanged:(NSNotification *)aNotification
{
    NSDictionary *userInfo = aNotification.userInfo;
    _table.scrollEnabled = YES;
    
    //
    // Get keyboard size.
    
    NSValue *beginFrameValue = userInfo[UIKeyboardFrameBeginUserInfoKey];
    CGRect keyboardBeginFrame = [self.view convertRect:beginFrameValue.CGRectValue fromView:nil];
    
    NSValue *endFrameValue = userInfo[UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardEndFrame = [self.view convertRect:endFrameValue.CGRectValue fromView:nil];
    
    //
    // Get keyboard animation.
    
    NSNumber *durationValue = userInfo[UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration = durationValue.doubleValue;
    
    NSNumber *curveValue = userInfo[UIKeyboardAnimationCurveUserInfoKey];
    UIViewAnimationCurve animationCurve = curveValue.intValue;
    
    //
    // Create animation.
    
    CGRect viewFrame = self.view.frame;
    viewFrame.size.height = (keyboardBeginFrame.origin.y - viewFrame.origin.y);
    self.view.frame = viewFrame;
    
    void (^animations)() = ^() {
        CGRect viewFrame = self.view.frame;
        viewFrame.size.height = (keyboardEndFrame.origin.y - viewFrame.origin.y);
        self.view.frame = viewFrame;
    };
    
    //
    // Begin animation.
    [UIView animateWithDuration:animationDuration
                          delay:0.0
                        options:(animationCurve << 16)
                     animations:animations
                     completion:nil];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    _table.scrollEnabled = NO;
    
    return YES;
}



@end
