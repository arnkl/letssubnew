//
//  RegisterViewController.h
//  letSub
//
//  Created by Vishnu Varthan on 08/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>


@interface RegisterViewController : UIViewController

- (IBAction)registerNewUser:(id)sender;
- (IBAction)cancelRegistration:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *usernameTxtField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTxtField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTxtField;
@property (weak, nonatomic) IBOutlet UITextField *cityTxtField;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UITextField *emailTxtField;
@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;
@end
