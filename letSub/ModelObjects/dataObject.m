//
//  dataObject.m
//  letSub
//
//  Created by Vishnu Varthan .P on 06/04/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "dataObject.h"

@implementation dataObject


+(NSString *)amenityNameForNumber :(int)number
{
    return [self amenityArray][number];
    
}

+(NSString *)restrictionNameForNumber:(int)number
{
    
    return [self restrictionArray][number];
}


+(NSArray *)amenityArray
{
    return @[@"Air-Conditioning",@"Dishwasher",@"Elevator",@"Game-Room",@"Gym",@"Heat-Included",@"Hot Water Included",@"Large-Closet",@"Parking",@"Pets",@"Power-Backup",@"Security",@"Swimming-Pool",@"Washer Dryer",@"Wi-Fi"];
}

+(NSArray *)restrictionArray
{
    return @[@"No Drinking",@"No Loud Music",@"No Overnight Guests",@"No Pets",@"No Smoking"];
}


+(int)getIndexForAmenity :(NSString *)amenity
{
    
    int index = (int)[[self amenityArray] indexOfObject:amenity];
    return index;
}

+(int)getIndexForRestriction :(NSString *)restriction
{
    int index = (int)[[self restrictionArray] indexOfObject:restriction];
    return index;
}



@end
