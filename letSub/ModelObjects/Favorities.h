//
//  Favorities.h
//  letSub
//
//  Created by Vishnu Varthan .P on 21/04/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Favorities : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Favorities+CoreDataProperties.h"
