//
//  MessageDataModel.h
//  letSub
//
//  Created by Vishnu Varthan .P on 18/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSQMessages.h"
#import "Messages.h"


@interface MessageDataModel : NSObject
{
    NSManagedObjectContext *context;
}
@property (strong, nonatomic) NSDictionary *avatars;
@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageData;
@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageData;

@property (strong, nonatomic) NSMutableArray *messagesList;
@property (strong , nonatomic) NSMutableDictionary *messagesDictionary;
@property (strong, nonatomic) NSMutableArray *userConversations;
-(void) LoadmessagesforuserID :(NSString*)userID;

@end
