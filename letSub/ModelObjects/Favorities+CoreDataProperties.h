//
//  Favorities+CoreDataProperties.h
//  letSub
//
//  Created by Vishnu Varthan .P on 21/04/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Favorities.h"

NS_ASSUME_NONNULL_BEGIN

@interface Favorities (CoreDataProperties)

@property (nullable, nonatomic, retain) id favoritiesPersistentArray;

@end

NS_ASSUME_NONNULL_END
