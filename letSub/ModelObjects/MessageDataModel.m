//
//  MessageDataModel.m
//  letSub
//
//  Created by Vishnu Varthan .P on 18/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "MessageDataModel.h"
#import "AppDelegate.h"

@implementation MessageDataModel
{
    Messages* message;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        
        JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
        self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLetsSubColor]];
        self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
        
        self.messagesList = [[NSMutableArray alloc]init];
        NSMutableArray *array = [[AppDelegate instance] fetchPersistentDataforEntity:@"Messages"];
        if([array count]!=0)
        {
            message =(Messages*)array[0];
            self.messagesDictionary = [message messagesDict];
        }

    }
    
    return self;
}


-(void) LoadmessagesforuserID :(NSString*)userID
{
    
    self.userConversations = [[NSMutableArray alloc]init];
    self.userConversations = [self.messagesDictionary valueForKey:userID];
    
    
    for (int i=0; i<[self.userConversations count]; i++) {
        NSDictionary * dict = self.userConversations [i];

        JSQMessage *conversation =  [[JSQMessage alloc] initWithSenderId:[dict objectForKey:@"senderID"]
                                                       senderDisplayName:@""
                                                                    date:[dict objectForKey:@"date"]
                                                                    text:[dict objectForKey:@"message"]
                                                                  status:[dict objectForKey:@"status"]];
        
        [self.messagesList addObject:conversation];
    }
    
}



@end
