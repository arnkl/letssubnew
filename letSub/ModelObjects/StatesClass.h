//
//  StatesClass.h
//  letSub
//
//  Created by Vishnu Varthan on 11/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StatesClass : NSObject
-(NSDictionary *)statesDictionary;
-(NSString *)abbrevatedStateForShortForm :(NSString*)shortForm;

@property(nonatomic) NSArray *Alabama,*Alaska,*Arizona,*Arkansas,*California,*Colorado,*Connecticut,*Delaware,*Florida,*Georgia,*Hawaii,*Idaho,*Illinois,*Indiana,*Iowa,*Kansas,*Kentucky,*Louisiana,*Maine,*Maryland,*Massachusetts,*Michigan,*Minnesota,*Mississippi,*Missouri,*Montana,*Nebraska,*Nevada,*NewHampshire,*NewJersey,*NewMexico,*NewYork,*NorthCarolina,*NorthDakota,*Ohio,*Oklahoma,*Oregon,*Pennsylvania,*RhodeIsland,*SouthCarolina,*SouthDakota,*Tennessee,*Texas,*Utah,*Vermont,*Virginia,*Washington,*WestVirginia,*Wisconsin,*Wyoming;

@end
