//
//  dataObject.h
//  letSub
//
//  Created by Vishnu Varthan .P on 06/04/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <Foundation/Foundation.h>

@interface dataObject : NSObject


+(NSString *)amenityNameForNumber :(int)number;
+(NSString *)restrictionNameForNumber :(int)number;

+(int)getIndexForAmenity :(NSString *)amenity;
+(int)getIndexForRestriction :(NSString *)restriction;


+(NSArray *)amenityArray;
+(NSArray *)restrictionArray;

@end
