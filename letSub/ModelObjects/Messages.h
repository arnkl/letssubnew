//
//  Messages.h
//  letSub
//
//  Created by Vishnu Varthan .P on 24/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Messages : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Messages+CoreDataProperties.h"
