//
//  Messages+CoreDataProperties.m
//  letSub
//
//  Created by Vishnu Varthan .P on 24/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Messages+CoreDataProperties.h"

@implementation Messages (CoreDataProperties)

@dynamic messagesDict;

@end
