//
//  MessagesListViewController.h
//  letSub
//
//  Created by Vishnu Varthan .P on 18/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessagesListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UIView *noDataView;

- (IBAction)refreshMessageList:(id)sender;
-(void)checkForNewMessages;

@end
