//
//  FirstViewController.h
//  letSub
//
//  Created by Vishnu Varthan .P on 04/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubletListingViewController.h"
#import "StatesClass.h"
#import "TTRangeSlider.h"

@interface HomeViewController : UIViewController

@property (weak,nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSDictionary *statesDictionary,*moreFilterDictionary;
- (IBAction)resetFilters:(id)sender;

@end

