//
//  FeedsViewController.m
//  letSub
//
//  Created by Vishnu Varthan .P on 08/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "FeedsViewController.h"
#import "CustomFavoritiesButton.h"
#import "JSQMessage.h"

@interface FeedsViewController ()
{
    NSMutableArray *feedTypeArray,*feedTimeArray,*userNameArray,*userIDArray,*messageArray,*userImgArray,*feedsIdArray,*favouriteArray;
    NSMutableArray *idArray,*areaArray,*imgArray,*cityArray,*stateArray,*listingNameArray,*priceArray,*typeArray;
    UIActivityIndicatorView *spinner;
    int pageIndex;
}
@end

@implementation FeedsViewController

-(void) viewDidLoad {
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getNewFeeds) name:@"getNewFeed" object:nil];
    
    
    feedTimeArray = [[NSMutableArray alloc]init];
    feedsIdArray = [[NSMutableArray alloc]init];
    userNameArray = [[NSMutableArray alloc]init];
    userIDArray =[[NSMutableArray alloc]init];
    feedTypeArray =[[NSMutableArray alloc]init];
    messageArray =[[NSMutableArray alloc]init];
    userImgArray = [[NSMutableArray alloc]init];
    
    idArray  =[[NSMutableArray alloc]init];
    areaArray =[[NSMutableArray alloc]init];
    imgArray =[[NSMutableArray alloc]init];
    cityArray =[[NSMutableArray alloc]init];
    stateArray =[[NSMutableArray alloc]init];
    listingNameArray =[[NSMutableArray alloc]init];
    priceArray =[[NSMutableArray alloc]init];
    typeArray =[[NSMutableArray alloc]init];
    favouriteArray = [[NSMutableArray alloc]init];

    
    pageIndex = 1;
    
    [self performSelector:@selector(getFeeds) withObject:self afterDelay:1];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(getNewFeeds) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    [self.tableView setEstimatedRowHeight:200.0];
    [self.tableView setRowHeight:UITableViewAutomaticDimension];

    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [spinner hidesWhenStopped];
    spinner.frame = CGRectMake(0, 0, 320, 44);
    self.tableView.tableFooterView = spinner;
    [super viewDidLoad];
}


-(void)viewDidAppear:(BOOL)animated
{
    self.navigationController.navigationBar.topItem.title = @"My Feeds";
    [super viewDidAppear:YES];
}


#pragma mark -

- (IBAction)toggle:(id)sender {
    
    [[AppDelegate instance].slideMenuVC toggleMenu];
}



-(void)getNewFeeds
{
    
  //  //NSLog(@"get feeds with index 0");
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }

    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"userID",@"0",@"pageIndex",[NSNumber numberWithBool:YES],@"pullNew",[feedsIdArray firstObject],@"postID",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email", nil];
    NSString *urlAddress = [NSString stringWithFormat:@"posts/loadPosts.json"];
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
      NSLog(@"JSON: %@", responseObject);
        NSDictionary *response = [responseObject objectForKey:@"response"];
        NSArray *array = [response objectForKey:@"feedList"];
        [self processFeedsWithData:array sender:self forType:0];
        _tableView.hidden = NO;
        _noDataView.hidden = YES;
        _errView.hidden = YES;
        [self.refreshControl endRefreshing];
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
       NSLog(@"Error: %@", error);
        _noDataView.hidden = YES;
        _tableView.hidden = YES;
        _errView.hidden = NO;
        [self.refreshControl endRefreshing];
        
    }];
}

-(void)getMoreFeedsForPage :(int)page
{
    // //NSLog(@"get feeds with index %d",page);
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }

    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"userID",[NSString stringWithFormat:@"%d",page],@"pageIndex",[NSNumber numberWithBool:NO],@"pullNew",[feedsIdArray lastObject],@"postID",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email", nil];
    NSString *urlAddress = [NSString stringWithFormat:@"posts/loadPosts.json"];
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
      //  //NSLog(@"JSON: %@", responseObject);
        NSDictionary *response = [responseObject objectForKey:@"response"];
        NSArray *array = [response objectForKey:@"feedList"];
        [self processFeedsWithData:array sender:self forType:2];
        _tableView.hidden = NO;
        _noDataView.hidden = YES;
        _errView.hidden = YES;
       
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
       // //NSLog(@"Error: %@", error);
        _noDataView.hidden = YES;
        _tableView.hidden = YES;
        _errView.hidden = NO;
        
    }];
    
    [spinner stopAnimating];

}

-(void)getFeeds
{
    [AppDelegate showLoadingView:self];
    
   //  //NSLog(@"get feeds with index 1");
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }

    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"userID",@"1",@"pageIndex",[NSNumber numberWithBool:NO],@"pullNew",[feedsIdArray firstObject],@"postID",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email", nil];
    NSString *urlAddress = [NSString stringWithFormat:@"posts/loadPosts.json"];
    

    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
       NSLog(@"JSON: %@", responseObject);
        
        NSDictionary *response = [responseObject objectForKey:@"response"];
        NSArray *array = [response objectForKey:@"feedList"];
        [self processFeedsWithData:array sender:self forType:1];
        _tableView.hidden = NO;
        _noDataView.hidden = YES;
        _errView.hidden = YES;
        [AppDelegate hideLoadingIndicator];
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
       NSLog(@"Error: %@", error);
        _noDataView.hidden = YES;
        _tableView.hidden = YES;
        _errView.hidden = NO;
        [AppDelegate hideLoadingIndicator];
        
    }];
    
    
    
    
    
}


-(void)processFeedsWithData :(NSArray *)array sender:(id)sender forType:(int)type
{
    NSMutableDictionary *data1 =[[NSMutableDictionary alloc]init];
    
   
    
    if(type == 1)
    {
        [feedTypeArray removeAllObjects];
        [feedTimeArray removeAllObjects];
        [userNameArray removeAllObjects];
        [userIDArray removeAllObjects];
        [feedsIdArray removeAllObjects];
        [messageArray removeAllObjects];

        [idArray removeAllObjects];
        [areaArray removeAllObjects];
        [imgArray removeAllObjects];
        [cityArray removeAllObjects];
        [stateArray removeAllObjects];
        [listingNameArray removeAllObjects];
        [priceArray removeAllObjects];
        [typeArray removeAllObjects];
        [favouriteArray removeAllObjects];
    }

    
    for (int i=0; i<[array count]; i++) {
        
        data1 = [array objectAtIndex:i];
        
     
        NSString *dateString =[data1 objectForKey:@"time"];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_GB"]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss +zzzz"];
        NSDate * dateFromString = [dateFormatter dateFromString:dateString];
        
        if (type == 0) {
            
            [feedTimeArray insertObject:[[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:dateFromString]atIndex:0];
            [feedTypeArray insertObject:[data1 objectForKey:@"type"] atIndex:0];
            [userNameArray insertObject:[data1 objectForKey:@"name"] atIndex:0];
            [userIDArray insertObject:[data1 objectForKey:@"userID"] atIndex:0];
            [userImgArray insertObject:[data1 objectForKey:@"userImage"] atIndex:0];
            [feedsIdArray insertObject:[data1 objectForKey:@"postID"] atIndex:0];
            
            NSDictionary *dict = [[NSDictionary alloc]init];
            dict = [data1 objectForKey:@"feedDetails"];
            
            [messageArray insertObject:[dict objectForKey:@"message"] atIndex:0];
            [idArray insertObject:[dict objectForKey:@"listingID"] atIndex:0];
            [areaArray insertObject:[dict objectForKey:@"size"] atIndex:0];
            [imgArray insertObject:[dict objectForKey:@"image"] atIndex:0];
            [cityArray insertObject:[dict objectForKey:@"location"] atIndex:0];
            [stateArray insertObject:[dict objectForKey:@"state"] atIndex:0];
            [listingNameArray insertObject:[dict objectForKey:@"name"] atIndex:0];
            [priceArray insertObject:[dict objectForKey:@"price"] atIndex:0];
            [typeArray insertObject:[dict objectForKey:@"type"] atIndex:0];
            [favouriteArray insertObject:[dict objectForKey:@"isFavourite"]atIndex:0];

        }
        
        else
        {
            
        [feedTimeArray addObject:[[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:dateFromString]];
        [feedTypeArray addObject:[data1 objectForKey:@"type"]];
        [userNameArray addObject:[data1 objectForKey:@"name"]];
        [userIDArray addObject:[data1 objectForKey:@"userID"]];
        [userImgArray addObject:[data1 objectForKey:@"userImage"]];
        [feedsIdArray addObject:[data1 objectForKey:@"postID"]];
        
        NSDictionary *dict = [[NSDictionary alloc]init];
        dict = [data1 objectForKey:@"feedDetails"];
        
        [messageArray addObject:[dict objectForKey:@"message"]];
        [idArray addObject:[dict objectForKey:@"listingID"]];
        [areaArray addObject:[dict objectForKey:@"size"]];
        [imgArray addObject:[dict objectForKey:@"image"]];
        [cityArray addObject:[dict objectForKey:@"location"]];
        [stateArray addObject:[dict objectForKey:@"state"]];
        [listingNameArray addObject:[dict objectForKey:@"name"]];
        [priceArray addObject:[dict objectForKey:@"price"]];
        [typeArray addObject:[dict objectForKey:@"type"]];
        [favouriteArray addObject:[dict objectForKey:@"isFavourite"]];

        }
    }
    
    if([sender isKindOfClass:[UIRefreshControl class]])
        [self.refreshControl endRefreshing];
    else
        [AppDelegate hideLoadingIndicator];

    [_tableView reloadData];

}

- (IBAction)reloadContent:(id)sender {
    
    [self getFeeds];
}

- (IBAction)createFeeds:(id)sender {
    
    FeedPostViewController *postController = [self.storyboard instantiateViewControllerWithIdentifier:@"FeedPostViewController"];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:postController];
    [self presentViewController:navigationController animated:YES completion:nil];
}


-(void)removeFavourities :(id)sender
{
    
    CustomFavoritiesButton *btn = (CustomFavoritiesButton *)sender;
    [btn setBackgroundImage:[UIImage imageNamed:@"favouriteUnSelected" ]forState:UIControlStateNormal];
     [btn addTarget:self action:@selector(addToFavorities:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }

    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"userID",idArray[btn.index],@"listingID",@"0",@"isFavourite",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email", nil];
    NSString *urlAddress = [NSString stringWithFormat:@"sublets/toggleFavouriteSublet.json"];
    
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
      //  //NSLog(@"JSON: %@", responseObject);
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
      //  //NSLog(@"Error: %@", error);
        
        [btn setBackgroundImage:[UIImage imageNamed:@"favouriteSelected" ]forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(removeFavourities:) forControlEvents:UIControlEventTouchUpInside];
        
    }];

}

-(void)addToFavorities:(id)sender
{
    
    CustomFavoritiesButton *btn = (CustomFavoritiesButton *)sender;
    [btn setBackgroundImage:[UIImage imageNamed:@"favouriteSelected" ]forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(removeFavourities:) forControlEvents:UIControlEventTouchUpInside];
    
    
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }

    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"userID",idArray[btn.index],@"listingID",@"1",@"isFavourite",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email", nil];
    NSString *urlAddress = [NSString stringWithFormat:@"sublets/toggleFavouriteSublet.json"];
    
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
       // //NSLog(@"JSON: %@", responseObject);
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
       // //NSLog(@"Error: %@", error);
        [btn setBackgroundImage:[UIImage imageNamed:@"favouriteUnSelected" ]forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(addToFavorities:) forControlEvents:UIControlEventTouchUpInside];
        
    }];

}


-(void)deletePostforIndex :(id)sender
{
    [AppDelegate showLoadingView:self];
    
    CustomFavoritiesButton *btn = (CustomFavoritiesButton *)sender;
    int index = btn.index;
    
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }

    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:feedsIdArray[index],@"postID",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email", nil];
    NSString *urlAddress = [NSString stringWithFormat:@"posts/deletePost.json"];
    
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
      //  //NSLog(@"JSON: %@", responseObject);
        
        [feedTypeArray removeObjectAtIndex:index];
        [feedTimeArray removeObjectAtIndex:index];
        [userNameArray removeObjectAtIndex:index];
        [userIDArray removeObjectAtIndex:index];
        [feedsIdArray removeObjectAtIndex:index];
        [messageArray removeObjectAtIndex:index];
        
        [idArray removeObjectAtIndex:index];
        [areaArray removeObjectAtIndex:index];
        [imgArray removeObjectAtIndex:index];
        [cityArray removeObjectAtIndex:index];
        [stateArray removeObjectAtIndex:index];
        [listingNameArray removeObjectAtIndex:index];
        [favouriteArray removeObjectAtIndex:index];
        [priceArray removeObjectAtIndex:index];
        [typeArray removeObjectAtIndex:index];
        
        [_tableView beginUpdates];
        [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        [_tableView endUpdates];
        [AppDelegate hideLoadingIndicator];


        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
      //  //NSLog(@"Error: %@", error);
        [AppDelegate hideLoadingIndicator];
        
    }];

    }



-(void)postNewFeedsWithText : (NSNotification*)info
{
  
    NSString * text = [info.userInfo objectForKey:@"message"];
    [feedTypeArray insertObject:[NSNumber numberWithInt:3] atIndex:0];
    [feedTimeArray insertObject:[[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:[NSDate date]] atIndex:0];
    [userNameArray insertObject:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserName"] atIndex:0];
    [userIDArray insertObject:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"] atIndex:0];
    [userImgArray insertObject:@"NA" atIndex:0];
    [feedsIdArray insertObject:[info.userInfo objectForKey:@"id"] atIndex:0];
    [messageArray insertObject:text atIndex:0];
    
    [idArray insertObject:@"NA" atIndex:0];
    [areaArray insertObject:@"NA" atIndex:0];
    [imgArray insertObject:@"NA" atIndex:0];
    [cityArray insertObject:@"NA" atIndex:0];
    [stateArray insertObject:@"NA" atIndex:0];
    [listingNameArray insertObject:@"NA" atIndex:0];
    [priceArray insertObject:@"NA" atIndex:0];
    [typeArray insertObject:@"NA" atIndex:0];
    [favouriteArray insertObject:[NSNumber numberWithBool:NO] atIndex:0];

    [_tableView reloadData];
}

#pragma mark - tableview Datasource & Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return [feedTypeArray count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    UITableViewCell *cell;
    if([feedTypeArray[indexPath.row] integerValue] == 1)
    {
        cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"type1Cell" forIndexPath:indexPath];
        UILabel *lbl1 = (UILabel*)[cell viewWithTag:820];
        lbl1.text = userNameArray [indexPath.row];
        UILabel *lbl2 = (UILabel*)[cell viewWithTag:821];
        lbl2.text = @"";
        lbl2.textAlignment = NSTextAlignmentLeft;
        UILabel *lbl3 = (UILabel*)[cell viewWithTag:822];
        lbl3.text = [NSString stringWithFormat:@"Looking for %@BED in %@ for $%@",typeArray[indexPath.row],cityArray[indexPath.row],priceArray[indexPath.row]];
       
        
        UIImageView *imgview = (UIImageView*)[cell viewWithTag:823];
        [imgview setImageWithURL:userImgArray [indexPath.row] placeholderImage:[UIImage imageNamed:@"defaultProfilePic"]];
        [[imgview layer] setCornerRadius:imgview.frame.size.height/2];
        [[imgview layer] setBorderWidth:0.1];
        [[imgview layer] setBorderColor:(__bridge CGColorRef _Nullable)([UIColor clearColor])];
        
        UILabel *lbl5 = (UILabel*)[cell viewWithTag:2000];
        lbl5.text = [NSString stringWithFormat:@"%@, %@",cityArray[indexPath.row],stateArray[indexPath.row]];
 
    }
    else if([feedTypeArray[indexPath.row] integerValue] == 2)
    {
        cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"type2Cell" forIndexPath:indexPath];
        UIImageView *imgview = (UIImageView*)[cell viewWithTag:824];
        [imgview setImageWithURL:userImgArray [indexPath.row] placeholderImage:[UIImage imageNamed:@"defaultProfilePic"]];
        [[imgview layer] setCornerRadius:imgview.frame.size.height/2];
        [[imgview layer] setBorderWidth:0.1];
        [[imgview layer] setBorderColor:(__bridge CGColorRef _Nullable)([UIColor clearColor])];
        UILabel *lbl1 = (UILabel*)[cell viewWithTag:825];
        lbl1.text = userNameArray [indexPath.row];
        UILabel *lbl2 = (UILabel*)[cell viewWithTag:826];
//        lbl2.attributedText = feedTimeArray[indexPath.row];
        lbl2.text = @"";
        lbl2.textAlignment = NSTextAlignmentLeft;

        UILabel *lbl3 = (UILabel*)[cell viewWithTag:827];
        lbl3.text = [NSString stringWithFormat:@"Looking for %@BED in %@ for $%@",typeArray[indexPath.row],cityArray[indexPath.row],priceArray[indexPath.row]];
        UIImageView *img = (UIImageView*)[cell viewWithTag:828];
        [img setImageWithURL:imgArray[indexPath.row] placeholderImage:[UIImage imageNamed:@"defaultImage"]];
        UILabel *lbl4 = (UILabel*)[cell viewWithTag:829];
        lbl4.text = listingNameArray [indexPath.row];
        UILabel *lbl5 = (UILabel*)[cell viewWithTag:830];
        lbl5.text = [NSString stringWithFormat:@"%@, %@",cityArray[indexPath.row],stateArray[indexPath.row]];
        UILabel *lbl7 = (UILabel*)[cell viewWithTag:2004];
        lbl7.text = [NSString stringWithFormat:@"%@, %@",cityArray[indexPath.row],stateArray[indexPath.row]];
        UILabel *lbl6 = (UILabel*)[cell viewWithTag:832];
        lbl6.text = [areaArray[indexPath.row] stringByAppendingString:@"sqft"];
       
         UIButton * btnAmount =(UIButton *)[cell viewWithTag:2003];
        [[btnAmount layer] setCornerRadius:20.0];
        [[btnAmount layer] setBorderColor:(__bridge CGColorRef _Nullable)([UIColor clearColor])];
        [[btnAmount layer] setBorderWidth:0.1];
        [btnAmount setTitle:[NSString stringWithFormat:@"$%@",priceArray[indexPath.row]] forState:UIControlStateNormal];
        
        CAGradientLayer *gradient = [CAGradientLayer layer];
        gradient.frame = btnAmount.bounds;
        [gradient setBorderWidth:0.1];
        [gradient setCornerRadius:20.0];
        [gradient setStartPoint:CGPointMake(0.0, 0.5)];
        [gradient setEndPoint:CGPointMake(1.0, 0.5)];
        [gradient setBorderColor:(__bridge CGColorRef _Nullable)([UIColor clearColor])];
        gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:0.267 green:0.176 blue:0.996 alpha:1.000] CGColor], (id)[[UIColor colorWithRed:0.467 green:0.000 blue:0.522 alpha:1.000] CGColor], nil];
        [btnAmount.layer insertSublayer:gradient atIndex:0];
        
//        CustomFavoritiesButton *deleteBtn = (CustomFavoritiesButton *)[cell viewWithTag:810];
//        deleteBtn.index = (int)indexPath.row;
//        [deleteBtn addTarget:self action:@selector(deletePostforIndex:) forControlEvents:UIControlEventTouchUpInside];
//        deleteBtn.hidden = ![userIDArray[indexPath.row] isEqualToString:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"]];
    
        CustomFavoritiesButton *btn = (CustomFavoritiesButton *)[cell viewWithTag:831];
        if ([favouriteArray[indexPath.row] boolValue] == 1)
        {
            [btn setBackgroundImage:[UIImage imageNamed:@"favouriteSelected" ]forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(removeFavourities:) forControlEvents:UIControlEventTouchUpInside];
        }
        else{
            [btn setBackgroundImage:[UIImage imageNamed:@"favouriteUnSelected" ]forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(addToFavorities:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        btn.index = (int)indexPath.row;
        
    }
    
   else  if([feedTypeArray[indexPath.row] integerValue] == 3)
    {
        cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"type3Cell" forIndexPath:indexPath];
        UILabel *lbl1 = (UILabel*)[cell viewWithTag:833];
        lbl1.text = userNameArray [indexPath.row];
        UILabel *lbl2 = (UILabel*)[cell viewWithTag:834];
//        lbl2.attributedText = feedTimeArray[indexPath.row];
        lbl2.text = @"";
        lbl2.textAlignment = NSTextAlignmentLeft;

        UILabel *lbl3 = (UILabel*)[cell viewWithTag:835];
        lbl3.text = messageArray[indexPath.row];
        //[NSString stringWithFormat:@"Looking for %@BED in %@ for $%@",typeArray[indexPath.row],cityArray[indexPath.row],priceArray[indexPath.row]];
        
        UIImageView *img = (UIImageView*)[cell viewWithTag:836];
        NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserPicture"];
        if(imageData)
            img.image = [UIImage imageWithData:imageData];
        else
            img.image = [UIImage imageNamed:@"defaultProfilePic"];
        [[img layer] setCornerRadius:img.frame.size.height/2];
        [[img layer] setBorderWidth:0.1];
        [[img layer] setBorderColor:(__bridge CGColorRef _Nullable)([UIColor clearColor])];
        
//        CustomFavoritiesButton *btn = (CustomFavoritiesButton *)[cell viewWithTag:837];
//        btn.index = (int)indexPath.row;
//        [btn addTarget:self action:@selector(deletePostforIndex:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([feedTypeArray[indexPath.row] integerValue] == 1)
    {
        
        ListerInfoViewController *lister = [self.storyboard instantiateViewControllerWithIdentifier:@"ListerInfoViewController"];
        lister.listerName = userNameArray[indexPath.row];
        lister.listerId = userIDArray [indexPath.row];
        lister.hideMessageCell = NO;
        [self.navigationController pushViewController:lister animated:YES];

    }
    
   if([feedTypeArray[indexPath.row] integerValue] == 2)
    {
        SubletDetailViewController *subletDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SubletDetailViewController"];
        subletDetailVC.listingID = idArray[indexPath.row];
        subletDetailVC.listingName = listingNameArray [indexPath.row];
        subletDetailVC.hideListerInfo = NO;
        UITableViewCell *selectedCell=[tableView cellForRowAtIndexPath:indexPath];
        UIImageView *imgview = (UIImageView*)[selectedCell viewWithTag:824];
        subletDetailVC.userImage = imgview.image;
        
        [self.navigationController pushViewController:subletDetailVC animated:YES];
        
    }
    
  
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([feedTypeArray[indexPath.row] integerValue] == 2)
        return 357;
        else
        return 160;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 70;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewCell *cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    UIImageView *view = (UIImageView*)[cell viewWithTag:1000];
    NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserPicture"];
    if(imageData)
        view.image = [UIImage imageWithData:imageData];
    else
        view.image = [UIImage imageNamed:@"defaultProfilePic"];

    
       return cell.contentView;
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    [spinner startAnimating];

    
    NSInteger currentOffset = scrollView.contentOffset.y;
    NSInteger maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    if (maximumOffset - currentOffset <= -40) {
        
        if(pageIndex < 50)
        {
            pageIndex ++;
            [self getMoreFeedsForPage:pageIndex];
            
        }
        else
        {
            [spinner stopAnimating];
            UILabel * lbl = [[UILabel alloc]init];
            lbl.text = @"No more data";
            lbl.frame = CGRectMake(0, 0, 320, 44);
            self.tableView.tableFooterView = lbl;
            
        }
       
    }
}

@end
