//
//  ProfileViewController.h
//  letSub
//
//  Created by Vishnu Varthan .P on 08/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTRangeSlider.h"

@interface ProfileViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,UITextViewDelegate,TTRangeSliderDelegate>

{
    UIButton *cameraButton,*moveInButton;
    UITextField *eMailTxt,*userNameTxt,*locationTxt,*preferredLocationTxt,*jobTitleTxt,*companyTxt,*ageTxt,*educationTxt;
    UIImageView *bgImageView,*dpImageView;
    BOOL showHiddenCell,allowEditing,isImageEdited,isCoverEdited;
    NSString *priceMax,*priceMin,*pickerDate,*eMailString,*userNameString,*locationString,*stateString,*preferredLocationString,*jobString,*companyString,*aboutString,*educationString,*ageString;
    UIDatePicker *datePicker;
    UITextView*AboutTxt;
    TTRangeSlider *slider;
    UIImagePickerController *dpPicker;
    UIScrollView *intrestScrollView;
    UIImage *coverImage,*profileImage;
}

@property (weak, nonatomic) IBOutlet UIButton *selectDate;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
