//
//  ProfileViewController.m
//  letSub
//
//  Created by Vishnu Varthan .P on 08/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "ProfileViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "JCTagListView.h"


@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UITabBar *tabBar = self.tabBarController.tabBar;
    
    for (UITabBarItem  *tab in tabBar.items) {
        tab.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        tab.image = [tab.image imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal];
        tab.title = nil;
    }

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
   
    priceMin =[[NSUserDefaults standardUserDefaults]objectForKey:@"loggedUserMinBudget"]?[[NSUserDefaults standardUserDefaults]objectForKey:@"loggedUserMinBudget"]: @"1000";
    priceMax =[[NSUserDefaults standardUserDefaults]objectForKey:@"loggedUserMaxBudget"]?[[NSUserDefaults standardUserDefaults]objectForKey:@"loggedUserMaxBudget"]: @"3500";
    pickerDate = [[NSUserDefaults standardUserDefaults]objectForKey:@"loggedUserMoveInDate"]?[[NSUserDefaults standardUserDefaults]objectForKey:@"loggedUserMoveInDate"]:@"click to select date..";
    
    profileImage = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:@"loggedUserPicture"]];
    coverImage = [UIImage imageWithData:[[NSUserDefaults standardUserDefaults]objectForKey:@"loggedUserCover"]];
    userNameString = [[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserName"];
    eMailString = [[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"];
    locationString = [[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserLocation"];
    preferredLocationString = [[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserPreferredLocation"];
    jobString = [[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserJob"];
    companyString = [[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserCompany"];
    aboutString=[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserdescription"];
    ageString= [[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAge"];
    educationString= [[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEducation"];
    // Do any additional setup after loading the view.
}


-(void)viewDidAppear:(BOOL)animated
{
    
    dpImageView.contentMode = UIViewContentModeScaleAspectFill;
    
//     //create effect
//        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
//        // add effect to an effect view
//        UIVisualEffectView *effectView = [[UIVisualEffectView alloc]initWithEffect:blur];
//        effectView.frame = bgImageView.frame;
//        // add the effect view to the image view
//        [bgImageView addSubview:effectView];
         showHiddenCell = NO;
    
    
    self.navigationController.navigationBar.topItem.title = @"Profile";
    [super viewDidAppear:YES];
}

#pragma mark -


-(UIScrollView *)createIntrestContentWithScrollView :(UIScrollView*)scrollView
{
    
    NSArray *viewsToRemove = [scrollView subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    
    if([[AppDelegate instance].userIntrestArray count]== 0)
    {
        UILabel *lable =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 125, 30)];
        lable.text = @"(No Interest)";
        lable.font = [UIFont systemFontOfSize:12];
        lable.textColor= [UIColor colorWithRed:128/255.0 green:128/255.0 blue:128/255.0 alpha:1.0];
        lable.textAlignment = NSTextAlignmentCenter;
        [scrollView addSubview:lable];
        lable.center = scrollView.center;
        scrollView.contentSize = CGSizeMake(10, scrollView.frame.size.height);
        return scrollView;
        
    }

    
    int x = 10;
    for (int i = 0; i < [[AppDelegate instance].userIntrestArray count]; i++) {
        
        NSDictionary *dict = [AppDelegate instance].userIntrestArray [i];
        
        UIView *View = [[UIView alloc] initWithFrame:CGRectMake(x, 0, 150, 180)];
        //View.backgroundColor = [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:0.70];
        View.layer.cornerRadius = 5.0f;
        
        UIImageView *intrestImg =[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 140, 100)];
        intrestImg.contentMode = UIViewContentModeScaleAspectFill;
        intrestImg.layer.masksToBounds = YES;
        [intrestImg.layer setBorderColor: [[UIColor lightGrayColor] CGColor]];
        [intrestImg.layer setBorderWidth: 0.1];
        [intrestImg setImageWithURL:[dict objectForKey:@"picture"] placeholderImage:[UIImage imageNamed:@"defaultImage"]];
        [View addSubview:intrestImg];
        
        UILabel *intrestLable =[[UILabel alloc]initWithFrame:CGRectMake(2, 110, 146, 50)];
        intrestLable.text = [dict objectForKey:@"name"];
        intrestLable.font = [UIFont systemFontOfSize:12];
        intrestLable.textColor= [UIColor colorWithRed:128/255.0 green:128/255.0 blue:128/255.0 alpha:1.0];
        intrestLable.numberOfLines = 2;
        intrestLable.textAlignment = NSTextAlignmentCenter;
        [View addSubview:intrestLable];
        
        [scrollView addSubview:View];
        x += View.frame.size.width + 10;
    }
    
    scrollView.contentSize = CGSizeMake(x, scrollView.frame.size.height);
    return scrollView;
}

- (IBAction)selectDate:(id)sender {
    
    showHiddenCell= YES;
    [_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

- (IBAction)donePickingDate:(id)sender {
    
    showHiddenCell = NO;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    pickerDate = [dateFormat stringFromDate:datePicker.date];
    [_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];

}


- (IBAction)editProfile:(id)sender {
    
    UIBarButtonItem *tmpButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(doneEditing:)];
    self.navigationItem.rightBarButtonItem = tmpButtonItem;
    allowEditing =YES;
    [_tableView reloadData];
  
}

- (IBAction)doneEditing:(id)sender {
    
    UIBarButtonItem *tmpButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:@selector(editProfile:)];
    self.navigationItem.rightBarButtonItem = tmpButtonItem;
    allowEditing = NO;
    [_tableView reloadData];
    [AppDelegate showLoadingView:self];
    
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
       authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }
    
    if([pickerDate isEqualToString:@"click to select date.."])
    {
        pickerDate = @"";
    }
    
    if([AboutTxt.text isEqualToString:@"write a description about yourself ...."])
    {
        AboutTxt.text = @"";
    }
    
    NSData* data = [ NSJSONSerialization dataWithJSONObject:[AppDelegate instance].userIntrestArray options:NSJSONWritingPrettyPrinted error:nil ];
    NSString *intrestData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

    
    NSString *urlAddress = [NSString stringWithFormat:@"users/updateUserProfile.json"];
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:pickerDate,@"moveInDate",priceMax,@"maxBudget",priceMin,@"minBudget",userNameString,@"username",eMailString,@"email",locationString,@"location",preferredLocationString,@"preferredLocation",jobString,@"jobTitle",companyString,@"company",aboutString,@"description",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,ageString,@"age_range",educationString,@"education",intrestData,@"interest",nil];
    
    
             LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
            [client POST:urlAddress parameters:dict  constructingBodyWithBlock:^(id<AFMultipartFormData> formData){
    
            if(profileImage)
               [formData appendPartWithFileData:UIImagePNGRepresentation(profileImage) name:@"picture" fileName:@"image.png" mimeType:@"image/png"];
                
                if(coverImage)
                    [formData appendPartWithFileData:UIImagePNGRepresentation(coverImage) name:@"coverPicture" fileName:@"image.png" mimeType:@"image/png"];
   

            } progress:nil  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    
                //NSLog(@"JSON: %@", responseObject);
              
                [[NSUserDefaults standardUserDefaults] setObject:pickerDate forKey:@"loggedUserMoveInDate"];
                [[NSUserDefaults standardUserDefaults] setObject:priceMax forKey:@"loggedUserMaxBudget"];
                [[NSUserDefaults standardUserDefaults] setObject:priceMin forKey:@"loggedUserMinBudget"];
                [[NSUserDefaults standardUserDefaults] setObject:userNameTxt.text forKey:@"loggedUserName"];
                [[NSUserDefaults standardUserDefaults]setObject:UIImagePNGRepresentation(dpImageView.image) forKey:@"loggedUserPicture"];
                [[NSUserDefaults standardUserDefaults]setObject:UIImagePNGRepresentation(bgImageView.image) forKey:@"loggedUserCover"];


                [[NSUserDefaults standardUserDefaults] setObject:eMailString forKey:@"loggedUserEmail"];
                [[NSUserDefaults standardUserDefaults] setObject:locationString forKey:@"loggedUserLocation"];
                [[NSUserDefaults standardUserDefaults] setObject:preferredLocationString forKey:@"loggedUserPreferredLocation"];
                [[NSUserDefaults standardUserDefaults] setObject:jobString forKey:@"loggedUserJob"];
                [[NSUserDefaults standardUserDefaults] setObject:companyString forKey:@"loggedUserCompany"];
                [[NSUserDefaults standardUserDefaults] setObject:aboutString forKey:@"loggedUserdescription"];
                [[NSUserDefaults standardUserDefaults] setObject:ageString forKey:@"loggedUserAge"];
                [[NSUserDefaults standardUserDefaults] setObject:educationString forKey:@"loggedUserEducation"];
                
                
                
                if(isCoverEdited)
                    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"customUserCover"];
                if(isImageEdited)
                    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"customUserImage"];

                
                
                [[NSUserDefaults standardUserDefaults]synchronize];

                [AppDelegate hideLoadingIndicator];
    
            } failure:^(NSURLSessionTask *operation, NSError *error) {
                //NSLog(@"Error: %@", error);
                [AppDelegate hideLoadingIndicator];
                NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
                alertViewController.title = @"Oops..";
                alertViewController.message = @"Operation cannot be completed, Try again !!";
                alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleSlideFromBottom;
                alertViewController.buttonColor = [UIColor colorWithRed:86.0f/255.0f green:1.0f/255.0f blue:71.0f/255.0f alpha:0.90f];
                
                [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK"
                                                                        style:UIAlertActionStyleDefault
                                                                      handler:^(NYAlertAction *action) {
                                                                          [self dismissViewControllerAnimated:YES completion:nil];}
                                                                          ]];
                         
                [self presentViewController:alertViewController animated:YES completion:nil];

    
            }];

    
}


-(IBAction)selectImageSource:(id)sender
{
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    [imagePickerController setDelegate:self];
    imagePickerController.allowsEditing = YES;
    
    if(sender == cameraButton)
        dpPicker = imagePickerController;
    
    
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"";
    alertViewController.message = @"Select an image source";
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleSlideFromBottom;
    alertViewController.buttonColor = [UIColor colorWithRed:86.0f/255.0f green:1.0f/255.0f blue:71.0f/255.0f alpha:0.90f];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Camera"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                              [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
                                                              [imagePickerController setCameraDevice:UIImagePickerControllerCameraDeviceFront];
                                                              self.tabBarController.tabBar.hidden = YES;
                                                              [self presentViewController:imagePickerController animated:YES completion:nil];
                                                              
                                                          }]];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Gallery"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                              [imagePickerController setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
                                                              [self presentViewController:imagePickerController animated:YES completion:nil];
                                                              self.tabBarController.tabBar.hidden = YES;
                                                          }]];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Cancel"
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                          }]];
    
    
    [self presentViewController:alertViewController animated:YES completion:nil];
}

- (IBAction)editImage:(id)sender {
    
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"";
    alertViewController.message = @"Select an image you want to edit";
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleSlideFromBottom;
    alertViewController.buttonColor = [UIColor colorWithRed:86.0f/255.0f green:1.0f/255.0f blue:71.0f/255.0f alpha:0.90f];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Profile Image"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                              [self selectImageSource:cameraButton];
                                                              
                                                          }]];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Cover Image"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                              [self selectImageSource:nil];
                                                              
                                                          }]];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Cancel"
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                          }]];
    
    
    [self presentViewController:alertViewController animated:YES completion:nil];
    
    
}

- (IBAction)toggle:(id)sender {
    
    [[AppDelegate instance].slideMenuVC toggleMenu];
}

-(void)deactivateUserAccount
{
    [AppDelegate showLoadingView:self];
    NSString *urlAddress = [NSString stringWithFormat:@"users/deactivateAccount.json"];
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }

    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"userID",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email", nil];
    [client POST:urlAddress parameters:dict  progress:nil  success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        //NSLog(@"JSON: %@", responseObject);
        responseObject = [responseObject objectForKey:@"response"];
        NSString *status =[responseObject objectForKey:@"status"];
        
        if([status boolValue] == 1)
        {

        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"TermsAccepted"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [[AppDelegate instance]deleteAllEntities:@"Messages"];
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager logOut];
        [AppDelegate hideLoadingIndicator];
        [[AppDelegate instance]openLoginPage];
        }
        else{
            
            [AppDelegate hideLoadingIndicator];
        }
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        //NSLog(@"Error: %@", error);
        [AppDelegate hideLoadingIndicator];
        
    }];

}


#pragma mark - Image Picker Delegates

- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingMediaWithInfo:(nonnull NSDictionary<NSString *,id> *)editingInfo
{
    
    if(picker == dpPicker)
    {
    profileImage = [editingInfo valueForKey:UIImagePickerControllerEditedImage];;
    isImageEdited = YES;
    }
    else{
        
       coverImage = [editingInfo valueForKey:UIImagePickerControllerEditedImage];
        isCoverEdited = YES;
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    self.tabBarController.tabBar.hidden = NO;
    [_tableView reloadData];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    self.tabBarController.tabBar.hidden = NO;
    
}

- (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

#pragma mark - KeyBoard Notifications

- (void)keyboardWillShow:(NSNotification*)aNotification {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.tableView.contentInset.top, 0.0, kbSize.height, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillHide:(NSNotification*)aNotification {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.35];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.tableView.contentInset.top, 0.0, 0.0, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    [UIView commitAnimations];
    
}

#pragma mark - table view delegates & data sources

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return showHiddenCell?14:13;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    switch (indexPath.row) {
            
        case 0:{
            
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"picCell" forIndexPath:indexPath];
            bgImageView = (UIImageView*)[cell viewWithTag:90];
            dpImageView = (UIImageView*)[cell viewWithTag:91];
            if(profileImage)
                dpImageView.image  = profileImage;
            else
                dpImageView.image  = [UIImage imageNamed:@"defaultProfilePic"];
            
            [[dpImageView layer] setCornerRadius:70.0];
            [[dpImageView layer] setBorderColor:[[UIColor colorWithRed:0.384 green:0.000 blue:0.686 alpha:1.000] CGColor]];
            [[dpImageView layer] setBorderWidth:1.0];
            if(coverImage)
               bgImageView.image = coverImage;
            else
               bgImageView.image = [self imageFromColor:[UIColor lightGrayColor]];

            
            cameraButton = (UIButton*)[cell viewWithTag:92];
            cameraButton.hidden = !allowEditing;
            userNameTxt = (UITextField *)[cell viewWithTag:94];
            userNameTxt.delegate = self;
            userNameTxt.text = userNameString;
            [userNameTxt setUserInteractionEnabled:allowEditing];
            locationTxt = (UITextField *)[cell viewWithTag:95];
            locationTxt.delegate = self;
            locationTxt.text =  locationString;
            [locationTxt setUserInteractionEnabled:allowEditing];
            
            break;
        }
        case 1:{
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"aboutCell" forIndexPath:indexPath];
            AboutTxt = (UITextView *)[cell viewWithTag:96];
            AboutTxt.delegate = self;
            AboutTxt.text =  aboutString?aboutString:@"write a description about yourself ....";
            [AboutTxt setUserInteractionEnabled:allowEditing];
            
            
            break;
        }
            
        case 2:{
            
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"educationCell" forIndexPath:indexPath];
            educationTxt = (UITextField*)[cell viewWithTag:892];
            educationTxt.delegate = self;
            educationTxt.text =  educationString;
            [educationTxt setUserInteractionEnabled:allowEditing];
            ageTxt = (UITextField*)[cell viewWithTag:893];
            ageTxt.delegate = self;
            ageTxt.text =  ageString;
            [ageTxt setUserInteractionEnabled:allowEditing];
            
            
            
          
            break;
        }
        case 3:{
          
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"companyCell" forIndexPath:indexPath];
            companyTxt = (UITextField*)[cell viewWithTag:103];
            companyTxt.delegate = self;
            companyTxt.text =  companyString;
            [companyTxt setUserInteractionEnabled:allowEditing];
            
            break;
        }
            
        case 4:{
            
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"intrestCell" forIndexPath:indexPath];
            UIScrollView *scrollView = (UIScrollView*)[cell viewWithTag:890];
            scrollView =[self createIntrestContentWithScrollView:scrollView];
            
            break;
        }
            
        case 5:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"pricePickerCell" forIndexPath:indexPath];
            slider = (TTRangeSlider *)[cell viewWithTag:104];
            slider.delegate = self;
            slider.tintColorBetweenHandles = [UIColor colorWithRed:0.447 green:0.000 blue:0.565 alpha:1.000];
            slider.handleImage = [UIImage imageNamed:@"slider-default7-handle.png"];
            [slider setSelectedMaximum:[priceMax integerValue]];
            [slider setSelectedMinimum:[priceMin integerValue]];
            [slider setUserInteractionEnabled:allowEditing];
            
            
            
            break;
        }
        case 6:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"workCell" forIndexPath:indexPath];
            jobTitleTxt = (UITextField*)[cell viewWithTag:102];
            jobTitleTxt.delegate = self;
            jobTitleTxt.text =  jobString;
            [jobTitleTxt setUserInteractionEnabled:allowEditing];
            
            
            break;
        }
        case 7:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
            cell.separatorInset = UIEdgeInsetsMake(0.f, 0.f, 0.f, cell.bounds.size.width);
            
            break;
        }
        case 8:{
            
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"emailCell" forIndexPath:indexPath];
            eMailTxt = (UITextField *)[cell viewWithTag:93];
            eMailTxt.text = eMailString;
            eMailTxt.delegate = self;
            if([eMailTxt.text isEqualToString:@""])
                eMailTxt.userInteractionEnabled = allowEditing;
            
            break;
            
        }
        case 9:
        {
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"preferredLocationCell" forIndexPath:indexPath];
            preferredLocationTxt = (UITextField *)[cell viewWithTag:101];
            preferredLocationTxt.delegate=self;
            preferredLocationTxt.text =  preferredLocationString;
            [preferredLocationTxt setUserInteractionEnabled:allowEditing];
           
            break;
        }
            
            
        case 10:{
            
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"moveInCell" forIndexPath:indexPath];
            moveInButton = (UIButton*)[cell viewWithTag:99];
            [moveInButton setTitle:pickerDate forState:UIControlStateNormal];
            if(showHiddenCell)
                cell.separatorInset = UIEdgeInsetsMake(0.f, 0.f, 0.f, cell.bounds.size.width);
            
            [moveInButton setUserInteractionEnabled:allowEditing];
            
            break;
        }
            
        case 11:{
            if(showHiddenCell)
            {
                cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"datePickerCell" forIndexPath:indexPath];
                datePicker =(UIDatePicker*)[cell viewWithTag:100];
                
            }
            else
                cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"logoutCell" forIndexPath:indexPath];
            
            break;
        }
        case 12:
        {
            if(showHiddenCell)
                cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"logoutCell" forIndexPath:indexPath];
            else
                cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"deActivateCell" forIndexPath:indexPath];

            break;
        }
            
        case 13:{
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"deActivateCell" forIndexPath:indexPath];
            
            
            break;
        }
            
                   
        default:
            break;
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   
    
     if(indexPath.row ==14 && !showHiddenCell)
    {
        
        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
        alertViewController.title = @"Warning !";
        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleSlideFromBottom;
        alertViewController.buttonColor = [UIColor colorWithRed:86.0f/255.0f green:1.0f/255.0f blue:71.0f/255.0f alpha:0.90f];
        
      
            alertViewController.message = @"Are you sure you want to log out?";
            [alertViewController addAction:[NYAlertAction actionWithTitle:@"LogOut"
                                                                    style:UIAlertActionStyleDestructive
                                                                  handler:^(NYAlertAction *action) {
                                                                      [self dismissViewControllerAnimated:YES completion:nil];
                                                                      NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
                                                                      [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
                                                                      [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"TermsAccepted"];
                                                                      [[NSUserDefaults standardUserDefaults]synchronize];
                                                                      [[AppDelegate instance]deleteAllEntities:@"Messages"];
                                                                      FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
                                                                      [loginManager logOut];
                                                                      
                                                                      [[AppDelegate instance]openLoginPage];
                                                                      
                                                                  }]];
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"Cancel"
                                                                style:UIAlertActionStyleCancel
                                                              handler:^(NYAlertAction *action) {
                                                                  [self dismissViewControllerAnimated:YES completion:nil];
                                                              }]];
        
        
        [self presentViewController:alertViewController animated:YES completion:nil];
            

    }
    
    else if(indexPath.row ==15)
    {
        
        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
        alertViewController.title = @"Warning !!";
        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleSlideFromBottom;
        alertViewController.buttonColor = [UIColor colorWithRed:86.0f/255.0f green:1.0f/255.0f blue:71.0f/255.0f alpha:0.90f];

        if(showHiddenCell)
        {
            alertViewController.message = @"Are you sure you want to log out?";
            [alertViewController addAction:[NYAlertAction actionWithTitle:@"LogOut"
                                                                    style:UIAlertActionStyleDestructive
                                                                  handler:^(NYAlertAction *action) {
                                                                      [self dismissViewControllerAnimated:YES completion:nil];
                                                                      NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
                                                                      [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
                                                                      [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"TermsAccepted"];
                                                                      [[NSUserDefaults standardUserDefaults]synchronize];
                                                                      [[AppDelegate instance]deleteAllEntities:@"Messages"];
                                                                      FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
                                                                      [loginManager logOut];
                                                                      
                                                                      [[AppDelegate instance]openLoginPage];
                                                                      
                                                                  }]];


        }
        else
        {
            alertViewController.message = @"Are you sure you want to Delete your Account?";
            [alertViewController addAction:[NYAlertAction actionWithTitle:@"Yes, Delete"
                                                                    style:UIAlertActionStyleDestructive
                                                                  handler:^(NYAlertAction *action) {
                                                                      [self dismissViewControllerAnimated:YES completion:nil];
                                                                      [self deactivateUserAccount];
                                                                  }]];
            
        }
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"Cancel"
                                                                style:UIAlertActionStyleCancel
                                                              handler:^(NYAlertAction *action) {
                                                                  [self dismissViewControllerAnimated:YES completion:nil];
                                                              }]];
        
        
        [self presentViewController:alertViewController animated:YES completion:nil];
        

    }
    else if (indexPath.row == 16)
    {
        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
        alertViewController.title = @"Warning !!";
        alertViewController.message = @"Are you sure you want to Delete your Account?";
        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleSlideFromBottom;
        alertViewController.buttonColor = [UIColor colorWithRed:86.0f/255.0f green:1.0f/255.0f blue:71.0f/255.0f alpha:0.90f];
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"Yes, Delete"
                                                                style:UIAlertActionStyleDestructive
                                                              handler:^(NYAlertAction *action) {
                                                                  [self dismissViewControllerAnimated:YES completion:nil];
                                                                  [self deactivateUserAccount];
                                                              }]];

        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"Cancel"
                                                                style:UIAlertActionStyleCancel
                                                              handler:^(NYAlertAction *action) {
                                                                  [self dismissViewControllerAnimated:YES completion:nil];
                                                              }]];
        
        [self presentViewController:alertViewController animated:YES completion:nil];
        
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
       
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    switch (indexPath.row) {
        case 0:
            return 330;
            break;
            case 1:
            return 85;
            break;
        case 2:
            return 85;
            break;
        case 3:
            return 85;
            break;
        case 4:
            return 172;
            break;
        case 11:
            return 108;
            break;
        case 12:
            return 208;
        case 14:
            return showHiddenCell?193:46;
            break;
        default:
            break;
    }
       return 120;
}

#pragma mark - text field delegates

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if(textField == userNameTxt)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"customUserName"];
    }
    if(textField == locationTxt)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"customUserLocation"];
    }
    if(textField == jobTitleTxt)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"customUserJobTitle"];
    }
    if(textField == companyTxt)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"customUserCompany"];
    }

    if(textField == ageTxt)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"customUserAge"];
    }
    if(textField == educationTxt)
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"customUserEducation"];
    }

    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{

    if(textField == userNameTxt)
    {
        userNameString= userNameTxt.text ;
    }
   else if(textField == eMailTxt)
    {
       eMailString= eMailTxt.text ;

    }
       else if(textField == locationTxt)
    {
        locationString=locationTxt.text;
    }
       else if(textField == preferredLocationTxt)
    {
        preferredLocationString =preferredLocationTxt.text;
    }
    else if(textField == jobTitleTxt)
    {
        jobString =jobTitleTxt.text;
    }
    else if(textField == companyTxt)
    {
        companyString =companyTxt.text;
    }
    else if(textField == educationTxt)
    {
        educationString =educationTxt.text;
    }
    else if(textField == ageTxt)
    {
        ageString =ageTxt.text;
    }
    

}

#pragma mark - text view delegates

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    
    if ([textView.text isEqualToString:@"write a description about yourself ...."]) {
        textView.text = @"";
    }
    
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    if(textView.text.length == 0){
        textView.text = @"write a description about yourself ....";
        [textView resignFirstResponder];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        if(textView.text.length == 0){
            textView.text = @"write a description of yourself ....";
            [textView resignFirstResponder];
        }
        return NO;
    }
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    
    aboutString = textView.text;
    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"customUserBio"];
    [[NSUserDefaults standardUserDefaults]synchronize];

}

#pragma mark - TTRangeSliderViewDelegate
-(void)rangeSlider:(TTRangeSlider *)sender didChangeSelectedMinimumValue:(float)selectedMinimum andMaximumValue:(float)selectedMaximum{
    
    
        priceMax = [NSString stringWithFormat:@"%.01f",selectedMaximum];
        priceMin = [NSString stringWithFormat:@"%.01f",selectedMinimum];
    
}

@end
