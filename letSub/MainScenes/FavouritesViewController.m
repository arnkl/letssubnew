//
//  SecondViewController.m
//  letSub
//
//  Created by Vishnu Varthan .P on 04/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "FavouritesViewController.h"
#import "SubletDetailViewController.h"


@interface FavouritesViewController ()
{
    NSMutableArray *idArray,*imageArray,*locationArray,*stateArray,*nameArray,*areaArray,*typeArray;
}
@end

@implementation FavouritesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    idArray= [[NSMutableArray alloc]init];
    imageArray = [[NSMutableArray alloc]init];
    locationArray = [[NSMutableArray alloc]init];
    stateArray =[[NSMutableArray alloc]init];
    nameArray = [[NSMutableArray alloc]init];
    areaArray =[[NSMutableArray alloc]init];
    typeArray =[[NSMutableArray alloc]init];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(loadFavoritesList:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
    [self performSelector:@selector(loadFavoritesList:) withObject:nil afterDelay:01];

    // Do any additional setup after loading the view, typically from a nib.
   
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    self.navigationController.navigationBar.topItem.title = @"Favorites";
}

#pragma mark -

-(void)loadFavoritesList:(id)sender{
    
    
    [idArray removeAllObjects];
    [imageArray removeAllObjects];
    [locationArray removeAllObjects];
    [stateArray removeAllObjects];
    [nameArray removeAllObjects];
    [areaArray removeAllObjects];
    [typeArray removeAllObjects];
    
    if(![sender isKindOfClass:[UIRefreshControl class]])
       [AppDelegate showLoadingView:self];
    
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }

    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"id",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email", nil];
    NSString *urlAddress = [NSString stringWithFormat:@"sublets/getFavouriteSublets.json"];
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    
    [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
     //   //NSLog(@"JSON: %@", responseObject);
        responseObject = [[responseObject objectForKey:@"response"]objectForKey:@"favouriteSubs"];
        NSString *status = [responseObject objectForKey:@"status"];
        NSArray *array = [responseObject objectForKey:@"list"];
        
        
        if([status boolValue] == 1)
        {
            
            NSMutableDictionary *data1 =[[NSMutableDictionary alloc]init];
            for (int i=0; i<[array count]; i++) {
                
                data1 = [array objectAtIndex:i];
                
                [idArray addObject:[data1 valueForKey:@"id"]];
                [imageArray addObject:[data1 valueForKey:@"image"]];
                [locationArray addObject:[data1 valueForKey:@"city"]];
                [stateArray addObject:[data1 valueForKey:@"state"]];
                [nameArray addObject:[data1 valueForKey:@"name"]];
                [areaArray addObject:[data1 valueForKey:@"size"]];
                [typeArray addObject:[data1 valueForKey:@"type"]];
                
                if([sender isKindOfClass:[UIRefreshControl class]])
                    [self.refreshControl endRefreshing];
                else
                    [AppDelegate hideLoadingIndicator];
            }
            if([sender isKindOfClass:[UIRefreshControl class]])
               [self.refreshControl endRefreshing];
            else
                [AppDelegate hideLoadingIndicator];

            [_tableView reloadData];
            
            _noDataView.hidden = YES;
            _tableView.hidden = NO;
            _errView.hidden = YES;
            
        }
        else{
            
            _noDataView.hidden = NO;
            _tableView.hidden = YES;
            _errView.hidden = YES;
            if([sender isKindOfClass:[UIRefreshControl class]])
                [self.refreshControl endRefreshing];
            else
                [AppDelegate hideLoadingIndicator];
        }

        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
      //  //NSLog(@"Error: %@", error);
        [AppDelegate hideLoadingIndicator];
        [self.refreshControl endRefreshing];
        _noDataView.hidden = YES;
        _tableView.hidden = YES;
        _errView.hidden = NO;
        
    }];
     
}


- (IBAction)toggle:(id)sender {
    
    [[AppDelegate instance].slideMenuVC toggleMenu];
}

-(void)removeDataAtIndex: (int)index
{
    
    
    [AppDelegate showLoadingView:self];
    
    NSString* authenticationKey;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
    {
        authenticationKey = @"oauth_id";
    }
    else{
        
        authenticationKey =@"password";
    }

    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"userID",idArray[index],@"listingID",@"0",@"isFavourite",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email", nil];
    NSString *urlAddress = [NSString stringWithFormat:@"sublets/toggleFavouriteSublet.json"];
    LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
    [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [AppDelegate hideLoadingIndicator];
       // //NSLog(@"JSON: %@", responseObject);
        
        [idArray removeObjectAtIndex:index];
        [imageArray removeObjectAtIndex:index];
        [locationArray removeObjectAtIndex:index];
        [stateArray removeObjectAtIndex:index];
        [nameArray removeObjectAtIndex:index];
        [areaArray removeObjectAtIndex:index];
        [typeArray removeObjectAtIndex:index];
        
        
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView reloadData];
        
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
      //  //NSLog(@"Error: %@", error);
        [AppDelegate hideLoadingIndicator];
        
      
      //  //NSLog(@"Operation failed...");
        
    }];
    
}
- (IBAction)reloadContent:(id)sender {
    
    [self loadFavoritesList:self];
}

#pragma mark - tableview Datasource & Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [idArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"favoriteCell" forIndexPath:indexPath];
    UIImageView *imgView = (UIImageView *)[cell viewWithTag:120];
    [imgView setImageWithURL:imageArray[indexPath.row] placeholderImage:[UIImage imageNamed:@"defaultImage"]];
    UILabel *heading = (UILabel *)[cell viewWithTag:121];
    heading.text = nameArray[indexPath.section];
    UILabel *location = (UILabel *)[cell viewWithTag:122];
    location.text =  [NSString stringWithFormat:@"%@, %@",locationArray[indexPath.section],stateArray[indexPath.section]];
    UILabel *detail = (UILabel *)[cell viewWithTag:123];
    detail.text =[NSString stringWithFormat:@"%@ / %@",typeArray[indexPath.row],areaArray[indexPath.section]];
     UIView *viewContainer = (UIView *)[cell viewWithTag:5000];
    [[viewContainer layer] setCornerRadius:10.0];
    [[viewContainer layer] setBorderWidth:4.0];
    [[viewContainer layer] setBorderColor:(__bridge CGColorRef _Nullable)([UIColor colorWithRed:0.937 green:0.929 blue:0.996 alpha:1.000])];
    [viewContainer setBackgroundColor:[UIColor whiteColor] ];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SubletDetailViewController* subLetDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SubletDetailViewController"];
    subLetDetailVC.listingID = idArray [indexPath.section];
    subLetDetailVC.listingName = nameArray [indexPath.section];
    subLetDetailVC.hideListerInfo = NO;
    [self.navigationController pushViewController:subLetDetailVC animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 150;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
        alertViewController.title = @"Warning !";
        alertViewController.message = @"Are you sure you want to remove listing from your favorites list?";
        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
        alertViewController.buttonColor = [UIColor colorWithRed:86.0f/255.0f green:1.0f/255.0f blue:71.0f/255.0f alpha:0.90f];
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"Remove"
                                                                style:UIAlertActionStyleDestructive
                                                              handler:^(NYAlertAction *action) {
                                                                  [self dismissViewControllerAnimated:YES completion:nil];
                                                                  [self removeDataAtIndex:(int)indexPath.row];
                                                                  
                                                                  
                                                              }]];
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"Cancel"
                                                                style:UIAlertActionStyleCancel
                                                              handler:^(NYAlertAction *action) {
                                                                  [self dismissViewControllerAnimated:YES completion:nil];
                                                              }]];
        
        
        [self presentViewController:alertViewController animated:YES completion:nil];
        
        //add code here for when you hit delete
    }
}


@end
