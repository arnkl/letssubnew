//
//  FeedsViewController.h
//  letSub
//
//  Created by Vishnu Varthan .P on 08/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubletDetailViewController.h"
#import "FeedPostViewController.h"

@interface FeedsViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (weak, nonatomic) IBOutlet UIView *errView;
@property (weak, nonatomic) IBOutlet UIView *noDataView;
- (IBAction)reloadContent:(id)sender;
- (IBAction)createFeeds:(id)sender;
-(void)processFeedsWithData :(NSArray *)array sender:(id)sender forType:(int)type;
@end
