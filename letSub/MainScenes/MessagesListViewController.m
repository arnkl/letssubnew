//
//  MessagesListViewController.m
//  letSub
//
//  Created by Vishnu Varthan .P on 18/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "MessagesListViewController.h"
#import "ChatViewController.h"
#import "JSQMessages.h"

@interface MessagesListViewController ()
{
    NSMutableDictionary *coreDataDict;
    NSMutableArray *newMessageArray;
    
}
@end

Messages *message;

@implementation MessagesListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    coreDataDict =[[NSMutableDictionary alloc]init];
    NSMutableArray *array = [[AppDelegate instance] fetchPersistentDataforEntity:@"Messages"];
   if([array count]!=0)
   {
       message =(Messages*)array[0];
       coreDataDict = [message messagesDict];
       if([coreDataDict count]!=0)
       {
       _table.hidden = NO;
       _noDataView.hidden = YES;
       [_table reloadData];
       }
       else
       {
           _table.hidden = YES;
           _noDataView.hidden = NO;
       }
   }
    else
    {
        _table.hidden = YES;
        _noDataView.hidden = NO;
    }
    
    [self performSelector:@selector(refreshMessageList:) withObject:self afterDelay:01];
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [_table reloadData];
    self.tabBarItem.badgeValue = nil;
}


#pragma mark -

-(void)checkForNewMessages
{
    
    @try{
        
        
        NSString* authenticationKey;
        if([[[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"]isEqualToString:@"Facebook"])
        {
            authenticationKey = @"oauth_id";
        }
        else{
            
            authenticationKey =@"password";
        }

        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"userID",[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserAuthentictionID"],authenticationKey,[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUserEmail"],@"email", nil];
        NSString *urlAddress = [NSString stringWithFormat:@"messages/receiveMessage.json"];
        LetsSubWebServiceClient *client = [LetsSubWebServiceClient sharedClient];
        [client POST:urlAddress parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSDictionary *response = [responseObject objectForKey:@"response"];
            NSString *status = [response objectForKey:@"status"];
            
            //NSLog(@"response...%@",responseObject);
            
            if([status boolValue]== 1)
            {
                [JSQSystemSoundPlayer jsq_playNewMessageAlert];
                
                NSArray *array = [response objectForKey:@"new_messages"];
                NSMutableDictionary *data1 =[[NSMutableDictionary alloc]init];
                newMessageArray = [[NSMutableArray alloc]init];
                NSMutableDictionary *  messagesDictionary =[[NSMutableDictionary alloc]init];
                
                NSMutableArray *resultArray=[[AppDelegate instance] fetchPersistentDataforEntity:@"Messages"];
                
                if([resultArray count] !=0)
                {
                    message =(Messages*)resultArray[0];
                    coreDataDict = [message messagesDict];
                    
                }
                else
                {
                    message=(Messages*)[NSEntityDescription insertNewObjectForEntityForName:@"Messages" inManagedObjectContext:[[AppDelegate instance] managedObjectContext]];
                }
                
                for (int i=0; i<[array count]; i++) {
                    
                    data1 = [array objectAtIndex:i];
                    messagesDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:[data1 objectForKey:@"senderName"],@"senderName",[data1 objectForKey:@"senderID"],@"senderID",[NSDate date],@"date",[data1 objectForKey:@"message"],@"message",[data1 objectForKey:@"senderImage"],@"senderImage",@"delivered",@"status", nil];
                    
                   [newMessageArray addObject:[data1 objectForKey:@"senderID"]];
                    NSMutableArray *array = [[NSMutableArray alloc]init];
                    
                    if([coreDataDict objectForKey:[data1 objectForKey:@"senderID"]])
                        array = [[coreDataDict objectForKey:[data1 objectForKey:@"senderID"]]mutableCopy];
                    
                    [array addObject:messagesDictionary];
                    [coreDataDict removeObjectForKey:[data1 objectForKey:@"senderID"]];
                    [coreDataDict setObject:array forKey:[data1 objectForKey:@"senderID"]];
                    
                }
                
                [message setMessagesDict:coreDataDict];
                [[AppDelegate instance] saveContext];
                
                _noDataView.hidden = YES;
                _table.hidden = NO;
                [_table reloadData];
                [self stopRefreshSpinner];
                
            }
            else{
                
                 [self stopRefreshSpinner];
            }
            
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            //NSLog(@"Error: %@", error);
            [self stopRefreshSpinner];
        }];
        
    }
    @catch (NSException *exception) {
        
        //NSLog(@"%@",exception);
    }
    
}

- (IBAction)toggle:(id)sender {
    
    [[AppDelegate instance].slideMenuVC toggleMenu];
}


-(void)stopRefreshSpinner
{
    
    UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshMessageList:)];
    self.navigationItem.rightBarButtonItem = refreshButton;
    
}

- (IBAction)refreshMessageList:(id)sender {
    
   UIActivityIndicatorView* activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [activityIndicator startAnimating];

    UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    [self navigationItem].rightBarButtonItem = barButton;
    [self checkForNewMessages];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[coreDataDict allKeys]count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
  
    UIImageView *image = (UIImageView*)[cell viewWithTag:660];
   
    NSMutableArray *array = [coreDataDict allValues][indexPath.row];
    [image setImageWithURL:[[array lastObject] objectForKey:@"senderImage"] placeholderImage:[UIImage imageNamed:@"defaultProfilePic"]];
   
    UILabel *timeLbl = (UILabel *)[cell viewWithTag:662];
    timeLbl.attributedText =[[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:[[array lastObject] objectForKey:@"date"]];
    
    UILabel *nameLbl = (UILabel *)[cell viewWithTag:661];
    nameLbl.text =[[coreDataDict allValues][indexPath.row][0] objectForKey:@"senderName"];
  
    UILabel *messageDescpLbl = (UILabel *)[cell viewWithTag:664];
    messageDescpLbl.text =[[array lastObject] objectForKey:@"message"];

    UILabel *unReadLbl = (UILabel *)[cell viewWithTag:663];
    
    //NSLog(@"content...%@",[coreDataDict allValues][indexPath.row]);  
    if([newMessageArray containsObject:[[coreDataDict allValues][indexPath.row][0] objectForKey:@"senderID"]])
        unReadLbl.hidden = NO;
    else
        unReadLbl.hidden =YES;
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChatViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
    vc.messageID = [coreDataDict allKeys][indexPath.row];
    vc.senderName = [[coreDataDict allValues][indexPath.row][0]objectForKey:@"senderName"];
    NSMutableArray *array = [coreDataDict allValues][indexPath.row];
    vc.senderImgURL = [[array lastObject] objectForKey:@"senderImage"];
    UITableViewCell *cell = [self.table cellForRowAtIndexPath:indexPath];
    UIImageView *imgView = (UIImageView *)[cell viewWithTag:660];
    vc.senderImage = imgView.image;
    
    if([newMessageArray containsObject:[[coreDataDict allValues][indexPath.row][0] objectForKey:@"senderID"]])  // no more a new message
        [newMessageArray removeObject:[[coreDataDict allValues][indexPath.row][0] objectForKey:@"senderID"]];
    
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.navigationController pushViewController:vc animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSMutableArray *array = [[AppDelegate instance] fetchPersistentDataforEntity:@"Messages"];
        if([array count]!=0)
        {
            message =(Messages*)array[0];
            [coreDataDict removeObjectForKey:[coreDataDict allKeys][indexPath.row]];
            [message setMessagesDict:coreDataDict];
            [[AppDelegate instance] saveContext];
            
            if([coreDataDict count]==0)
            {
                    _table.hidden = YES;
                    _noDataView.hidden = NO;

            }
        }

        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [tableView reloadData];
        
        //add code here for when you hit delete
    }
}



@end
