//
//  FirstViewController.m
//  letSub
//
//  Created by Vishnu Varthan .P on 04/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import "HomeViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreLocation/CoreLocation.h>
#import "AboutViewController.h"
#import "MoreFiltersViewController.h"
@implementation CALayer (Additions)
- (void)setBorderColorFromUIColor:(UIColor *)color
{
    self.borderColor = color.CGColor;
}

@end

@interface HomeViewController ()<UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate,TTRangeSliderDelegate>
@property (nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation HomeViewController
{
    BOOL showHiddenCell,showStatePicker;
    UIPickerView *statePicker,*LocationPicker;
    NSString *selectedState,*selectedLocation,*priceMax,*priceMin,*selectedType,*selectedRooms;
    int moreFiltersCount;
    CLGeocoder *geocoder;
    UIButton *btnType1,*btnType2, *btnType3;
    UIButton *btnLocation1,*btnLocation2, *btnLocation3;
    UIButton *btnRoom1,*btnRoom2, *btnRoom3,*btnRoom4,*btnRoom5, *btnRoom6;
    UIButton *btnFindNow;
}

@synthesize tableView = _tableView;
@synthesize locationManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadViewDatas];
    
   UITabBar *tabBar = self.tabBarController.tabBar;
   for (UITabBarItem  *tab in tabBar.items) {
       tab.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
       tab.image = [tab.image imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal];
       tab.title = nil;
   }
   
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;

    [[NSNotificationCenter defaultCenter]removeObserver:@"AddMoreFilters"];
    // Do any additional setup after loading the view, typically from a nib.
}


-(void)viewDidAppear:(BOOL)animated {
    self.navigationController.navigationBar.topItem.title = @"Home";
    [super viewDidAppear:YES];
    showStatePicker=showHiddenCell = NO;
}

#pragma mark -


-(void)updateMoreFilters :(NSNotification*)info
{
    _moreFilterDictionary = info.userInfo;
    moreFiltersCount = (int)[[info.userInfo objectForKey:@"count"]integerValue];
    [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:showHiddenCell?9:8 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}


-(void)loadViewDatas
{
    
    selectedLocation = selectedState = @"Anywhere";
    [btnLocation1 setTitle:@"Select Location" forState:UIControlStateNormal];
    [btnLocation2 setTitle:@"Select State" forState:UIControlStateNormal];
    priceMax = @"1500";
    priceMin = @"500";
    
    self.statesDictionary = [[StatesClass new]statesDictionary];
}

- (IBAction)closePicker:(id)sender {
    
    showHiddenCell = showStatePicker = NO;
    [_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    
    
}

- (IBAction)submitSearch:(id)sender {
    
    
    NSMutableArray *amenities  =[[NSMutableArray alloc]init];
    NSMutableArray *restrictions = [[NSMutableArray alloc]init];
    
    NSIndexPath * index = [self.moreFilterDictionary objectForKey:@"indexPaths"][0];
    if(index.section == 0)
    {
        amenities = nil;
        restrictions = nil;
        
    }
    else
    {
        NSArray *array = [_moreFilterDictionary objectForKey:@"indexPaths"];
        for (int i =0; [array count]>i; i++) {
            
            NSIndexPath *index = array[i];
            if(index.section == 1)
            {
                [amenities addObject:[dataObject amenityNameForNumber:(int)index.row]];
            }
            else if(index.section == 2)
            {
                [restrictions addObject:[dataObject restrictionNameForNumber:(int)index.row]];
            }
            
        }
        
    }
   
    NSString * state = [selectedState isEqualToString:@"Anywhere"]?@"":selectedState;
    NSString * city = [selectedLocation isEqualToString:@"Anywhere"]?@"":selectedLocation;
    NSString *minStay = [[_moreFilterDictionary objectForKey:@"minimumStay"]isEqualToString:@"Any"]||![_moreFilterDictionary objectForKey:@"minimumStay"]?@"":[_moreFilterDictionary objectForKey:@"minimumStay"];
    
    
    id moveInDate;
    
    if([[_moreFilterDictionary objectForKey:@"moveInDate"]isEqualToString:@"Any"]||![_moreFilterDictionary objectForKey:@"moveInDate"])
    {
        moveInDate = @"";
        
    }
    else{
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MM/dd/yyyy"];
        moveInDate = [dateFormat dateFromString:[_moreFilterDictionary objectForKey:@"moveInDate"]];

    }
   
    NSData *amenityData = nil;
    NSData *restrictionData = nil;
    
    if(amenities)
    {
        
    amenityData = [NSJSONSerialization dataWithJSONObject:amenities
                                                          options:kNilOptions error:nil];
    }
    if(restrictions)
    {
        
    restrictionData = [NSJSONSerialization dataWithJSONObject:restrictions
                                                              options:kNilOptions error:nil];
    }
    
    
    NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:state,@"state",city,@"city",priceMax,@"maxPrice",priceMin,@"minPrice",selectedType?selectedType:@"",@"type",selectedRooms?selectedRooms:@"",@"room",[[NSUserDefaults standardUserDefaults]valueForKey:@"loggedUserId"],@"userId",minStay,@"minimumStay",moveInDate,@"moveInDate",amenityData?amenityData:@"",@"amenities",restrictionData?restrictionData:@"",@"restrictions", nil];
    
    SubletListingViewController * subletListVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SubletListingViewController"];
    subletListVC.searchParameters = dict;
    [self.navigationController pushViewController:subletListVC animated:YES];
    
}

-(IBAction)selectedAddFilter:(id)sender {
    MoreFiltersViewController *moreFilterVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MoreFiltersViewController"];
    moreFilterVC.moreFilterDatas = self.moreFilterDictionary;
    [self.navigationController pushViewController:moreFilterVC animated:YES];
    [[NSNotificationCenter defaultCenter ]addObserver:self selector:@selector(updateMoreFilters:) name:@"AddMoreFilters" object:nil];
}

- (IBAction)selectedType:(id)sender {
    
//    UISegmentedControl *segmentControl = (UISegmentedControl*)sender;
//    selectedType = [NSString stringWithFormat:@"%ld",(long)segmentControl.selectedSegmentIndex];
//    int tag = [sender tag];
    switch ([sender tag]) {
        case 500:
            [[btnType1 layer] setCornerRadius:12.0];
            [[btnType1 layer] setBorderWidth:1.0];
            [[btnType1 layer] setBorderColor:[[UIColor colorWithRed:0.384 green:0.000 blue:0.686 alpha:1.000] CGColor]];
            [[btnType2 layer] setBorderWidth:0.0];
            [[btnType3 layer] setBorderWidth:0.0];
            break;
        case 501:
            [[btnType2 layer] setCornerRadius:12.0];
            [[btnType2 layer] setBorderWidth:1.0];
            [[btnType2 layer] setBorderColor:[[UIColor colorWithRed:0.384 green:0.000 blue:0.686 alpha:1.000] CGColor]];
            [[btnType1 layer] setBorderWidth:0.0];
            [[btnType3 layer] setBorderWidth:0.0];
            break;
        case 502:
            [[btnType3 layer] setCornerRadius:12.0];
            [[btnType3 layer] setBorderWidth:1.0];
            [[btnType3 layer] setBorderColor:[[UIColor colorWithRed:0.384 green:0.000 blue:0.686 alpha:1.000] CGColor]];
            [[btnType2 layer] setBorderWidth:0.0];
            [[btnType1 layer] setBorderWidth:0.0];
            break;
        default:
            break;
    }
    
  
}

-(IBAction)selectedLocation:(id)sender {
    switch ([sender tag]) {
        case 700:
            [[btnLocation1 layer] setCornerRadius:12.0];
            [[btnLocation1 layer] setBorderWidth:1.0];
            [[btnLocation1 layer] setBorderColor:[[UIColor colorWithRed:0.384 green:0.000 blue:0.686 alpha:1.000] CGColor]];
            [[btnLocation2 layer] setBorderWidth:0.0];
            [[btnLocation3 layer] setBorderWidth:0.0];
            showHiddenCell = showStatePicker = YES;
            [_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case 701:
            [[btnLocation2 layer] setCornerRadius:12.0];
            [[btnLocation2 layer] setBorderWidth:1.0];
            [[btnLocation2 layer] setBorderColor:[[UIColor colorWithRed:0.384 green:0.000 blue:0.686 alpha:1.000] CGColor]];
            [[btnLocation1 layer] setBorderWidth:0.0];
            [[btnLocation3 layer] setBorderWidth:0.0];
            if(![selectedState isEqualToString:@"Anywhere"]){
                showStatePicker = NO;
                showHiddenCell= YES;
                [_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
            }
            break;
        case 702:
            [[btnLocation3 layer] setCornerRadius:12.0];
            [[btnLocation3 layer] setBorderWidth:1.0];
            [[btnLocation3 layer] setBorderColor:[[UIColor colorWithRed:0.384 green:0.000 blue:0.686 alpha:1.000] CGColor]];
            [[btnLocation2 layer] setBorderWidth:0.0];
            [[btnLocation1 layer] setBorderWidth:0.0];
            if(showHiddenCell)
                [self closePicker:self];
            [self getUsersCurrentLocaion];
            break;
        default:
            break;
    }
}

- (IBAction)selectedRoom:(id)sender {
    
//    UISegmentedControl *segmentControl = (UISegmentedControl*)sender;
//    selectedRooms = [NSString stringWithFormat:@"%ld",(long)segmentControl.selectedSegmentIndex];

    switch ([sender tag]) {
        case 600:
            [[btnRoom1 layer] setCornerRadius:12.0];
            [[btnRoom1 layer] setBorderWidth:1.0];
            [[btnRoom1 layer] setBorderColor:[[UIColor colorWithRed:0.384 green:0.000 blue:0.686 alpha:1.000] CGColor]];
            [[btnRoom2 layer] setBorderWidth:0.0];
            [[btnRoom3 layer] setBorderWidth:0.0];
            [[btnRoom4 layer] setBorderWidth:0.0];
            [[btnRoom5 layer] setBorderWidth:0.0];
            [[btnRoom6 layer] setBorderWidth:0.0];
            selectedRooms = @"0";
            break;
        case 601:
            [[btnRoom2 layer] setCornerRadius:12.0];
            [[btnRoom2 layer] setBorderWidth:1.0];
            [[btnRoom2 layer] setBorderColor:[[UIColor colorWithRed:0.384 green:0.000 blue:0.686 alpha:1.000] CGColor]];
            [[btnRoom1 layer] setBorderWidth:0.0];
            [[btnRoom3 layer] setBorderWidth:0.0];
            [[btnRoom4 layer] setBorderWidth:0.0];
            [[btnRoom5 layer] setBorderWidth:0.0];
            [[btnRoom6 layer] setBorderWidth:0.0];
            selectedRooms = @"1";
            break;
        case 602:
            [[btnRoom3 layer] setCornerRadius:12.0];
            [[btnRoom3 layer] setBorderWidth:1.0];
            [[btnRoom3 layer] setBorderColor:[[UIColor colorWithRed:0.384 green:0.000 blue:0.686 alpha:1.000] CGColor]];
            [[btnRoom2 layer] setBorderWidth:0.0];
            [[btnRoom1 layer] setBorderWidth:0.0];
            [[btnRoom4 layer] setBorderWidth:0.0];
            [[btnRoom5 layer] setBorderWidth:0.0];
            [[btnRoom6 layer] setBorderWidth:0.0];
            selectedRooms = @"2";
            break;
        case 603:
            [[btnRoom4 layer] setCornerRadius:12.0];
            [[btnRoom4 layer] setBorderWidth:1.0];
            [[btnRoom4 layer] setBorderColor:[[UIColor colorWithRed:0.384 green:0.000 blue:0.686 alpha:1.000] CGColor]];
            [[btnRoom2 layer] setBorderWidth:0.0];
            [[btnRoom3 layer] setBorderWidth:0.0];
            [[btnRoom1 layer] setBorderWidth:0.0];
            [[btnRoom5 layer] setBorderWidth:0.0];
            [[btnRoom6 layer] setBorderWidth:0.0];
            selectedRooms = @"3";
            break;
        case 604:
            [[btnRoom5 layer] setCornerRadius:12.0];
            [[btnRoom5 layer] setBorderWidth:1.0];
            [[btnRoom5 layer] setBorderColor:[[UIColor colorWithRed:0.384 green:0.000 blue:0.686 alpha:1.000] CGColor]];
            [[btnRoom2 layer] setBorderWidth:0.0];
            [[btnRoom3 layer] setBorderWidth:0.0];
            [[btnRoom4 layer] setBorderWidth:0.0];
            [[btnRoom1 layer] setBorderWidth:0.0];
            [[btnRoom6 layer] setBorderWidth:0.0];
            selectedRooms = @"4";
            break;
        case 605:
            [[btnRoom6 layer] setCornerRadius:12.0];
            [[btnRoom6 layer] setBorderWidth:1.0];
            [[btnRoom6 layer] setBorderColor:[[UIColor colorWithRed:0.384 green:0.000 blue:0.686 alpha:1.000] CGColor]];
            [[btnRoom2 layer] setBorderWidth:0.0];
            [[btnRoom3 layer] setBorderWidth:0.0];
            [[btnRoom4 layer] setBorderWidth:0.0];
            [[btnRoom5 layer] setBorderWidth:0.0];
            [[btnRoom1 layer] setBorderWidth:0.0];
            selectedRooms = @"5";
            break;
        default:
            break;
    }
}

-(void)getUsersCurrentLocaion
{
    [AppDelegate showLoadingView:self];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager requestWhenInUseAuthorization];
    [locationManager startUpdatingLocation];
}

- (IBAction)toggle:(id)sender {
    
    [[AppDelegate instance].slideMenuVC toggleMenu];
}

- (IBAction)resetFilters:(id)sender {
    
    selectedLocation = selectedState = @"Anywhere";
    [btnLocation1 setTitle:@"Select Location" forState:UIControlStateNormal];
    [btnLocation2 setTitle:@"Select State" forState:UIControlStateNormal];
    priceMax = @"1500";
    priceMin = @"500";
    moreFiltersCount = 0;
    selectedType = @"";
    selectedRooms =@"";
    [[btnType3 layer] setBorderWidth:0.0];
    [[btnType2 layer] setBorderWidth:0.0];
    [[btnType1 layer] setBorderWidth:0.0];
    [[btnRoom6 layer] setBorderWidth:0.0];
    [[btnRoom2 layer] setBorderWidth:0.0];
    [[btnRoom3 layer] setBorderWidth:0.0];
    [[btnRoom4 layer] setBorderWidth:0.0];
    [[btnRoom5 layer] setBorderWidth:0.0];
    [[btnRoom1 layer] setBorderWidth:0.0];
    [[btnLocation3 layer] setBorderWidth:0.0];
    [[btnLocation2 layer] setBorderWidth:0.0];
    [[btnLocation1 layer] setBorderWidth:0.0];
//    [bhkSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];
//    [typeSegment setSelectedSegmentIndex:UISegmentedControlNoSegment];
    _moreFilterDictionary = nil;
    
    [self.tableView reloadData];
    
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if([CLLocationManager locationServicesEnabled]){
        
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
            alertViewController.title = @"App Permission Denied";
            alertViewController.message = @"To re-enable, please go to Settings and turn on Location Service for this app.";
            alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
            alertViewController.buttonColor = [UIColor colorWithRed:86.0f/255.0f green:1.0f/255.0f blue:71.0f/255.0f alpha:0.90f];
            
            [alertViewController addAction:[NYAlertAction actionWithTitle:@"Ok"
                                                                    style:UIAlertActionStyleCancel
                                                                  handler:^(NYAlertAction *action) {
                                                                      [self dismissViewControllerAnimated:YES completion:nil];
                                                                  }]];
            
            [self presentViewController:alertViewController animated:YES completion:nil];
        }
    }
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
   // //NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
//    if (currentLocation != nil) {
//        //NSLog(@"%@", [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude]);
//        //NSLog(@"%@", [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude]);
//    }
    
    // Stop Location Manager
    [locationManager stopUpdatingLocation];
    
    // Reverse Geocoding
   // //NSLog(@"Resolving the Address");
    geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error){  for (CLPlacemark * placemark in placemarks) {
       // NSString * addressName = [placemark name];
        NSString * city = [placemark subAdministrativeArea];
       // NSString *postalCode = [placemark postalCode];// locality means "city"
        NSString * administrativeArea = [placemark administrativeArea]; // which is "state" in the U.S.A.
        
      //  //NSLog( @"name is %@ and locality is %@ and administrative area is %@ with zip %@", addressName, city, administrativeArea,postalCode );
        
        
        
        
        selectedState = [[StatesClass new]abbrevatedStateForShortForm:administrativeArea];
        selectedLocation = city;
        [btnLocation1 setTitle:city forState:UIControlStateNormal];
        [btnLocation2 setTitle:selectedState forState:UIControlStateNormal];
        
        [_tableView reloadData];
        [AppDelegate hideLoadingIndicator];
        
    }    } ];
    
    
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            [AppDelegate hideLoadingIndicator];
        }
            break;
        default:{
            [locationManager startUpdatingLocation];
        }
            break;
    }
}

#pragma mark - Picker View Data source & Delegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    if(pickerView == statePicker)
    {
        return [[self.statesDictionary allKeys]count];
    }
    else
    {
        return [[self.statesDictionary objectForKey:selectedState]count];
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(pickerView == statePicker)
    {
        selectedState = [[[self.statesDictionary allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)]objectAtIndex:row];
        [btnLocation1 setTitle:selectedState forState:UIControlStateNormal];
    }
    else
    {
        selectedLocation = [[[self.statesDictionary objectForKey:selectedState]sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)] objectAtIndex:row];
        [btnLocation2 setTitle:selectedLocation forState:UIControlStateNormal];
        
    }
//   [_tableView reloadData];
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(pickerView == statePicker)
    {
        
        return [[[self.statesDictionary allKeys] sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)]objectAtIndex:row];
    }
    else
    {
        return [[[self.statesDictionary objectForKey:selectedState]sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)] objectAtIndex:row];
    }
}

#pragma mark - TableView Delegates & Datasources

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return showHiddenCell?7:6;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    switch (indexPath.row) {
            
        case 0:
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"headingCell" forIndexPath:indexPath];
            cell.separatorInset = UIEdgeInsetsMake(0.f, 0.f, 0.f, cell.bounds.size.width);
            break;
        case 1:
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"typeCell" forIndexPath:indexPath];
            btnType1 =(UIButton *)[cell viewWithTag:500];
            btnType2 =(UIButton *)[cell viewWithTag:501];
            btnType3 =(UIButton *)[cell viewWithTag:502];
            break;
        case 2:
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"bhkCell" forIndexPath:indexPath];
            btnRoom1 =(UIButton *)[cell viewWithTag:600];
            btnRoom2 =(UIButton *)[cell viewWithTag:601];
            btnRoom3 =(UIButton *)[cell viewWithTag:602];
            btnRoom4 =(UIButton *)[cell viewWithTag:603];
            btnRoom5 =(UIButton *)[cell viewWithTag:604];
            btnRoom6 =(UIButton *)[cell viewWithTag:605];
            break;
            
        case 3:
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"locationCell" forIndexPath:indexPath];
            cell.separatorInset = UIEdgeInsetsMake(0.f, 0.f, 0.f, cell.bounds.size.width);
            btnLocation1 =(UIButton *)[cell viewWithTag:700];
            btnLocation2 =(UIButton *)[cell viewWithTag:701];
            btnLocation3 =(UIButton *)[cell viewWithTag:702];
            if (![selectedState isEqualToString:@"Anywhere"]) {
                [btnLocation1 setTitle:selectedState forState:UIControlStateNormal];
            }else {
                [btnLocation1 setTitle:@"Select Location" forState:UIControlStateNormal];
            }
            if (![selectedLocation isEqualToString:@"Anywhere"]) {
                [btnLocation2 setTitle:selectedLocation forState:UIControlStateNormal];
            }else {
               [btnLocation2 setTitle:@"Select Location" forState:UIControlStateNormal];
            }
            break;
            
//        case 4:
//            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"currentLocationCell" forIndexPath:indexPath];
//            break;
//            
//        case 5:
//            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"selectStateCell" forIndexPath:indexPath];
//            cell.detailTextLabel.text = selectedState;
//            
//            break;
            
        case 4:
            if(showHiddenCell && showStatePicker)
            {
                cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"statePickerCell" forIndexPath:indexPath];
                
                statePicker = (UIPickerView *)[cell viewWithTag:3];
                selectedLocation = @"Anywhere";
//                [btnLocation1 setTitle:@"Select Location" forState:UIControlStateNormal];
//                [btnLocation2 setTitle:@"Select State" forState:UIControlStateNormal];
            }else if (showHiddenCell && !showStatePicker)
            {
                cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"locationPickerCell" forIndexPath:indexPath];
                LocationPicker = (UIPickerView *)[cell viewWithTag:5];
                [LocationPicker reloadAllComponents];
            }
            else
            {
                cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"pricePickerCell" forIndexPath:indexPath];
                
                TTRangeSlider *slider = (TTRangeSlider *)[cell viewWithTag:8];
                slider.delegate = self;
                slider.tintColorBetweenHandles = [UIColor colorWithRed:0.447 green:0.000 blue:0.565 alpha:1.000];
                slider.handleImage = [UIImage imageNamed:@"slider-default7-handle.png"];
                [slider setSelectedMaximum:[priceMax floatValue]];
                [slider setSelectedMinimum:[priceMin floatValue]];
                
            }
            break;
            
        case 5:
            
            if(showHiddenCell)
            {
                cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"pricePickerCell" forIndexPath:indexPath];
                
                TTRangeSlider *slider = (TTRangeSlider *)[cell viewWithTag:8];
                slider.tintColorBetweenHandles = [UIColor colorWithRed:0.447 green:0.000 blue:0.565 alpha:1.000];
                slider.delegate = self;
                slider.handleImage = [UIImage imageNamed:@"slider-default7-handle.png"];
                [slider setSelectedMaximum:[priceMax floatValue]];
                [slider setSelectedMinimum:[priceMin floatValue]];
                
            }
            else
            {
                cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"moreCell" forIndexPath:indexPath];
                UILabel *lable =(UILabel *)[cell viewWithTag:2];
                if(moreFiltersCount >0)
                    lable.text = [NSString stringWithFormat:@"%d",moreFiltersCount];
                else
                    lable.text = @"";
                btnFindNow =(UIButton *)[cell viewWithTag:1000];
                [[btnFindNow layer] setCornerRadius:20.0];
                [[btnFindNow layer] setBorderColor:(__bridge CGColorRef _Nullable)([UIColor clearColor])];
                [[btnFindNow layer] setBorderWidth:0.1];
                
                CAGradientLayer *gradient = [CAGradientLayer layer];
                gradient.frame = btnFindNow.bounds;
                [gradient setBorderWidth:0.1];
                [gradient setCornerRadius:20.0];
                [gradient setStartPoint:CGPointMake(0.0, 0.5)];
                [gradient setEndPoint:CGPointMake(1.0, 0.5)];
                [gradient setBorderColor:(__bridge CGColorRef _Nullable)([UIColor clearColor])];
                gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:0.267 green:0.176 blue:0.996 alpha:1.000] CGColor], (id)[[UIColor colorWithRed:0.467 green:0.000 blue:0.522 alpha:1.000] CGColor], nil];
                [btnFindNow.layer insertSublayer:gradient atIndex:0];
            }
            break;
            
        case 6:
            cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"moreCell" forIndexPath:indexPath];
            UILabel *lable =(UILabel *)[cell viewWithTag:2];
            if(moreFiltersCount >0)
                lable.text = [NSString stringWithFormat:@"%d",moreFiltersCount];
            else
                lable.text = @"";
            btnFindNow =(UIButton *)[cell viewWithTag:1000];
            [[btnFindNow layer] setCornerRadius:20.0];
            [[btnFindNow layer] setBorderColor:(__bridge CGColorRef _Nullable)([UIColor clearColor])];
            [[btnFindNow layer] setBorderWidth:0.1];
            
            CAGradientLayer *gradient = [CAGradientLayer layer];
            gradient.frame = btnFindNow.bounds;
            [gradient setBorderWidth:0.1];
            [gradient setCornerRadius:20.0];
            [gradient setStartPoint:CGPointMake(0.0, 0.5)];
            [gradient setEndPoint:CGPointMake(1.0, 0.5)];
            [gradient setBorderColor:(__bridge CGColorRef _Nullable)([UIColor clearColor])];
            gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:0.267 green:0.176 blue:0.996 alpha:1.000] CGColor], (id)[[UIColor colorWithRed:0.467 green:0.000 blue:0.522 alpha:1.000] CGColor], nil];
            [btnFindNow.layer insertSublayer:gradient atIndex:0];

            break;
            
    }
    
  
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    switch (indexPath.row) {
//        case 4:
//            
//          
//            break;
//        case 5:
//            
//            
//            break;
//        case 6:
//            
//            
//            break;
//        case 8:
//            if(!showHiddenCell)
//            {
//                MoreFiltersViewController *moreFilterVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MoreFiltersViewController"];
//                moreFilterVC.moreFilterDatas = self.moreFilterDictionary;
//                [self.navigationController pushViewController:moreFilterVC animated:YES];
//                [[NSNotificationCenter defaultCenter ]addObserver:self selector:@selector(updateMoreFilters:) name:@"AddMoreFilters" object:nil];                
//            }
//            break;
//        case 9:
//            if(showHiddenCell)
//            {
//                
//            }
//            break;
//            
//        default:
//            break;
//    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    switch (indexPath.row) {
        case 0:
            return 60;
            
        case 1:
            return 135;
            
        case 2:
            return 135;
            
        case 3:
            return 135;
            
        case 4:
            if(showHiddenCell && showStatePicker)
                return 193;
            else
                return 135;
            
        case 5:
            if(showHiddenCell && showStatePicker)
                return 135;
            else if (showHiddenCell && !showStatePicker)
                return 193;
            else
                return 110;
            
        case 6:
            return 110;
        default:
            return 110;
    }
  
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

#pragma mark - TTRangeSliderViewDelegate

-(void)rangeSlider:(TTRangeSlider *)sender didChangeSelectedMinimumValue:(float)selectedMinimum andMaximumValue:(float)selectedMaximum{
    
        priceMax = [NSString stringWithFormat:@"%.0f",selectedMaximum];
        priceMin = [NSString stringWithFormat:@"%.0f",selectedMinimum];
    
}

#pragma mark - KeyBoard Notifications

- (void)keyboardWillShow:(NSNotification*)aNotification {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.tableView.contentInset.top, 0.0, kbSize.height, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillHide:(NSNotification*)aNotification {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.35];
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.tableView.contentInset.top, 0.0, 0.0, 0.0);
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    [UIView commitAnimations];
    
}

@end
