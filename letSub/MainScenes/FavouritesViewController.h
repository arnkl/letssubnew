//
//  SecondViewController.h
//  letSub
//
//  Created by Vishnu Varthan .P on 04/03/16.
//  Copyright © 2016 com.letssub.LetsSub All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavouritesViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *errView;
@property (weak, nonatomic) IBOutlet UIView *noDataView;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

- (IBAction)reloadContent:(id)sender;

@end

